/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>

#include <string>
#include <tuple>  // for tie
#include <utility>

#include "clang/AST/ASTContext.h"
#include "clang/AST/CanonicalType.h"
#include "clang/AST/Decl.h"
#include "clang/AST/Expr.h"
#include "clang/AST/OperationKinds.h"
#include "clang/AST/Stmt.h"
#include "clang/Analysis/AnalysisDeclContext.h"
#include "clang/Analysis/ProgramPoint.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugReporter.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugReporterVisitors.h"
#include "clang/StaticAnalyzer/Core/BugReporter/PathDiagnostic.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CallEvent.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ExplodedGraph.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramStateTrait.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState_Fwd.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/SValBuilder.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/SVals.h"
#include "llvm/ADT/FoldingSet.h"
#include "llvm/ADT/IntrusiveRefCntPtr.h"
#include "llvm/ADT/Optional.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/raw_ostream.h"

#include "src/analyzer.h"
#include "src/debug.h"
#include "src/util.h"

namespace clang {
class Decl;
}

static inline llvm::raw_ostream& debug(int verbosity) {
    return Debug::log(verbosity, Debug::Topic::FLOATMAGE);
}

REGISTER_TRAIT_WITH_PROGRAMSTATE(ExceptionPending, bool)

class PreviousExceptionFinder : public ento::BugReporterVisitor {
    const clang::StackFrameContext* m_stackFrame;
    const clang::CallExpr* m_firstCall;
    const clang::CallExpr* m_secondCall;

    static std::pair<const ento::PathDiagnosticLocation, const clang::CallExpr*>
    getCallerLocationFromNode(const ento::ExplodedNode* node,
                              ento::BugReporterContext& cx) {
        clang::ProgramPoint pp = node->getLocation();
        const ento::PathDiagnosticLocation loc =
            ento::PathDiagnosticLocation::create(pp, cx.getSourceManager());
        return {loc, llvm::cast<clang::CallExpr>(loc.asStmt())};
    }

    static unsigned stackDepth(const clang::LocationContext* loc) {
        unsigned stackDepth = 0;
        while ((loc = loc->getParent()))
            stackDepth++;
        return stackDepth;
    }

    static std::pair<const clang::StackFrameContext*, const clang::CallExpr*>
    moveUpStack(const clang::StackFrameContext* stackFrame, unsigned nFrames) {
        assert(nFrames > 0 && "Don't move 0 frames up the stack");

        const clang::Stmt* callSite;
        for (unsigned count = 0; count < nFrames; ++count) {
            callSite = stackFrame->getCallSite();
            assert(stackFrame->getParent() &&
                   "Tried to move too far up the stack");
            stackFrame = stackFrame->getParent()->getStackFrame();
        }
        return {stackFrame, llvm::cast<clang::CallExpr>(callSite)};
    }

 public:
    PreviousExceptionFinder(const clang::StackFrameContext* stackFrame)
        : m_stackFrame(stackFrame),
          m_firstCall(nullptr),
          m_secondCall(nullptr) {}

    // Used for hashing/comparing the object
    void Profile(llvm::FoldingSetNodeID& id) const override {
        static int tag = 0;
        id.AddPointer(&tag);
        id.AddPointer(m_stackFrame);
        id.AddPointer(m_firstCall);
        id.AddPointer(m_secondCall);
    }

    std::shared_ptr<ento::PathDiagnosticPiece> reportSiteOfExceptionPending(
        const ento::ExplodedNode* node, ento::BugReporterContext& cx,
        ento::BugReport& report) {
        const clang::StackFrameContext* exceptionStackFrame =
            node->getLocationContext()->getStackFrame();
        ento::PathDiagnosticLocation loc;

        m_secondCall = llvm::cast<clang::CallExpr>(report.getStmt());

        // Basically, for the error message to make sense, we need to make sure
        // that we report the two JSAPI calls as originating from the same stack
        // frame. Because the analyzer inlines calls for inter-procedure
        // analysis, we may otherwise get messages saying to check the error
        // status of some call before some other call, where the two calls are
        // in entirely different procedures, which would be confusing.
        unsigned exceptionStackDepth = stackDepth(exceptionStackFrame);
        unsigned secondCallStackDepth = stackDepth(m_stackFrame);
        if (exceptionStackDepth == secondCallStackDepth) {
            // The site that originated the exception is in the same procedure
            // as the site of the second JSAPI call. @node is the correct
            // location to warn about.
            std::tie(loc, m_firstCall) = getCallerLocationFromNode(node, cx);
        } else if (exceptionStackDepth > secondCallStackDepth) {
            // It's possible that the exception originated in a deeper stack
            // frame than the second JSAPI call, due to inlining. Go up in the
            // stack until we are located in the same stack frame as where the
            // bug report initiated.
            std::tie(exceptionStackFrame, m_firstCall) =
                moveUpStack(exceptionStackFrame,
                            exceptionStackDepth - secondCallStackDepth);
            loc = ento::PathDiagnosticLocation(
                m_firstCall, cx.getSourceManager(), exceptionStackFrame);
        } else {
            // It's also possible that the second JSAPI call was in a deeper
            // stack frame. This happens if you forgot to annotate a function,
            // and the analyzer inlined it. This is one reason why we override
            // the last piece of the path (see getEndPath() below.)
            std::tie(m_stackFrame, m_secondCall) = moveUpStack(
                m_stackFrame, secondCallStackDepth - exceptionStackDepth);
            std::tie(loc, m_firstCall) = getCallerLocationFromNode(node, cx);
        }

        if (!loc.isValid() || !loc.asLocation().isValid())
            return nullptr;

        llvm::SmallString<512> buf;
        llvm::raw_svector_ostream os(buf);
        os << "Exception is set pending when '";
        m_firstCall->getDirectCallee()->printQualifiedName(os);
        os << "()' fails";

        return std::make_shared<ento::PathDiagnosticEventPiece>(loc, os.str());
    }

    std::shared_ptr<ento::PathDiagnosticPiece> VisitNode(
        const ento::ExplodedNode* succ, ento::BugReporterContext& cx,
        ento::BugReport& report) override {
        const ento::ExplodedNode* pred = succ->getFirstPred();

        // If @succ's state has an exception pending and @pred's does not, then
        // @pred is the call site where the exception was made pending.
        if (succ->getState()->get<ExceptionPending>() &&
            !pred->getState()->get<ExceptionPending>())
            return reportSiteOfExceptionPending(pred, cx, report);

        return nullptr;
    }

    std::shared_ptr<ento::PathDiagnosticPiece> getEndPath(
        ento::BugReporterContext& cx, const ento::ExplodedNode* node,
        ento::BugReport& report) override {
        const ento::PathDiagnosticLocation loc = ento::PathDiagnosticLocation(
            m_secondCall, cx.getSourceManager(), m_stackFrame);

        llvm::SmallString<512> buf;
        llvm::raw_svector_ostream os(buf);
        os << "Check the error status of '";
        m_firstCall->getDirectCallee()->printQualifiedName(os);
        os << "()' before calling '";
        m_secondCall->getDirectCallee()->printQualifiedName(os);
        os << "()'";

        return std::make_shared<ento::PathDiagnosticEventPiece>(loc, os.str());
    }
};

FloatmageAnalyzer::FloatmageAnalyzer()
    : m_callWithExceptionPending(std::make_unique<ento::BugType>(
          this, "Call with exception pending", "Floatmage")) {}

// Return the function declaration associated with a CallEvent or null if
// there is no such declaration
static const clang::FunctionDecl* DeclarationFromCallEvent(
    const ento::CallEvent& call) {
    const clang::Decl* d = call.getDecl();
    if (!d)
        return nullptr;
    return llvm::dyn_cast<clang::FunctionDecl>(d);
}

void FloatmageAnalyzer::checkPreCall(const ento::CallEvent& call,
                                     ento::CheckerContext& cx) const {
    debug(3) << "Pre call " << call << "\n";

    ento::ProgramStateRef state = cx.getState();

    if (!state->get<ExceptionPending>())
        return;

    const clang::FunctionDecl* callee = DeclarationFromCallEvent(call);
    if (!callee)
        return;

    if (!FunctionHasJSAPIReturnConvention(callee))
        return;

    llvm::SmallString<512> buf;
    llvm::raw_svector_ostream os(buf);
    os << "Calling " << call << " with exception already pending";

    ento::ExplodedNode* node = cx.generateNonFatalErrorNode();
    auto report = std::make_unique<ento::BugReport>(*m_callWithExceptionPending,
                                                    os.str(), node);
    const clang::StackFrameContext* stackFrame = cx.getStackFrame();
    report->addVisitor(std::make_unique<PreviousExceptionFinder>(stackFrame));
    cx.emitReport(std::move(report));
}

void FloatmageAnalyzer::checkPostCall(const ento::CallEvent& call,
                                      ento::CheckerContext& cx) const {
    debug(3) << "Post call " << call << "\n";

    const clang::FunctionDecl* callee = DeclarationFromCallEvent(call);
    if (!callee)
        return;

    if (!FunctionHasJSAPIReturnConvention(callee))
        return;

    ento::ProgramStateRef state = cx.getState();

    ento::SValBuilder& builder = cx.getSValBuilder();
    ento::SVal retval = call.getReturnValue();
    ento::SVal failureValue = builder.makeZeroVal(call.getResultType());
    ento::SVal success = builder.evalBinOp(
        state, clang::BO_NE, retval, failureValue, builder.getConditionType());

    auto definedSuccess = success.getAs<ento::DefinedOrUnknownSVal>();
    if (!definedSuccess)
        return;

    ento::ProgramStateRef succeeded, failed;
    std::tie(succeeded, failed) = state->assume(*definedSuccess);
    if (succeeded)
        cx.addTransition(succeeded->set<ExceptionPending>(false));
    if (failed)
        cx.addTransition(failed->set<ExceptionPending>(true));
}
