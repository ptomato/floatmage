/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>

#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState_Fwd.h"

namespace clang {
namespace ento {
class CallEvent;
class CheckerContext;
}  // namespace ento
}  // namespace clang

namespace check = clang::ento::check;
namespace ento = clang::ento;

class FloatmageAnalyzer
    : public ento::Checker<check::PreCall, check::PostCall> {
    std::unique_ptr<ento::BugType> m_callWithExceptionPending;

 public:
    explicit FloatmageAnalyzer();

    void checkPreCall(const ento::CallEvent& call,
                      ento::CheckerContext& cx) const;
    void checkPostCall(const ento::CallEvent& call,
                       ento::CheckerContext& cx) const;
};
