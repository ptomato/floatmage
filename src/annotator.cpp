/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <unordered_set>
#include <vector>

#include "clang/AST/Attr.h"                   // for AnnotateAttr
#include "clang/AST/Decl.h"                   // for FunctionDecl, Namespace...
#include "clang/AST/DeclBase.h"               // for Decl (ptr only), DeclCo...
#include "clang/AST/DeclCXX.h"                // for CXXConstructorDecl, CXX...
#include "clang/AST/DeclGroup.h"              // for DeclGroupRef
#include "clang/AST/Type.h"                   // for QualType, Type
#include "clang/Basic/SourceManager.h"        // for SourceManager
#include "clang/Basic/Specifiers.h"           // for AccessSpecifier, Access...
#include "clang/Frontend/CompilerInstance.h"  // for CompilerInstance
#include "llvm/ADT/ArrayRef.h"                // for ArrayRef
#include "llvm/ADT/StringRef.h"               // for StringRef
#include "llvm/Support/Casting.h"             // for isa, dyn_cast
#include "llvm/Support/raw_ostream.h"         // for outs, raw_ostream

#include "src/annotator.h"
#include "src/preannotated.h"
#include "src/util.h"  // for TypeIsJSContextPointer

Annotator::Annotator(clang::CompilerInstance& compiler,
                     bool debugAnnotationCandidates, unsigned esrVersion)
    : m_src(compiler.getSourceManager()),
      m_ast(compiler.getASTContext()),
      m_preannotated(getPreannotated(esrVersion)),
      m_assumedInfallible(getAssumedInfallible(esrVersion)),
      m_assumedInfallibleTypes(getAssumedInfallibleTypes(esrVersion)),
      m_debugAnnotationCandidates(debugAnnotationCandidates) {}

static bool HasAnyJSContextArg(const clang::FunctionDecl* func) {
    for (const auto* arg : func->parameters()) {
        if (TypeIsJSContextPointer(arg->getType()))
            return true;
    }
    return false;
}

// Heuristics for ruling out functions included from jsapi.h and friends that
// probably don't need to be annotated as jsapi_return_convention
static bool JSAPIReturnConventionLikely(const clang::FunctionDecl* func) {
    // Constructors, cast operators, destructors are infallible and cannot have
    // JSAPI return convention. Private methods may, but we are not interested
    // in annotating them at this time for external JSAPI users.
    if (llvm::isa<clang::CXXConstructorDecl>(func) ||
        llvm::isa<clang::CXXConversionDecl>(func) ||
        llvm::isa<clang::CXXDestructorDecl>(func) ||
        func->isOverloadedOperator() ||
        func->getAccess() == clang::AccessSpecifier::AS_private)
        return false;

    // Eliminate non-methods (functions) that have no JSContext* argument,
    // they cannot have JSAPI return convention
    if (!llvm::isa<clang::CXXMethodDecl>(func)) {
        if (!HasAnyJSContextArg(func))
            return false;
    }

    // Void return type cannot have JSAPI return convention
    const clang::Type* rtype = func->getReturnType().getTypePtr();
    if (rtype->isVoidType())
        return false;

    // Eliminate mozilla namespace (too low in the stack) and all detail
    // namespaces (private)
    std::string qident = func->getQualifiedNameAsString();
    if (qident.find("::detail::") != std::string::npos ||
        qident.find("mozilla::") != std::string::npos)
        return false;

    return true;
}

void Annotator::PreannotateAndOutputCandidates(clang::Decl* decl) {
    // Recurse into namespaces
    if (const auto* ns = llvm::dyn_cast<clang::NamespaceDecl>(decl)) {
        for (clang::Decl* decl : ns->decls())
            PreannotateAndOutputCandidates(decl);
    }

    auto* func = llvm::dyn_cast<clang::FunctionDecl>(decl);
    if (!func)
        return;

    // Annotate the function if it is in our preannotated list
    std::string qident = func->getQualifiedNameAsString();
    if (m_preannotated.count(qident) > 0) {
        auto* attr = clang::AnnotateAttr::CreateImplicit(
            m_ast, "jsapi_return_convention");
        func->addAttr(attr);
        return;
    }

    if (!m_debugAnnotationCandidates || m_assumedInfallible.count(qident) > 0)
        return;

    // Eliminate methods of classes that are known not to have any methods
    // with JSAPI return convention
    for (const std::string& templateName : m_assumedInfallibleTypes) {
        if (qident.find(templateName) == 0)
            return;
    }

    if (!JSAPIReturnConventionLikely(func))
        return;

    llvm::StringRef includeFile = m_src.getBufferName(func->getLocation());
    if (includeFile.find("mozjs") == std::string::npos)
        return;

    llvm::outs() << "Candidate for preannotation: " << qident << "\n";
}

bool Annotator::HandleTopLevelDecl(clang::DeclGroupRef group) {
    for (auto it = group.begin(), end = group.end(); it != end; it++)
        PreannotateAndOutputCandidates(*it);
    return true;
}
