/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <unordered_set>
#include <vector>

#include "clang/AST/ASTConsumer.h"

namespace clang {
class ASTContext;
class CompilerInstance;
class Decl;
class DeclGroupRef;
class SourceManager;
}

// Walks through the AST and annotates any functions that have been
// "preannotated" in preannotated.cpp. Optionally suggests functions from JSAPI
// header files that might need to be preannotated, as a debug aid for building
// up the preannotation lists.
class Annotator : public clang::ASTConsumer {
    clang::SourceManager& m_src;
    clang::ASTContext& m_ast;
    const std::unordered_set<std::string> m_preannotated;
    const std::unordered_set<std::string> m_assumedInfallible;
    const std::vector<std::string> m_assumedInfallibleTypes;

    // Command line options
    bool m_debugAnnotationCandidates : 1;

    void PreannotateAndOutputCandidates(clang::Decl* decl);

 public:
    explicit Annotator(clang::CompilerInstance& compiler,
                       bool debugAnnotationCandidates, unsigned esrVersion);

    // Overrides of ASTConsumer methods
    bool HandleTopLevelDecl(clang::DeclGroupRef group) override;
};
