/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include "clang/AST/ASTContext.h"
#include "clang/AST/Decl.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchersInternal.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/SourceManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/AnalysisManager.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/Casting.h"

#include "src/correctness.h"
#include "src/util.h"

// Implements checks on functions that are annotated with
// jsapi_return_convention, to make sure that they meet certain minimum
// requirements. They must have a suitable return type that can indicate whether
// the function threw an exception, and they must have a JSContext* parameter
// (or if a method, the class must have a JSContext* member.)

namespace ast_matchers = clang::ast_matchers;

class AnnotationCorrectnessCallback
    : public ast_matchers::MatchFinder::MatchCallback {
    clang::DiagnosticsEngine& m_diagnostics;

    void reportNoJSContext(const clang::FunctionDecl* func) {
        bool is_method = llvm::isa<clang::CXXMethodDecl>(func);

        unsigned id = m_diagnostics.getCustomDiagID(
            clang::DiagnosticsEngine::Error,
            "%q0 should have JSContext* as the type of its first "
            "argument%select{| or as a member of its class}1 so that it can "
            "report exceptions");
        const clang::DiagnosticBuilder builder =
            m_diagnostics.Report(func->getLocation(), id) << func << is_method;
        if (func->param_size() > 0)
            builder << func->parameters()[0]->getSourceRange();
    }

    void reportVoidReturn(const clang::FunctionDecl* func) {
        unsigned id = m_diagnostics.getCustomDiagID(
            clang::DiagnosticsEngine::Error,
            "%q0 must have a return type; it needs to indicate whether an "
            "exception was thrown");
        m_diagnostics.Report(func->getLocation(), id)
            << func->getReturnTypeSourceRange() << func;
    }

    void reportNoTruthValueReturn(const clang::FunctionDecl* func) {
        unsigned id = m_diagnostics.getCustomDiagID(
            clang::DiagnosticsEngine::Error,
            "The return type %q0 of %q1 should be a type with a value to "
            "clearly indicate whether an exception was thrown, such as 'bool'");
        m_diagnostics.Report(func->getLocation(), id)
            << func->getReturnTypeSourceRange() << func->getReturnType()
            << func;
    }

 public:
    AnnotationCorrectnessCallback(clang::DiagnosticsEngine& diagnostics)
        : m_diagnostics(diagnostics) {}

    void run(const ast_matchers::MatchFinder::MatchResult& result) {
        if (const auto* func =
                result.Nodes.getNodeAs<clang::FunctionDecl>("no_jscontext")) {
            reportNoJSContext(func);
            return;
        }

        if (const auto* func =
                result.Nodes.getNodeAs<clang::FunctionDecl>("void_return")) {
            reportVoidReturn(func);
            return;
        }

        if (const auto* func = result.Nodes.getNodeAs<clang::FunctionDecl>(
                "no_truthvalue_return")) {
            reportNoTruthValueReturn(func);
            return;
        }
    }
};

void AnnotationCorrectnessChecker::checkASTDecl(
    const clang::FunctionDecl* func, ento::AnalysisManager& analysisMgr,
    ento::BugReporter&) const {
    if (!analysisMgr.getSourceManager().isWrittenInMainFile(
            func->getLocation()))
        return;

    ast_matchers::MatchFinder finder;
    AnnotationCorrectnessCallback callback(
        analysisMgr.getASTContext().getDiagnostics());

    using namespace ast_matchers;  // otherwise much too tedious

    // clang-format off
    finder.addMatcher(
        functionDecl(
            unless(cxxMethodDecl()),
            isAnnotatedJSAPIReturnConvention(),
            unless(hasAnyParameter(hasJSContextType())))
            .bind("no_jscontext"),
        &callback);
    finder.addMatcher(
        cxxMethodDecl(
            isAnnotatedJSAPIReturnConvention(),
            unless(anyOf(
                ofClass(hasDescendant(fieldDecl(hasJSContextType()))),
                hasAnyParameter(hasJSContextType()))))
            .bind("no_jscontext"),
        &callback);
    finder.addMatcher(
        functionDecl(
            isAnnotatedJSAPIReturnConvention(),
            returns(voidType()))
            .bind("void_return"),
        &callback);
    finder.addMatcher(
        functionDecl(
            isAnnotatedJSAPIReturnConvention(),
            unless(returns(hasCanonicalType(anyOf(
                voidType(),  // handled in the matcher above
                booleanType(),
                isAnyPointer(),
                hasDeclaration(anyOf(
                    namedDecl(hasAnyName("jsid", "JS::PropertyKey")),
                    // matches classes that are std::is_convertible<T, bool> or
                    // std::is_convertible<T, U*>
                    hasDescendant(cxxConversionDecl(returns(anyOf(
                        booleanType(),
                        isAnyPointer())))))))))))
            .bind("no_truthvalue_return"),
        &callback);
    // clang-format on

    finder.match(*func, analysisMgr.getASTContext());
}
