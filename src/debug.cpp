/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <sstream>
#include <string>

#include "clang/AST/ASTContext.h"
#include "clang/AST/Decl.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CallEvent.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState_Fwd.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/raw_ostream.h"

#include "src/debug.h"

namespace ento = clang::ento;

static int wantedVerbosity = -1;
static int allowedTopics = -1;

namespace Debug {

llvm::raw_ostream& log(int verbosity, Topic t) {
    if (wantedVerbosity == -1) {
        const char* env = std::getenv("FLOATMAGE_DEBUG_VERBOSITY");
        if (env)
            wantedVerbosity = std::atoi(env);
        else
            wantedVerbosity = 0;
    }
    if (allowedTopics == -1) {
        const char* env = std::getenv("FLOATMAGE_DEBUG_TOPICS");
        if (env) {
            allowedTopics = 0;
            std::istringstream buf(env);
            std::string topic;
            while (std::getline(buf, topic, ',')) {
                if (topic == "FLOATMAGE")
                    allowedTopics |= Topic::FLOATMAGE;
                if (topic == "JSVALUE")
                    allowedTopics |= Topic::JSVALUE;
            }
        } else {
            allowedTopics = Topic::ALL;
        }
    }

    if (verbosity <= wantedVerbosity && allowedTopics & t) {
        llvm::errs() << "debug: [";
        if (t & Topic::FLOATMAGE)
            llvm::errs() << "floatmage";
        if (t & Topic::JSVALUE)
            llvm::errs() << "jsvalue";
        return llvm::errs() << "] ";
    }
    return llvm::nulls();
}

void setVerbosity(int verbosity) { wantedVerbosity = verbosity; }

};  // namespace Debug

// Debug pretty printers

llvm::raw_ostream& operator<<(llvm::raw_ostream& os,
                              const ento::CallEvent& call) {
    const auto* decl = llvm::dyn_cast_or_null<clang::NamedDecl>(call.getDecl());
    if (!decl)
        return os << "(unknown function)";
    os << "'";
    decl->getNameForDiagnostic(os, decl->getASTContext().getPrintingPolicy(),
                               /* qualified = */ true);
    return os << "()'";
}
