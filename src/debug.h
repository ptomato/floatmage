/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace clang {
namespace ento {
class CallEvent;
}
}  // namespace clang
namespace llvm {
class raw_ostream;
}

namespace Debug {

enum Topic { FLOATMAGE = 0x1, JSVALUE = 0x2, ALL = FLOATMAGE | JSVALUE };

// verbosity: 0 = nothing
//            1 = log when handling special JS APIs
//            2 = log most things that the plugin does
//            3 = log every PreCall and PostCall
llvm::raw_ostream& log(int verbosity, Topic t);

void setVerbosity(int verbosity);

};  // namespace Debug

llvm::raw_ostream& operator<<(llvm::raw_ostream& os,
                              const clang::ento::CallEvent& call);
