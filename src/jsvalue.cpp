/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdint.h>

#include <algorithm>
#include <bitset>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "clang/AST/ASTContext.h"
#include "clang/AST/CanonicalType.h"
#include "clang/AST/Decl.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/DeclTemplate.h"
#include "clang/AST/Expr.h"
#include "clang/AST/ExprCXX.h"
#include "clang/AST/OperationKinds.h"
#include "clang/AST/TemplateBase.h"
#include "clang/AST/Type.h"
#include "clang/Analysis/AnalysisDeclContext.h"
#include "clang/Analysis/ProgramPoint.h"
#include "clang/Basic/OperatorKinds.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugReporter.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugReporterVisitors.h"
#include "clang/StaticAnalyzer/Core/BugReporter/PathDiagnostic.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CallEvent.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ConstraintManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ExplodedGraph.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/MemRegion.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramStateTrait.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/SValBuilder.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/SVals.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/FoldingSet.h"
#include "llvm/ADT/ImmutableSet.h"
#include "llvm/ADT/IntrusiveRefCntPtr.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/raw_ostream.h"

#include "src/debug.h"
#include "src/jsvalue.h"

namespace nonloc = clang::ento::nonloc;

using Key = const ento::MemRegion*;
using KeySet = llvm::ImmutableSet<Key>;

static KeySet::Factory sets;

static constexpr uint64_t NAN_BITS = 9221120237041090560;
static constexpr int64_t MAX_INT32 = 0x7fffffff;
static constexpr int64_t MIN_INT32 = -0x80000000;

static inline llvm::raw_ostream& debug(int verbosity) {
    return Debug::log(verbosity, Debug::Topic::JSVALUE);
}

static std::string printTag(JSValueTag t) {
    if (t == 0)
        return "impossible";
    if (t == JSValueTag::Unknown)
        return "unknown";
    if (t == JSValueTag::GCThing)
        return "any GC thing";
    if (t == (JSValueTag::Unknown & ~JSValueTag::GCThing))
        return "anything but a GC thing";
    if (t == JSValueTag::Primitive)
        return "any primitive value";
    if (t == (JSValueTag::Unknown & ~JSValueTag::Primitive))
        return "anything but a primitive value";

    std::bitset<16> bits(t);
    if (bits.count() > 7)
        return "anything but " + printTag(JSValueTag(JSValueTag::Unknown & ~t));

    std::vector<std::string> descriptions;
    if (t & JSValueTag::Double)
        descriptions.push_back("a double");
    if (t & JSValueTag::Int32)
        descriptions.push_back("a 32-bit integer");
    if (t & JSValueTag::Boolean)
        descriptions.push_back("a boolean");
    if (t & JSValueTag::Undefined)
        descriptions.push_back("undefined");
    if (t & JSValueTag::Null)
        descriptions.push_back("null");
    if ((t & JSValueTag::Magic) == JSValueTag::Magic)
        descriptions.push_back("magic");
    else if (t & JSValueTag::MagicEnum)
        descriptions.push_back("a magic value");
    else if (t & JSValueTag::MagicInt)
        descriptions.push_back("a magic integer");
    if (t & JSValueTag::String)
        descriptions.push_back("a string");
    if (t & JSValueTag::Symbol)
        descriptions.push_back("a symbol");
    if (t & JSValueTag::PrivateGCThing)
        descriptions.push_back("a private GC value");
    if (t & JSValueTag::BigInt)
        descriptions.push_back("a bigint");
    if (t & JSValueTag::Object)
        descriptions.push_back("an object");
    if (t & JSValueTag::PrivatePointer)
        descriptions.push_back("a private pointer");
    if (t & JSValueTag::PrivateInt)
        descriptions.push_back("a private integer");

    if (descriptions.size() == 1)
        return descriptions[0];
    if (descriptions.size() == 2)
        return descriptions[0] + " or " + descriptions[1];

    std::string retval;
    std::string last = descriptions.back();
    descriptions.pop_back();
    for (const std::string& item : descriptions)
        retval += item + ", ";
    return retval + "or " + last;
}

static llvm::raw_ostream& operator<<(llvm::raw_ostream& os, JSValueTag t) {
    return os << printTag(t);
}

namespace llvm {
template <>
struct FoldingSetTrait<JSValueTag> {
    static inline void Profile(JSValueTag tag, FoldingSetNodeID& id) {
        id.AddInteger(static_cast<int>(tag));
    }
};
}  // namespace llvm

// Program state

// Map from JS::Value memory regions ("keys") to the tag bits that indicate
// their types
REGISTER_MAP_WITH_PROGRAMSTATE(JSValue, Key, JSValueTag);

// Map from JS::RootedValue or JS::HandleValue memory regions ("wrapper keys")
// to JS::Value memory regions ("wrapped keys"). This is necessary because if an
// mutation is made on, say, a JS::HandleValue, we want that knowledge to be
// reflected in the underlying JS::Value.
REGISTER_MAP_WITH_PROGRAMSTATE(Wrap, Key, Key);

// Map from keys to a list of other keys that are known to have the same type
// as the first key. This is so that if an assumption is made on one key, it is
// propagated to other keys that are known to have the same tags as it.
REGISTER_MAP_WITH_PROGRAMSTATE(KnownEqual, Key, KeySet);

JSValueModeling::JSValueModeling()
    : m_wrongValueTag(std::make_unique<ento::BugType>(
          this, "JS::Value method called with wrong value tag", "Floatmage")),
      m_unnecessaryCompare(std::make_unique<ento::BugType>(
          this, "JS::Value comparison is always true", "Floatmage")),
      m_impossibleCompare(std::make_unique<ento::BugType>(
          this, "JS::Value comparison can never be true", "Floatmage")),
      m_nullArgument(std::make_unique<ento::BugType>(
          this, "GC thing pointer must be non-null", "Floatmage")) {}

static bool isJSValue(const clang::CXXRecordDecl* klass) {
    if (!klass)
        return false;
    const auto* ns =
        llvm::dyn_cast<clang::NamespaceDecl>(klass->getDeclContext());
    return ns && ns->getName() == "JS" && klass->getName() == "Value";
}

static bool isTemplatedJSValue(const clang::CXXRecordDecl* klass,
                               const std::string& templateName) {
    if (!klass)
        return false;
    const auto* ns =
        llvm::dyn_cast<clang::NamespaceDecl>(klass->getDeclContext());
    if (!ns || !(ns->getName() == "JS" && klass->getName() == templateName))
        return false;
    const auto* tmpl =
        llvm::dyn_cast<clang::ClassTemplateSpecializationDecl>(klass);
    if (!tmpl)
        return false;
    const clang::TemplateArgumentList& args = tmpl->getTemplateArgs();
    return isJSValue(args[0].getAsType()->getAsCXXRecordDecl());
}

static bool isJSValueWrappedPtrOperations(const clang::CXXRecordDecl* klass) {
    if (!klass)
        return false;
    const auto* ns =
        llvm::dyn_cast<clang::NamespaceDecl>(klass->getDeclContext());
    if (!ns ||
        (ns->getName() != "js" && klass->getName() != "WrappedPtrOperations"))
        return false;
    const auto* tmpl =
        llvm::dyn_cast<clang::ClassTemplateSpecializationDecl>(klass);
    if (!tmpl)
        return false;
    const clang::TemplateArgumentList& args = tmpl->getTemplateArgs();
    return isJSValue(args[0].getAsType()->getAsCXXRecordDecl());
}

static bool isCallToSafelyInitializedJSValue(const clang::Expr* expr) {
    const auto* tmp = llvm::dyn_cast<clang::MaterializeTemporaryExpr>(expr);
    if (!tmp)
        return false;
    const auto* call = llvm::dyn_cast<clang::CallExpr>(tmp->GetTemporaryExpr());
    if (!call)
        return false;
    const clang::FunctionDecl* func = call->getDirectCallee();
    return func && func->getName() == "SafelyInitialized";
}

static bool isAssignment(const clang::Expr* expr) {
    const auto* op = llvm::dyn_cast<clang::CXXOperatorCallExpr>(expr);
    return op && op->isAssignmentOp();
}

static Key keyForSVal(const ento::SVal& sval) {
    Key key = sval.getAsRegion();
    if (!key) {
        const auto lcv = sval.getAs<nonloc::LazyCompoundVal>();
        if (lcv)
            key = lcv->getRegion();
    }
    assert(key && "Need to handle getting a key from another type of SVal");
    key = key->getMostDerivedObjectRegion();
    return key;
}

// This gets the value to use as the wrapped key, given an SVal instance
// representing a, JS::RootedValue or JS::HandleValue, if the key is not already
// in the Wrap map.
static Key keyForTemplatedSVal(ento::ProgramStateRef state,
                               const clang::CXXRecordDecl* klass,
                               const ento::SVal& sval) {
    clang::FieldDecl* ptrField;
    for (auto* field : klass->fields()) {
        if (field->getName() == "ptr") {
            ptrField = field;
            break;
        }
    }
    assert(ptrField);
    return keyForSVal(state->getLValue(ptrField, sval));
}

// This follows the chain of wrapped keys in the Wrap map.
static Key wrappedKey(ento::ProgramStateRef state, Key key) {
    while (const Key* wrappedKey = state->get<Wrap>(key))
        key = *wrappedKey;
    return key;
}

// JSValueTag::Unknown is returned if the value wasn't constructed in scope
static JSValueTag tagForKey(ento::ProgramStateRef state, Key key) {
    const JSValueTag* tagp = state->get<JSValue>(wrappedKey(state, key));
    return tagp ? *tagp : JSValueTag::Unknown;
}

static ento::ProgramStateRef assumeKey(ento::ProgramStateRef state, Key key,
                                       const JSValueTag tag) {
    state = state->set<JSValue>(wrappedKey(state, key), tag);

    const KeySet* setp = state->get<KnownEqual>(key);
    if (setp) {
        KeySet sameAsKey = *setp;
        for (const Key& otherKey : sameAsKey)
            state = state->set<JSValue>(otherKey, tag);
    }

    return state;
}

static ento::ProgramStateRef breakKnownEqualAssociations(
    ento::ProgramStateRef state, Key key) {
    const KeySet* setp = state->get<KnownEqual>(key);
    if (setp) {
        KeySet sameAsKey = *setp;
        for (const Key& otherKey : sameAsKey) {
            setp = state->get<KnownEqual>(otherKey);
            if (!setp)
                continue;
            KeySet sameAsOtherKey = *setp;
            sameAsOtherKey = sets.remove(sameAsOtherKey, key);
            state = state->set<KnownEqual>(otherKey, sameAsOtherKey);
        }
    }
    state = state->remove<KnownEqual>(key);
    return state;
}

static ento::ProgramStateRef mutateKey(ento::ProgramStateRef state, Key key,
                                       const JSValueTag tag) {
    state = state->set<JSValue>(wrappedKey(state, key), tag);
    state = breakKnownEqualAssociations(state, key);
    return state;
}

static ento::ProgramStateRef assumeKeySameAs(ento::ProgramStateRef state,
                                             Key key, Key otherKey) {
    const JSValueTag tag = tagForKey(state, otherKey);
    state = state->set<JSValue>(wrappedKey(state, key), tag);

    const KeySet* setp = state->get<KnownEqual>(key);
    KeySet sameAsKey = setp ? *setp : sets.getEmptySet();
    setp = state->get<KnownEqual>(otherKey);
    KeySet sameAsOtherKey = setp ? *setp : sets.getEmptySet();
    sameAsKey = sets.add(sameAsKey, otherKey);
    sameAsOtherKey = sets.add(sameAsOtherKey, key);
    state = state->set<KnownEqual>(key, sameAsKey);
    state = state->set<KnownEqual>(otherKey, sameAsOtherKey);

    return state;
}

static ento::ProgramStateRef createWrap(ento::ProgramStateRef state, Key key,
                                        Key wrappedKey) {
    return state->set<Wrap>(key, wrappedKey);
}

static ento::ProgramStateRef swapKeys(ento::ProgramStateRef state, Key key1,
                                      Key key2) {
    const JSValueTag tag1 = tagForKey(state, key1);
    const JSValueTag tag2 = tagForKey(state, key2);
    state = state->set<JSValue>(key1, tag2);
    state = state->set<JSValue>(key2, tag1);

    const KeySet* setp1 = state->get<KnownEqual>(key1);
    const KeySet* setp2 = state->get<KnownEqual>(key2);
    if (setp1 && setp2) {
        state = state->set<KnownEqual>(key1, *setp2);
        state = state->set<KnownEqual>(key2, *setp1);
    } else if (setp1) {
        state = state->set<KnownEqual>(key2, *setp1);
        state = state->remove<KnownEqual>(key1);
    } else if (setp2) {
        state = state->set<KnownEqual>(key1, *setp2);
        state = state->remove<KnownEqual>(key2);
    }

    return state;
}

static ento::ProgramStateRef wrapAndMoveAssociationsTo(
    ento::ProgramStateRef state, Key from, Key to) {
    const KeySet* setp = state->get<KnownEqual>(from);
    if (setp) {
        KeySet sameAsFrom = *setp;
        for (const Key& otherKey : sameAsFrom) {
            setp = state->get<KnownEqual>(otherKey);
            if (!setp)
                continue;
            KeySet sameAsOtherKey = *setp;
            sameAsOtherKey = sets.remove(sameAsOtherKey, from);
            sameAsOtherKey = sets.add(sameAsOtherKey, to);
            state = state->set<KnownEqual>(otherKey, sameAsOtherKey);
        }
        state = state->set<KnownEqual>(to, sameAsFrom);
        state = state->remove<KnownEqual>(from);
    }

    const JSValueTag tag = tagForKey(state, from);
    state = assumeKey(state, to, tag);
    state = createWrap(state, from, to);

    return state;
}

static ento::ProgramStateRef deleteKey(ento::ProgramStateRef state, Key key) {
    state = state->remove<JSValue>(key);
    state = breakKnownEqualAssociations(state, key);
    return state;
}

static void describe(llvm::raw_svector_ostream& os, Key key,
                     const char* fallbackDescription) {
    if (key->canPrintPretty())
        os << key->getDescriptiveName();
    else
        os << fallbackDescription;
}

// Adds notes to a path-sensitive bug report annotating where assumptions were
// made about the type stored in the JSValue under consideration.
class JSValueStoreVisitor final : public ento::BugReporterVisitor {
    Key m_key;
    bool m_done : 1;

    void trackOriginalValue(ento::BugReporterContext& cx,
                            ento::BugReport& report,
                            ento::ProgramStateRef state, const ento::SVal sval,
                            llvm::raw_svector_ostream& os,
                            const char* fallbackDescription,
                            bool* shouldSuppressIfUnknown) const {
        Key copiedKey = keyForSVal(sval);
        os << " because ";
        describe(os, copiedKey, fallbackDescription);
        os << " is " << tagForKey(state, copiedKey);

        // Emit path notes for the copied JS::Value as well
        report.addVisitor(std::make_unique<JSValueStoreVisitor>(copiedKey));
        *shouldSuppressIfUnknown = false;
    }

    bool adjustNoteLocation(ento::BugReporterContext& cx,
                            ento::BugReport& report,
                            const ento::CallEvent& call,
                            const ento::ExplodedNode* node,
                            const clang::LocationContext* lcx,
                            llvm::raw_svector_ostream& os,
                            bool* shouldSuppressIfUnknown,
                            ento::PathDiagnosticLocation* loc) const {
        // Skip generating a note for the 1-arg JS::RootedValue constructor in
        // ESR60
        if (call.isCalled({{"JS", "GCPolicy", "initial"}, 0}))
            return false;

        const auto* meth =
            llvm::dyn_cast_or_null<clang::CXXMethodDecl>(call.getDecl());

        // Don't emit a note for JS::RootedValue(JSContext*) because it's just
        // noise. On the other hand, do emit a special note for RootedValue(
        // JSContext*, JS::Value) to show that the two values share state.
        if (meth) {
            const clang::CXXRecordDecl* klass = meth->getParent();

            if (call.isCalled({{"js", "WrappedPtrOperations", "value"}, 0}) ||
                call.isCalled({{"JS", "Rooted", "get"}, 0}) ||
                call.isCalled({{"JS", "Handle", "get"}, 0}) ||
                call.isCalled({{"JS", "MutableHandle", "get"}, 0})) {
                const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
                trackOriginalValue(cx, report, node->getState(),
                                   meth->getCXXThisVal(), os, "wrapped value",
                                   shouldSuppressIfUnknown);
                return true;
            }

            bool isValue = isJSValue(klass);
            bool isRootedValue = isTemplatedJSValue(klass, "Rooted");
            if (isRootedValue && call.getKind() == ento::CE_CXXConstructor) {
                if (call.getNumArgs() != 2)
                    return false;

                // Skip generating a note if the second argument is
                // SafelyInitialized<JS::Value>()), which most likely means we
                // are inside the 1-arg constructor
                const clang::Expr* copiedExpr = call.getArgExpr(1);
                if (isCallToSafelyInitializedJSValue(copiedExpr))
                    return false;

                trackOriginalValue(cx, report, node->getState(),
                                   call.getArgSVal(1), os, "argument 2",
                                   shouldSuppressIfUnknown);
                *loc = ento::PathDiagnosticLocation(call.getArgExpr(1),
                                                    cx.getSourceManager(),
                                                    lcx->getParent());
                return true;
            }
            if (call.getKind() == ento::CE_CXXMemberOperator &&
                isAssignment(call.getOriginExpr()) &&
                (isValue || isRootedValue)) {
                trackOriginalValue(cx, report, node->getState(),
                                   call.getArgSVal(0), os, "assigned value",
                                   shouldSuppressIfUnknown);
                return true;
            }
        }

        // Treat all of these inlined static constructors as opaque because
        // otherwise the path diagnostic notes get confusing.
        // TODO: See if it's possible to remove the extra "Calling 'X'" and
        // "Returning from 'X'" notes generated here
        if (call.isCalled({{"JS", "BooleanValue"}, 1}) ||
            call.isCalled({{"JS", "CanonicalizedDoubleValue"}, 1}) ||
            call.isCalled({{"JS", "DoubleValue"}, 1}) ||
            call.isCalled({{"JS", "FalseValue"}, 0}) ||
            call.isCalled({{"JS", "Float32Value"}, 1}) ||
            call.isCalled({{"JS", "InfinityValue"}, 0}) ||
            call.isCalled({{"JS", "Int32Value"}, 1}) ||
            call.isCalled({{"JS", "MagicValue"}, 1}) ||
            call.isCalled({{"JS", "MagicValueUint32"}, 1}) ||
            call.isCalled({{"JS", "NaNValue"}, 0}) ||
            call.isCalled({{"JS", "NullValue"}, 0}) ||
            call.isCalled({{"JS", "ObjectValue"}, 1}) ||
            call.isCalled({{"JS", "PrivateGCThingValue"}, 1}) ||
            call.isCalled({{"JS", "PrivateUint32Value"}, 1}) ||
            call.isCalled({{"JS", "PrivateValue"}, 1}) ||
            call.isCalled({{"JS", "StringValue"}, 1}) ||
            call.isCalled({{"JS", "TrueValue"}, 0}) ||
            call.isCalled({{"JS", "UndefinedValue"}, 0})) {
            *loc = ento::PathDiagnosticLocation(
                call.getOriginExpr(), cx.getSourceManager(), lcx->getParent());
        }
        return true;
    }

    std::shared_ptr<ento::PathDiagnosticPiece> addJSValueNote(
        ento::BugReporterContext& cx, ento::BugReport& report,
        const ento::ExplodedNode* node, const JSValueTag tag,
        bool suppressIfUnknown = true) const {
        llvm::SmallString<512> buf;
        llvm::raw_svector_ostream os(buf);
        os << "Assuming ";
        describe(os, m_key, "value");
        os << " is " << tag;

        ento::ProgramStateManager& stateMgr = cx.getStateManager();
        ento::CallEventManager& callMgr = stateMgr.getCallEventManager();

        clang::ProgramPoint pp = node->getLocation();
        const clang::LocationContext* lcx = pp.getLocationContext();
        ento::PathDiagnosticLocation loc =
            ento::PathDiagnosticLocation::create(pp, cx.getSourceManager());

        if (const auto post = node->getLocationAs<clang::PostStmt>()) {
            if (const auto* op =
                    post->getStmtAs<clang::CXXOperatorCallExpr>()) {
                if (op->isAssignmentOp()) {
                    ento::CallEventRef<> call =
                        callMgr.getCall(op, node->getState(), lcx);
                    if (!adjustNoteLocation(cx, report, *call, node, lcx, os,
                                            &suppressIfUnknown, &loc))
                        return nullptr;
                }
            }
        }

        if (lcx->getParent()) {
            ento::CallEventRef<> call =
                callMgr.getCaller(lcx->getStackFrame(), node->getState());
            if (call) {
                if (!adjustNoteLocation(cx, report, *call, node, lcx, os,
                                        &suppressIfUnknown, &loc))
                    return nullptr;
            }
        }

        if (suppressIfUnknown && tag == JSValueTag::Unknown)
            return nullptr;
        return std::make_shared<ento::PathDiagnosticEventPiece>(loc, os.str());
    }

 public:
    JSValueStoreVisitor(Key key) : m_key(key), m_done(false) {}

    void Profile(llvm::FoldingSetNodeID& id) const override {
        static int tag = 0;
        id.AddPointer(&tag);
        id.AddPointer(m_key);
    }

    std::shared_ptr<ento::PathDiagnosticPiece> VisitNode(
        const ento::ExplodedNode* node, ento::BugReporterContext& cx,
        ento::BugReport& report) override {
        if (m_done)
            return nullptr;

        const ento::ExplodedNode* pred = node->getFirstPred();
        const JSValueTag* tagp = node->getState()->get<JSValue>(m_key);
        if (!tagp)
            return nullptr;  // variable doesn't exist yet
        const JSValueTag tagAfter = *tagp;
        tagp = pred->getState()->get<JSValue>(m_key);
        if (!tagp) {
            // First appearance of this JSValue
            m_done = true;
            return addJSValueNote(cx, report, node, tagAfter);
        }
        const JSValueTag tagBefore = *tagp;
        if (tagAfter != tagBefore) {
            // Assumption about the stored type changed
            return addJSValueNote(cx, report, node, tagAfter,
                                  /* suppressIfUnknown = */ false);
        }
        return nullptr;
    }

    static void addToReport(std::unique_ptr<ento::BugReport>& report,
                            ento::ProgramStateRef state, Key key) {
        report->addVisitor(std::make_unique<JSValueStoreVisitor>(key));
        while (const Key* wrappedKey = state->get<Wrap>(key)) {
            report->addVisitor(
                std::make_unique<JSValueStoreVisitor>(*wrappedKey));
            key = *wrappedKey;
        }
    }
};

void JSValueModeling::reportWrongConversion(ento::CheckerContext& cx,
                                            const ento::CallEvent& call,
                                            ento::ExplodedNode* node, Key jsval,
                                            const JSValueTag tag) const {
    llvm::SmallString<512> buf;
    llvm::raw_svector_ostream os(buf);
    os << "Call to " << call << " is incorrect because ";
    describe(os, jsval, "value");
    os << " is " << tag;
    auto report =
        std::make_unique<ento::BugReport>(*m_wrongValueTag, os.str(), node);
    JSValueStoreVisitor::addToReport(report, node->getState(), jsval);
    cx.emitReport(std::move(report));
}

void JSValueModeling::reportImpossibleCompare(ento::CheckerContext& cx,
                                              const ento::CallEvent& call,
                                              ento::ExplodedNode* node,
                                              Key jsval1, const JSValueTag tag1,
                                              Key jsval2,
                                              const JSValueTag tag2) const {
    llvm::SmallString<512> buf;
    llvm::raw_svector_ostream os(buf);
    os << "Call to " << call << " can never succeed because ";
    describe(os, jsval1, "first value");
    os << " can only be " << tag1 << " and ";
    describe(os, jsval2, "second value");
    os << " can only be " << tag2;
    auto report =
        std::make_unique<ento::BugReport>(*m_impossibleCompare, os.str(), node);
    JSValueStoreVisitor::addToReport(report, node->getState(), jsval1);
    JSValueStoreVisitor::addToReport(report, node->getState(), jsval2);
    cx.emitReport(std::move(report));
}

void JSValueModeling::reportUnnecessaryCompare(ento::CheckerContext& cx,
                                               const ento::CallEvent& call,
                                               ento::ExplodedNode* node,
                                               Key jsval1, Key jsval2,
                                               const JSValueTag tag) const {
    llvm::SmallString<512> buf;
    llvm::raw_svector_ostream os(buf);
    os << "Call to " << call << " is unnecessary because both ";
    describe(os, jsval1, "first value");
    os << " and ";
    describe(os, jsval2, "second value");
    os << " are " << tag;
    auto report = std::make_unique<ento::BugReport>(*m_unnecessaryCompare,
                                                    os.str(), node);
    JSValueStoreVisitor::addToReport(report, node->getState(), jsval1);
    JSValueStoreVisitor::addToReport(report, node->getState(), jsval2);
    cx.emitReport(std::move(report));
}

void JSValueModeling::reportNullArgument(ento::CheckerContext& cx,
                                         const ento::CallEvent& call,
                                         ento::ExplodedNode* node,
                                         size_t arg_ix) const {
    llvm::SmallString<512> buf;
    llvm::raw_svector_ostream os(buf);
    os << "Argument " << arg_ix << " to " << call << " must not be null";
    auto report =
        std::make_unique<ento::BugReport>(*m_nullArgument, os.str(), node);
    const auto arg = call.getArgSVal(arg_ix).getAs<ento::KnownSVal>();
    if (arg && arg->getAsRegion())
        report->addVisitor(std::make_unique<ento::FindLastStoreBRVisitor>(
            *arg, arg->getAsRegion(), /* enableNullFPSuppression = */ false));
    cx.emitReport(std::move(report));
}

static void modelMutator(ento::CheckerContext& cx, const ento::CallEvent& call,
                         const JSValueTag checkTag) {
    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());

    debug(2) << call << " Mutate @" << jsval << " to " << checkTag << "\n";

    cx.addTransition(mutateKey(cx.getState(), jsval, checkTag));
}

void JSValueModeling::modelConversion(ento::CheckerContext& cx,
                                      const ento::CallEvent& call,
                                      const JSValueTag checkTag) const {
    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());
    const JSValueTag tag = tagForKey(cx.getState(), jsval);

    debug(2) << call << ": Convert " << tag << " @" << jsval << " to "
             << checkTag << "\n";

    if (!(tag & checkTag) || (tag & checkTag) < tag) {
        ento::ExplodedNode* node = cx.generateErrorNode();
        reportWrongConversion(cx, call, node, jsval, tag);
    }
}

static void assumeNonNullReturn(ento::CheckerContext& cx,
                                const ento::CallEvent& call) {
    if (const auto retval =
            call.getReturnValue().getAs<ento::DefinedOrUnknownSVal>())
        cx.addTransition(cx.getState()->assume(*retval, true));
}

static void assumeObjectOrNullReturn(ento::CheckerContext& cx,
                                     const ento::CallEvent& call) {
    const auto retval =
        call.getReturnValue().getAs<ento::DefinedOrUnknownSVal>();
    if (!retval)
        return;

    ento::ProgramStateRef state = cx.getState();
    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());
    const JSValueTag tag = tagForKey(state, jsval);

    if (tag == JSValueTag::Object) {
        cx.addTransition(state->assume(*retval, false));
    } else if (tag == JSValueTag::Null) {
        cx.addTransition(state->assume(*retval, true));
    }
}

static void modelStaticConstructor(ento::CheckerContext& cx,
                                   const ento::CallEvent& call,
                                   const JSValueTag tag) {
    ento::ProgramStateRef state = cx.getState();
    Key jsval = keyForSVal(call.getReturnValue());
    cx.addTransition(mutateKey(state, jsval, tag));

    debug(2) << call << " " << tag << " @" << jsval << "\n";
}

static JSValueTag tagForRawBits(ento::CheckerContext& cx,
                                const ento::SVal bits) {
    ento::SValBuilder& builder = cx.getSValBuilder();

    const ento::SVal nanBits =
        builder.makeIntValWithPtrWidth(NAN_BITS, /* unsigned = */ true);
    const ento::SVal isNanBits = builder.evalBinOp(
        cx.getState(), clang::BO_EQ, bits, nanBits, builder.getConditionType());
    if (isNanBits.isConstant(1))
        return JSValueTag::Double;

    return JSValueTag::Unknown;
}

static JSValueTag tagForNumberParameter(ento::CheckerContext& cx,
                                        const ento::CallEvent& call,
                                        size_t arg_ix) {
    ento::SValBuilder& builder = cx.getSValBuilder();
    clang::ASTContext& ast = cx.getASTContext();
    ento::ProgramStateRef state = cx.getState();

    const clang::ParmVarDecl* param = call.parameters()[arg_ix];
    const clang::QualType paramType = param->getOriginalType();
    const ento::SVal arg = call.getArgSVal(arg_ix);
    if (paramType->isFloatingType()) {
        // Analyzer cannot reason about floating point numbers
        return JSValueTag::Number;
    } else if (paramType->isUnsignedIntegerType()) {
        if (ast.getTypeInfo(paramType).Width <= 31)
            return JSValueTag::Int32;
        const ento::SVal maxInt32 =
            builder.makeIntValWithPtrWidth(MAX_INT32, /* unsigned = */ true);
        const ento::SVal notTooLarge = builder.evalBinOp(
            state, clang::BO_LE, arg, maxInt32, builder.getConditionType());
        if (notTooLarge.isConstant(1))
            return JSValueTag::Int32;
        if (notTooLarge.isConstant(0))
            return JSValueTag::Double;
        return JSValueTag::Number;
    }
    // signed integer
    if (ast.getTypeInfo(paramType).Width <= 32)
        return JSValueTag::Int32;
    const ento::SVal maxInt32 =
        builder.makeIntValWithPtrWidth(MAX_INT32, /* unsigned = */ false);
    const ento::SVal notTooLarge = builder.evalBinOp(
        state, clang::BO_LE, arg, maxInt32, builder.getConditionType());
    const ento::SVal minInt32 =
        builder.makeIntValWithPtrWidth(MIN_INT32, /* unsigned = */ false);
    const ento::SVal notTooSmall = builder.evalBinOp(
        state, clang::BO_GE, arg, minInt32, builder.getConditionType());
    if (notTooLarge.isConstant(1) && notTooSmall.isConstant(1))
        return JSValueTag::Int32;
    if ((notTooLarge.isConstant(0) && notTooSmall.isConstant(1)) ||
        (notTooLarge.isConstant(1) && notTooSmall.isConstant(0)))
        return JSValueTag::Double;
    return JSValueTag::Number;
}

JSValueTag JSValueModeling::tagForObjectOrNullParameter(
    ento::CheckerContext& cx, const ento::CallEvent& call,
    size_t arg_ix) const {
    ento::ProgramStateRef state = cx.getState();
    const ento::SVal& arg = call.getArgSVal(arg_ix);
    const ento::ConditionTruthVal argIsNull = state->isNull(arg);
    if (argIsNull.isConstrainedTrue())
        return JSValueTag::Null;
    if (argIsNull.isConstrainedFalse())
        return JSValueTag::Object;
    return JSValueTag::ObjectOrNull;
}

void JSValueModeling::checkArgumentNonNull(ento::CheckerContext& cx,
                                           const ento::CallEvent& call) const {
    ento::ProgramStateRef state = cx.getState();
    ento::ConditionTruthVal isNonNull = state->isNonNull(call.getArgSVal(0));
    if (isNonNull.isConstrainedFalse()) {
        ento::ExplodedNode* node = cx.generateErrorNode();
        reportNullArgument(cx, call, node, 0);
    } else if (!isNonNull.isConstrainedTrue()) {
        ento::ExplodedNode* node = cx.generateNonFatalErrorNode(state);
        reportNullArgument(cx, call, node, 0);
    }
}

static void modelJSRootedValueConstructor(ento::CheckerContext& cx,
                                          const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* ctor = llvm::cast<ento::CXXConstructorCall>(&call);
    Key jsval = keyForSVal(ctor->getCXXThisVal());

    const clang::CXXRecordDecl* rootedValue = ctor->getDecl()->getParent();
    Key wrappedKey =
        keyForTemplatedSVal(state, rootedValue, ctor->getCXXThisVal());
    state = createWrap(state, jsval, wrappedKey);

    if (ctor->getNumArgs() == 1) {
        // default is UndefinedValue
        state = mutateKey(state, wrappedKey, JSValueTag::Undefined);
        debug(2) << call << " (1 argument) undefined @" << jsval << " from "
                 << wrappedKey << "\n";
    } else if (ctor->getNumArgs() == 2) {
        // 2 arguments. Don't use the wrapped key mechanism here. The JS::Value
        // argument may be a temporary expression. As the key, we actually do
        // want to use this->ptr.
        Key jsvalArg = keyForSVal(call.getArgSVal(1));
        state = assumeKeySameAs(state, jsval, jsvalArg);
        debug(2) << call << " (" << ctor->getNumArgs() << " arguments) "
                 << tagForKey(state, jsvalArg) << " @" << jsval << " from "
                 << wrappedKey << ", copied from " << jsvalArg << "\n";
    } else {
        debug(1) << call << " (" << ctor->getNumArgs() << " arguments)\n";
        return;
    }

    cx.addTransition(state);
}

static void modelJSRootedValueAssignment(
    ento::CheckerContext& cx, const ento::CXXMemberOperatorCall& call) {
    ento::ProgramStateRef state = cx.getState();

    Key jsval = keyForSVal(call.getCXXThisVal());

    // Don't use the wrapped key mechanism here. The JS::Value argument may be
    // a temporary expression. As the key, we do actually want to use this->ptr.
    Key jsvalArg = keyForSVal(call.getArgSVal(0));

    const auto* rootedValue =
        llvm::cast<clang::CXXRecordDecl>(call.getDecl()->getParent());
    Key wrappedKey =
        keyForTemplatedSVal(state, rootedValue, call.getCXXThisVal());
    state = createWrap(state, jsval, wrappedKey);
    state = assumeKeySameAs(state, jsval, jsvalArg);
    cx.addTransition(state);

    debug(2) << call << " assign " << tagForKey(state, jsvalArg) << " @"
             << jsval << " from @" << wrappedKey << ", copied from " << jsvalArg
             << "\n";
}

static void modelFromMarkedLocation(ento::CheckerContext& cx,
                                    const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    Key jsval = keyForSVal(call.getReturnValue());

    // Argument is a JS::Value*, use the wrapped key mechanism.
    const clang::QualType ty = call.parameters()[0]->getType();
    const clang::CXXRecordDecl* klass = ty->getPointeeCXXRecordDecl();
    if (!klass)
        return;
    Key jsvalArg = keyForSVal(call.getArgSVal(0));
    cx.addTransition(createWrap(state, jsval, jsvalArg));

    debug(1) << "fromMarkedLocation() " << tagForKey(state, jsvalArg) << " @"
             << jsval << " from " << jsvalArg << "\n";
}

static void modelJSHandleValueConstructor(ento::CheckerContext& cx,
                                          const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* ctor = llvm::cast<ento::CXXConstructorCall>(&call);
    Key jsval = keyForSVal(ctor->getCXXThisVal());

    // If we are constructing the JS::HandleValue implicitly from a reference to
    // a JS::RootedValue or other JS::HandleValue, then use the wrapped key
    // mechanism.
    const clang::QualType ty = ctor->parameters()[0]->getType();
    const clang::CXXRecordDecl* klass = ty->getPointeeCXXRecordDecl();
    if (!klass || (!isTemplatedJSValue(klass, "Rooted") &&
                   !isTemplatedJSValue(klass, "Handle")))
        return;
    Key jsvalArg = keyForSVal(call.getArgSVal(0));
    cx.addTransition(createWrap(state, jsval, jsvalArg));

    debug(2) << "Construct JS::HandleValue " << tagForKey(state, jsvalArg)
             << " @" << jsval << " from @" << jsvalArg << "\n";
}

static void modelJSMutableHandleValueConstructor(ento::CheckerContext& cx,
                                                 const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* ctor = llvm::cast<ento::CXXConstructorCall>(&call);
    Key jsval = keyForSVal(ctor->getCXXThisVal());

    if (ctor->getNumArgs() != 1)
        return;

    // We are constructing the JS::MutableHandleValue implicitly from a
    // pointer to a JS::RootedValue. Use the wrapped key mechanism.
    const clang::QualType ty = ctor->parameters()[0]->getType();
    const clang::CXXRecordDecl* klass = ty->getPointeeCXXRecordDecl();
    if (!klass || !isTemplatedJSValue(klass, "Rooted"))
        return;
    Key jsvalArg = keyForSVal(call.getArgSVal(0));
    cx.addTransition(createWrap(state, jsval, jsvalArg));

    debug(2) << "Construct JS::MutableHandleValue "
             << tagForKey(state, jsvalArg) << " @" << jsval << " from "
             << jsvalArg << "\n";
}

void JSValueModeling::modelEqualityFunction(ento::CheckerContext& cx,
                                            const ento::CallEvent& call) const {
    ento::ProgramStateRef state = cx.getState();
    clang::ASTContext& ast = cx.getASTContext();
    ento::SValBuilder& builder = cx.getSValBuilder();

    Key jsval1 = keyForSVal(call.getArgSVal(1));
    const JSValueTag tag1 = tagForKey(state, jsval1);
    Key jsval2 = keyForSVal(call.getArgSVal(2));
    const JSValueTag tag2 = tagForKey(state, jsval2);

    debug(2) << call << "(" << tag1 << ", " << tag2 << ")\n";

    bool isSingleBit1 = std::bitset<16>(tag1).count() == 1;
    bool isSingleBit2 = std::bitset<16>(tag2).count() == 1;

    const ento::SVal outParamPointer = call.getArgSVal(3);
    const auto outParam = state->getSVal(outParamPointer.getAsRegion(), ast.BoolTy).getAs<ento::DefinedOrUnknownSVal>();
    if (!outParam)
        return;

    if ((tag1 & tag2) == 0) {
        ento::ExplodedNode* node = cx.generateNonFatalErrorNode(state);
        reportImpossibleCompare(cx, call, node, jsval1, tag1, jsval2, tag2);
        state = state->assume(*outParam, false);
        cx.addTransition(state);
        return;
    }

    // If return value unknown:
    // - Assume a state where:
    //   - *arg4 is true
    //   - Both values have the intersection of tag bits
    // - Assume a state where:
    //   - *arg4 is false
    //   - If one value has a single bit tagged which is null or undefined, that
    //     bit is removed from the other value
    ento::ProgramStateRef sameState, diffState;
    std::tie(sameState, diffState) = state->assume(*outParam);
    if (sameState) {
        sameState = assumeKey(sameState, jsval1, JSValueTag(tag1 & tag2));
        sameState = assumeKeySameAs(sameState, jsval2, jsval1);
        sameState = sameState->bindLoc(*outParam, builder.makeTruthVal(true),
                                       cx.getLocationContext());
        cx.addTransition(sameState);
    }
    if (diffState) {
        if (isSingleBit1)
            diffState = assumeKey(diffState, jsval2, JSValueTag(tag2 & ~tag1));
        if (isSingleBit2)
            diffState = assumeKey(diffState, jsval1, JSValueTag(tag1 & ~tag2));
        diffState = diffState->bindLoc(*outParam, builder.makeTruthVal(false),
                                       cx.getLocationContext());
        cx.addTransition(diffState);
    }
}

static void reinitValue(ento::CheckerContext& cx, const ento::CallEvent& call,
                        size_t arg_ix) {
    Key jsval = keyForSVal(call.getArgSVal(arg_ix));
    cx.addTransition(mutateKey(cx.getState(), jsval, JSValueTag::Unknown));
}

static void modelJSValueConstructor(ento::CheckerContext& cx,
                                    const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* ctor = llvm::cast<ento::CXXConstructorCall>(&call);
    Key jsval = keyForSVal(ctor->getCXXThisVal());

    if (ctor->getNumArgs() == 0) {
        // Default after construction is UndefinedValue
        state = mutateKey(state, jsval, JSValueTag::Undefined);
    } else {
        // 1 argument
        const clang::Expr* arg1 = ctor->getArgExpr(0);
        const clang::QualType arg1Type = arg1->getType();
        const clang::CXXRecordDecl* rec = arg1Type->getAsCXXRecordDecl();
        if (rec && isJSValue(rec)) {
            Key jsvalArg = keyForSVal(call.getArgSVal(0));
            debug(2) << "Construct " << call << " @" << jsval << " from "
                     << jsvalArg << ", " << tagForKey(state, jsvalArg) << "\n";
            state = assumeKeySameAs(state, jsval, jsvalArg);
        } else if (arg1Type->isUnsignedIntegerType()) {
            const JSValueTag tag = tagForRawBits(cx, ctor->getArgSVal(0));
            debug(2) << "Construct " << call << " @" << jsval << ", " << tag
                     << ", from raw bits\n";
            state = mutateKey(state, jsval, tag);
        } else if (arg1Type->isFloatingType()) {
            debug(2) << "Construct " << call << " @" << jsval
                     << " from double\n";
            state = mutateKey(state, jsval, JSValueTag::Double);
        }
    }
    cx.addTransition(state);
}

static void modelJSValueDestructor(ento::CheckerContext& cx,
                                   const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* dtor = llvm::cast<ento::CXXDestructorCall>(&call);
    Key jsval = keyForSVal(dtor->getCXXThisVal());

    debug(2) << call << " " << tagForKey(state, jsval) << " @" << jsval << "\n";

    cx.addTransition(deleteKey(state, jsval));
}

static void propagateWrappedPtr(ento::CheckerContext& cx,
                                const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());
    if (state->get<Wrap>(jsval))
        return;

    Key wrappedKey = keyForSVal(call.getReturnValue());

    debug(1) << call << ": Propagating " << tagForKey(state, jsval) << " @"
             << jsval << " to " << wrappedKey << "\n";

    state = wrapAndMoveAssociationsTo(state, jsval, wrappedKey);
    cx.addTransition(state);
}

static void modelSwap(ento::CheckerContext& cx, const ento::CallEvent& call) {
    ento::ProgramStateRef state = cx.getState();

    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval1 = keyForSVal(meth->getCXXThisVal());
    Key jsval2 = keyForSVal(call.getArgSVal(0));
    state = swapKeys(state, jsval1, jsval2);
    cx.addTransition(state);
}

void JSValueModeling::modelCheck(ento::CheckerContext& cx,
                                 const ento::CallEvent& call,
                                 const JSValueTag checkTag) const {
    ento::ProgramStateRef state = cx.getState();
    ento::SValBuilder& builder = cx.getSValBuilder();

    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());
    const JSValueTag tag = tagForKey(state, jsval);

    debug(2) << call << " Check @" << jsval << ", " << tag << ", is "
             << checkTag << "\n";

    if (!(tag & checkTag)) {
        state = state->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(false));
        cx.addTransition(state);
    } else if ((tag & checkTag) < tag) {
        const auto retval =
            call.getReturnValue().getAs<ento::DefinedOrUnknownSVal>();
        if (!retval)
            return;

        ento::ProgramStateRef isState, isntState;
        std::tie(isState, isntState) = state->assume(*retval);
        if (isState) {
            isState = assumeKey(isState, jsval, JSValueTag(tag & checkTag));
            isState =
                isState->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                  builder.makeTruthVal(true));
            cx.addTransition(isState);
        }
        if (isntState) {
            isntState =
                assumeKey(isntState, jsval, JSValueTag(tag & ~checkTag));
            isntState = isntState->BindExpr(call.getOriginExpr(),
                                            cx.getLocationContext(),
                                            builder.makeTruthVal(false));
            cx.addTransition(isntState);
        }
    } else {
        state = state->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(true));
        cx.addTransition(state);
    }
}

// Checking a specific value, e.g. isInt32(arg), isTrue(), isFalse(), means the
// tag is that tag in the true branch, but says nothing about the false branch
void JSValueModeling::modelCheckSpecificValue(ento::CheckerContext& cx,
                                              const ento::CallEvent& call,
                                              const JSValueTag checkTag) const {
    ento::ProgramStateRef state = cx.getState();
    ento::SValBuilder& builder = cx.getSValBuilder();

    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());
    const JSValueTag tag = tagForKey(state, jsval);

    debug(2) << "Check " << tag << " is " << checkTag << " @" << jsval << "\n";

    if (!(tag & checkTag)) {
        state = state->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(false));
        cx.addTransition(state);
    } else {
        const auto retval =
            call.getReturnValue().getAs<ento::DefinedOrUnknownSVal>();
        if (!retval)
            return;

        ento::ProgramStateRef isState, isntState;
        std::tie(isState, isntState) = state->assume(*retval);
        if (isState) {
            isState = assumeKey(isState, jsval, checkTag);
            isState =
                isState->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                  builder.makeTruthVal(true));
            cx.addTransition(isState);
        }
        if (isntState) {
            isntState = isntState->BindExpr(call.getOriginExpr(),
                                            cx.getLocationContext(),
                                            builder.makeTruthVal(false));
            cx.addTransition(isntState);
        }
    }
}

static void modelJSValueAssignment(ento::CheckerContext& cx,
                                   const ento::CXXMemberOperatorCall& call) {
    Key jsval = keyForSVal(call.getCXXThisVal());
    Key jsvalOther = keyForSVal(call.getArgSVal(0));
    cx.addTransition(assumeKeySameAs(cx.getState(), jsval, jsvalOther));

    debug(2) << call << " assign @" << jsvalOther << ", "
             << tagForKey(cx.getState(), jsvalOther) << ", to @" << jsval
             << "\n";
}

void JSValueModeling::modelEqualityOperator(
    ento::CheckerContext& cx, const ento::CXXMemberOperatorCall& call,
    bool inverse) const {
    ento::ProgramStateRef state = cx.getState();
    ento::SValBuilder& builder = cx.getSValBuilder();

    Key jsval1 = keyForSVal(call.getCXXThisVal());
    const JSValueTag tag1 = tagForKey(state, jsval1);
    Key jsval2 = keyForSVal(call.getArgSVal(0));
    const JSValueTag tag2 = tagForKey(state, jsval2);

    debug(2) << tag1 << (inverse ? " != " : " == ") << tag2 << "\n";

    bool isSingleBit1 = std::bitset<16>(tag1).count() == 1;
    bool isSingleBit2 = std::bitset<16>(tag2).count() == 1;

    // Return false if values have no overlapping types
    if ((tag1 & tag2) == 0) {
        ento::ExplodedNode* node = cx.generateNonFatalErrorNode(state);
        reportImpossibleCompare(cx, call, node, jsval1, tag1, jsval2, tag2);
        state = state->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(inverse));
        cx.addTransition(state);
        return;
    }

    // If return value unknown:
    // - Assume a true state where both values have the intersection of tag bits
    // - Assume a false state where:
    //   - If one value has a single bit tagged:
    //     - That bit is removed from the other value
    const auto retval =
        call.getReturnValue().getAs<ento::DefinedOrUnknownSVal>();
    if (!retval)
        return;

    ento::ProgramStateRef sameState, diffState;
    std::tie(sameState, diffState) = state->assume(*retval);
    if (sameState) {
        sameState = assumeKey(sameState, jsval1, JSValueTag(tag1 & tag2));
        sameState = assumeKeySameAs(sameState, jsval2, jsval1);
        sameState =
            sameState->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(!inverse));
        cx.addTransition(sameState);
    }
    if (diffState) {
        if (isSingleBit1)
            diffState = assumeKey(diffState, jsval2, JSValueTag(tag2 & ~tag1));
        if (isSingleBit2)
            diffState = assumeKey(diffState, jsval1, JSValueTag(tag1 & ~tag2));
        diffState =
            diffState->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(inverse));
        cx.addTransition(diffState);
    }
}

void modelIsArrayObject(ento::CheckerContext& cx, const ento::CallEvent& call) {
    // Make sure this isn't the overload with JS::HandleObject
    if (!isTemplatedJSValue(
            call.parameters()[1]->getType()->getAsCXXRecordDecl(), "Handle"))
        return;

    ento::ProgramStateRef state = cx.getState();
    clang::ASTContext& ast = cx.getASTContext();
    ento::SValBuilder& builder = cx.getSValBuilder();

    const ento::MemRegion* key = keyForSVal(call.getArgSVal(1));
    const JSValueTag tag = tagForKey(state, key);

    debug(1) << call << "(" << tag << " @" << key << ")\n";

    const ento::SVal outParamPointer = call.getArgSVal(2);
    const auto outParam =
        state->getSVal(outParamPointer.getAsRegion(), ast.BoolTy)
            .getAs<ento::DefinedOrUnknownSVal>();
    if (!outParam)
        return;

    if ((tag & JSValueTag::Object) == 0) {
        state = state->assume(*outParam, false);
        cx.addTransition(state);
        return;
    }

    ento::ProgramStateRef isState, isntState;
    std::tie(isState, isntState) = state->assume(*outParam);
    if (isState) {
        isState = assumeKey(isState, key, JSValueTag::Object);
        isState = isState->bindLoc(*outParam, builder.makeTruthVal(true),
                                   cx.getLocationContext());
        debug(1) << "Assume @" << key << " is an object\n";
        cx.addTransition(isState);
    }
    if (isntState) {
        isntState = isntState->bindLoc(*outParam, builder.makeTruthVal(false),
                                       cx.getLocationContext());
        cx.addTransition(isntState);
    }
}

void modelGetReservedSlot(ento::CheckerContext& cx,
                          const ento::CallEvent& call) {
    const clang::Expr* slotExpr = call.getArgExpr(1)->IgnoreParenImpCasts();
    const auto* ref = llvm::dyn_cast<clang::DeclRefExpr>(slotExpr);
    if (!ref)
        return;

    // List well-known values of reserved slots here and give the return value
    // the correct type, assuming that they are called on an object of the
    // right type
    JSValueTag tag = JSValueTag::Unknown;
    std::string fullName = ref->getDecl()->getQualifiedNameAsString();
    if (fullName == "js::detail::TypedArrayLengthSlot")
        tag = JSValueTag::Int32;

    if (tag == JSValueTag::Unknown)
        return;

    // No clue why call.getReturnValue() is UnknownSVal here!
    const ento::DefinedOrUnknownSVal retval =
        cx.getSValBuilder().conjureSymbolVal(nullptr, call.getOriginExpr(),
                                             call.getLocationContext(), 0);

    ento::ProgramStateRef state = cx.getState();
    Key jsval = keyForSVal(retval);
    state = mutateKey(cx.getState(), jsval, tag);
    state = state->BindExpr(call.getOriginExpr(), call.getLocationContext(),
                            retval);
    cx.addTransition(state);

    debug(1) << "GetReservedSlot(" << fullName << "), assuming @" << jsval
             << " " << tag << "\n";
}

static void assumeTypeOfValue(ento::CheckerContext& cx,
                              ento::ProgramStateRef state,
                              const ento::CallEvent& call, Key jsval,
                              const JSValueTag tag, uint64_t jstype) {
    ento::SValBuilder& builder = cx.getSValBuilder();
    const clang::Expr* retExpr = call.getOriginExpr();
    ento::DefinedSVal retval = builder.makeIntVal(jstype, retExpr->getType());

    ento::ProgramStateRef newState = assumeKey(state, jsval, tag);
    newState = newState->BindExpr(retExpr, call.getLocationContext(), retval);

    debug(1) << call << ": Assuming @" << jsval << " " << tag << "\n";

    cx.addTransition(newState);
}

void JSValueModeling::modelTypeOfValue(ento::CheckerContext& cx,
                                       const ento::CallEvent& call) const {
    ento::ProgramStateRef state = cx.getState();
    Key jsval = keyForSVal(call.getArgSVal(1));
    const JSValueTag tag = tagForKey(state, jsval);

    if (!(tag & JSValueTag::Ordinary)) {
        ento::ExplodedNode* node = cx.generateErrorNode(state);
        reportWrongConversion(cx, call, node, jsval, tag);
        return;
    }

    if (tag & JSValueTag::Undefined)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::Undefined),
                          /* JSTYPE_UNDEFINED = */ 0);
    if (tag & JSValueTag::Object) {
        const JSValueTag newTag = JSValueTag(tag & JSValueTag::Object);
        assumeTypeOfValue(cx, state, call, jsval, newTag,
                          /* JSTYPE_OBJECT = */ 1);
        assumeTypeOfValue(cx, state, call, jsval, newTag,
                          /* JSTYPE_FUNCTION = */ 2);
    }
    if (tag & JSValueTag::String)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::String),
                          /* JSTYPE_STRING = */ 3);
    if (tag & JSValueTag::Number)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::Number),
                          /* JSTYPE_NUMBER = */ 4);
    if (tag & JSValueTag::Boolean)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::Boolean),
                          /* JSTYPE_BOOLEAN = */ 5);
    if (tag & JSValueTag::Null)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::Null),
                          /* JSTYPE_NULL = */ 6);
    if (tag & JSValueTag::Symbol)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::Symbol),
                          /* JSTYPE_SYMBOL = */ 7);
    if (tag & JSValueTag::BigInt)
        assumeTypeOfValue(cx, state, call, jsval,
                          JSValueTag(tag & JSValueTag::BigInt),
                          /* JSTYPE_BIGINT = */ 8);
}

bool JSValueModeling::evalSameType(ento::CheckerContext& cx,
                                   const ento::CallEvent& call) const {
    ento::ProgramStateRef state = cx.getState();
    ento::SValBuilder& builder = cx.getSValBuilder();

    Key jsval1 = keyForSVal(call.getArgSVal(0));
    const JSValueTag tag1 = tagForKey(state, jsval1);
    Key jsval2 = keyForSVal(call.getArgSVal(1));
    const JSValueTag tag2 = tagForKey(state, jsval2);

    debug(1) << "JS::SameType(" << tag1 << ", " << tag2 << ")\n";

    bool isSingleBit1 = std::bitset<16>(tag1).count() == 1;
    bool isSingleBit2 = std::bitset<16>(tag2).count() == 1;

    // Return true if:
    // - Values have both the same single bit tagged, or
    // - One is magic value and the other is magic int, or
    // - Both are private (tag < JSVAL_TAG_CLEAR)
    if (isSingleBit1 && isSingleBit2 &&
        (tag1 == tag2 || (tag1 | tag2) == JSValueTag::Magic ||
         (tag1 | tag2) == JSValueTag::Private)) {
        state = state->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(true));
        ento::ExplodedNode* node = cx.generateNonFatalErrorNode(state);
        reportUnnecessaryCompare(cx, call, node, jsval1, jsval2,
                                 JSValueTag(tag1 | tag2));
        return true;
    }

    // Return false if the values have no overlapping tags
    if ((tag1 & tag2) == 0) {
        state = state->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(false));
        ento::ExplodedNode* node = cx.generateNonFatalErrorNode(state);
        reportImpossibleCompare(cx, call, node, jsval1, tag1, jsval2, tag2);
        return true;
    }

    // If return value unknown:
    // - Assume a true state where both values have the intersection of tag bits
    // - Assume a false state where:
    //   - If one value has a single bit tagged:
    //     - That bit is removed from the other value
    const auto retval =
        call.getReturnValue().getAs<ento::DefinedOrUnknownSVal>();
    if (!retval)
        return false;

    ento::ProgramStateRef sameState, diffState;
    std::tie(sameState, diffState) = state->assume(*retval);
    if (sameState) {
        sameState = assumeKey(sameState, jsval1, JSValueTag(tag1 & tag2));
        sameState = assumeKeySameAs(sameState, jsval2, jsval1);
        sameState =
            sameState->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(true));
        cx.addTransition(sameState);
    }
    if (diffState) {
        if (isSingleBit1)
            diffState = assumeKey(diffState, jsval2, JSValueTag(tag2 & ~tag1));
        if (isSingleBit2)
            diffState = assumeKey(diffState, jsval1, JSValueTag(tag1 & ~tag2));
        diffState =
            diffState->BindExpr(call.getOriginExpr(), cx.getLocationContext(),
                                builder.makeTruthVal(false));
        cx.addTransition(diffState);
    }
    return true;
}

bool JSValueModeling::evalConversionPrivate(ento::CheckerContext& cx,
                                            const ento::CallEvent& call,
                                            const JSValueTag checkTag) const {
    const auto* meth = llvm::cast<ento::CXXInstanceCall>(&call);
    Key jsval = keyForSVal(meth->getCXXThisVal());
    const JSValueTag tag = tagForKey(cx.getState(), jsval);

    debug(2) << call << ": Convert " << tag << " @" << jsval << " to "
             << checkTag << "\n";

    if (!(tag & checkTag)) {
        ento::ExplodedNode* node = cx.generateErrorNode();
        reportWrongConversion(cx, call, node, jsval, tag);
    }
    return true;
}

void JSValueModeling::checkPreCall(const ento::CallEvent& call,
                                   ento::CheckerContext& cx) const {
    debug(3) << "Pre call " << call << "\n";

    const auto* meth =
        llvm::dyn_cast_or_null<clang::CXXMethodDecl>(call.getDecl());
    if (!meth || !isJSValue(meth->getParent()) ||
        call.getKind() != ento::CE_CXXMember)
        return;

    if (call.isCalled({{"JS", "Value", "setNull"}, 0}))
        modelMutator(cx, call, JSValueTag::Null);
    else if (call.isCalled({{"JS", "Value", "setUndefined"}, 0}))
        modelMutator(cx, call, JSValueTag::Undefined);
    else if (call.isCalled({{"JS", "Value", "setInt32"}, 1}))
        modelMutator(cx, call, JSValueTag::Int32);
    else if (call.isCalled({{"JS", "Value", "setDouble"}, 1}))
        modelMutator(cx, call, JSValueTag::Double);
    else if (call.isCalled({{"JS", "Value", "setObject"}, 1}))
        modelMutator(cx, call, JSValueTag::Object);
    else if (call.isCalled({{"JS", "Value", "setBoolean"}, 1}))
        modelMutator(cx, call, JSValueTag::Boolean);
    else if (call.isCalled({{"JS", "Value", "setMagic"}, 1}))
        modelMutator(cx, call, JSValueTag::MagicEnum);
    else if (call.isCalled({{"JS", "Value", "setMagicUint32"}, 1}))
        modelMutator(cx, call, JSValueTag::MagicInt);
    else if (call.isCalled({{"JS", "Value", "setPrivateGCThing"}, 1}))
        modelMutator(cx, call, JSValueTag::PrivateGCThing);
    else if (call.isCalled({{"JS", "Value", "setNaN"}, 0}))
        modelMutator(cx, call, JSValueTag::Double);
    else if (call.isCalled({{"JS", "Value", "setNumber"}, 1}))
        modelMutator(cx, call, tagForNumberParameter(cx, call, 0));
    else if (call.isCalled({{"JS", "Value", "setObjectOrNull"}, 1}))
        modelMutator(cx, call, tagForObjectOrNullParameter(cx, call, 0));
    else if (call.isCalled({{"JS", "Value", "magicUint32"}, 0}))
        modelConversion(cx, call, JSValueTag::Magic);
    else if (call.isCalled({{"JS", "Value", "toInt32"}, 0}))
        modelConversion(cx, call, JSValueTag::Int32);
    else if (call.isCalled({{"JS", "Value", "toDouble"}, 0}))
        modelConversion(cx, call, JSValueTag::Double);
    else if (call.isCalled({{"JS", "Value", "toNumber"}, 0}))
        modelConversion(cx, call, JSValueTag::Number);
    else if (call.isCalled({{"JS", "Value", "toObject"}, 0}))
        modelConversion(cx, call, JSValueTag::Object);
    else if (call.isCalled({{"JS", "Value", "toObjectOrNull"}, 0}))
        modelConversion(cx, call, JSValueTag::ObjectOrNull);
    else if (call.isCalled({{"JS", "Value", "toGCThing"}, 0}))
        modelConversion(cx, call, JSValueTag::GCThing);
    else if (call.isCalled({{"JS", "Value", "toBoolean"}, 0}))
        modelConversion(cx, call, JSValueTag::Boolean);
    else if (call.isCalled({{"JS", "Value", "toString"}, 0}))
        modelConversion(cx, call, JSValueTag::String);
    else if (call.isCalled({{"JS", "Value", "toSymbol"}, 0}))
        modelConversion(cx, call, JSValueTag::Symbol);
    else if (call.isCalled({{"JS", "Value", "toBigInt"}, 0}))
        modelConversion(cx, call, JSValueTag::BigInt);
    // JS::Value::toGCCellPtr() calls toGCThing() inline and can just be modeled
    // by the analyzer
}

void JSValueModeling::checkPostCall(const ento::CallEvent& call,
                                    ento::CheckerContext& cx) const {
    debug(3) << "Post call " << call << "\n";

    const auto* meth =
        llvm::dyn_cast_or_null<clang::CXXMethodDecl>(call.getDecl());
    if (!meth) {
        // Toplevel functions such as JS::NullValue()
        if (call.getKind() != ento::CE_Function)
            return;

        if (call.isCalled({{"JS", "NullValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Null);
        else if (call.isCalled({{"JS", "UndefinedValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Undefined);
        else if (call.isCalled({{"JS", "Int32Value"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::Int32);
        else if (call.isCalled({{"JS", "DoubleValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({{"JS", "CanonicalizedDoubleValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({{"JS", "DoubleNaNValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({"JS_GetNaNValue", 1}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({{"JS", "NaNValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({"JS_GetPositiveInfinityValue", 1}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({"JS_GetNegativeInfinityValue", 1}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({{"JS", "InfinityValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({{"JS", "Float32Value"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::Double);
        else if (call.isCalled({{"JS", "BooleanValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::Boolean);
        else if (call.isCalled({{"JS", "TrueValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Boolean);
        else if (call.isCalled({{"JS", "FalseValue"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Boolean);
        else if (call.isCalled({{"JS", "ObjectValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::Object);
        else if (call.isCalled({{"JS", "MagicValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::MagicEnum);
        else if (call.isCalled({{"JS", "MagicValueUint32"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::MagicInt);
        else if (call.isCalled({{"JS", "PrivateValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::PrivatePointer);
        else if (call.isCalled({{"JS", "PrivateUint32Value"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::PrivateInt);
        else if (call.isCalled({{"JS", "PrivateGCThingValue"}, 1}))
            modelStaticConstructor(cx, call, JSValueTag::PrivateGCThing);
        else if (call.isCalled({{"JS", "StringValue"}, 1})) {
            checkArgumentNonNull(cx, call);
            modelStaticConstructor(cx, call, JSValueTag::String);
        } else if (call.isCalled({{"JS", "SymbolValue"}, 1})) {
            checkArgumentNonNull(cx, call);
            modelStaticConstructor(cx, call, JSValueTag::Symbol);
        } else if (call.isCalled({{"JS", "BigIntValue"}, 1})) {
            checkArgumentNonNull(cx, call);
            modelStaticConstructor(cx, call, JSValueTag::BigInt);
        } else if (call.isCalled({{"JS", "NumberValue"}, 1}))
            modelStaticConstructor(cx, call,
                                   tagForNumberParameter(cx, call, 0));
        else if (call.isCalled({{"JS", "ObjectOrNullValue"}, 1}))
            modelStaticConstructor(cx, call,
                                   tagForObjectOrNullParameter(cx, call, 0));
        else if (call.isCalled({{"JS", "SameValue"}, 4}) ||
                 call.isCalled({{"JS", "StrictlyEqual"}, 4}) ||
                 call.isCalled({{"JS", "LooselyEqual"}, 4}) ||
                 call.isCalled({"JS_SameValue", 4}) ||
                 call.isCalled({"JS_StrictlyEqual", 4}) ||
                 call.isCalled({"JS_LooselyEqual", 4}))
            modelEqualityFunction(cx, call);
        else if (call.isCalled({"JS_GetPropertyById", 4}))
            reinitValue(cx, call, 3);
        else if (call.isCalled({"JS_IsArrayObject", 3}) ||
                 call.isCalled({{"JS", "IsArrayObject"}, 3}))
            modelIsArrayObject(cx, call);
        else if (call.isCalled({{"js", "GetReservedSlot"}, 2}))
            modelGetReservedSlot(cx, call);
        else if (call.isCalled({"JS_TypeOfValue", 2}))
            modelTypeOfValue(cx, call);
        return;
    }

    const clang::CXXRecordDecl* klass = meth->getParent();

    if (isTemplatedJSValue(klass, "Rooted")) {
        switch (call.getKind()) {
            case ento::CE_CXXConstructor:
                modelJSRootedValueConstructor(cx, call);
                return;
            case ento::CE_CXXDestructor:
                modelJSValueDestructor(cx, call);
                return;
            case ento::CE_CXXMemberOperator: {
                const auto* op = llvm::cast<ento::CXXMemberOperatorCall>(&call);
                if (isAssignment(op->getOriginExpr()))
                    modelJSRootedValueAssignment(cx, *op);
                return;
            }
            default:
                return;
        }
    }

    if (isTemplatedJSValue(klass, "Handle")) {
        switch (call.getKind()) {
            case ento::CE_Function:
                if (call.isCalled({{"JS", "Handle", "fromMarkedLocation"}}))
                    modelFromMarkedLocation(cx, call);
                return;
            case ento::CE_CXXConstructor:
                modelJSHandleValueConstructor(cx, call);
                return;
            case ento::CE_CXXDestructor:
                modelJSValueDestructor(cx, call);
                return;
            case ento::CE_CXXMember:
                if (call.isCalled({{"JS", "Handle", "get"}, 0}))
                    propagateWrappedPtr(cx, call);
                return;
            default:
                return;
        }
    }

    if (isTemplatedJSValue(klass, "MutableHandle")) {
        switch (call.getKind()) {
            case ento::CE_Function:
                if (call.isCalled(
                        {{"JS", "MutableHandle", "fromMarkedLocation"}}))
                    modelFromMarkedLocation(cx, call);
                return;
            case ento::CE_CXXConstructor:
                modelJSMutableHandleValueConstructor(cx, call);
                return;
            case ento::CE_CXXDestructor:
                modelJSValueDestructor(cx, call);
                return;
            case ento::CE_CXXMember:
                if (call.isCalled({{"JS", "MutableHandle", "get"}, 0}))
                    propagateWrappedPtr(cx, call);
                return;
            default:
                return;
        }
    }

    if (isJSValueWrappedPtrOperations(klass) &&
        call.isCalled({{"js", "WrappedPtrOperations", "value"}, 0})) {
        propagateWrappedPtr(cx, call);
        return;
    }

    // Check for various other methods
    if (!isJSValue(klass)) {
        if (call.isCalled({{"JS", "CallArgsBase", "calleev"}, 0}))
            modelStaticConstructor(cx, call, JSValueTag::Object);
        return;
    }

    // JS::Value namespace
    switch (call.getKind()) {
        case ento::CE_Function:
            if (call.isCalled({{"JS", "Value", "fromInt32"}, 1}))
                modelStaticConstructor(cx, call, JSValueTag::Int32);
            else if (call.isCalled({{"JS", "Value", "fromDouble"}, 1}))
                modelStaticConstructor(cx, call, JSValueTag::Double);
            // TODO: JS::Value::fromTagAndPayload(JSValueTag, PayloadType)
            else if (call.isCalled({{"JS", "Value", "fromRawBits"}, 1}))
                modelStaticConstructor(cx, call,
                                       tagForRawBits(cx, call.getArgSVal(0)));
            return;
        case ento::CE_CXXConstructor:
            modelJSValueConstructor(cx, call);
            return;
        case ento::CE_CXXDestructor:
            modelJSValueDestructor(cx, call);
            return;
        case ento::CE_CXXMember:
            if (call.isCalled({{"JS", "Value", "swap"}, 1}))
                modelSwap(cx, call);
            else if (call.isCalled({{"JS", "Value", "isNull"}, 0}))
                modelCheck(cx, call, JSValueTag::Null);
            else if (call.isCalled({{"JS", "Value", "isUndefined"}, 0}))
                modelCheck(cx, call, JSValueTag::Undefined);
            else if (call.isCalled({{"JS", "Value", "isInt32"}, 0}))
                modelCheck(cx, call, JSValueTag::Int32);
            else if (call.isCalled({{"JS", "Value", "isDouble"}, 0}))
                modelCheck(cx, call, JSValueTag::Double);
            else if (call.isCalled({{"JS", "Value", "isNumber"}, 0}))
                modelCheck(cx, call, JSValueTag::Number);
            else if (call.isCalled({{"JS", "Value", "isString"}, 0}))
                modelCheck(cx, call, JSValueTag::String);
            else if (call.isCalled({{"JS", "Value", "isSymbol"}, 0}))
                modelCheck(cx, call, JSValueTag::Symbol);
            else if (call.isCalled({{"JS", "Value", "isBigInt"}, 0}))
                modelCheck(cx, call, JSValueTag::BigInt);
            else if (call.isCalled({{"JS", "Value", "isObject"}, 0}))
                modelCheck(cx, call, JSValueTag::Object);
            else if (call.isCalled({{"JS", "Value", "isBoolean"}, 0}))
                modelCheck(cx, call, JSValueTag::Boolean);
            else if (call.isCalled({{"JS", "Value", "isPrivateGCThing"}, 0}))
                modelCheck(cx, call, JSValueTag::PrivateGCThing);
            else if (call.isCalled({{"JS", "Value", "isPrimitive"}, 0}))
                modelCheck(cx, call, JSValueTag::Primitive);
            else if (call.isCalled({{"JS", "Value", "isGCThing"}, 0}))
                modelCheck(cx, call, JSValueTag::GCThing);
            else if (call.isCalled({{"JS", "Value", "isMagic"}, 0}))
                modelCheck(cx, call, JSValueTag::Magic);
            else if (call.isCalled({{"JS", "Value", "isInt32"}, 1}))
                modelCheckSpecificValue(cx, call, JSValueTag::Int32);
            else if (call.isCalled({{"JS", "Value", "isTrue"}, 0}))
                modelCheckSpecificValue(cx, call, JSValueTag::Boolean);
            else if (call.isCalled({{"JS", "Value", "isFalse"}, 0}))
                modelCheckSpecificValue(cx, call, JSValueTag::Boolean);
            else if (call.isCalled({{"JS", "Value", "isMagic"}, 1}))
                modelCheckSpecificValue(cx, call, JSValueTag::MagicEnum);
            // These are modeled in PreCall
            else if (call.isCalled({{"JS", "Value", "toString"}, 0}) ||
                     call.isCalled({{"JS", "Value", "toSymbol"}, 0}) ||
                     call.isCalled({{"JS", "Value", "toBigInt"}, 0}))
                assumeNonNullReturn(cx, call);
            else if (call.isCalled({{"JS", "Value", "toObjectOrNull"}, 0}))
                assumeObjectOrNullReturn(cx, call);

            // TODO: value.traceKind()
            // TODO: value.payloadAsRawUint32()
            // TODO: value.asRawBits()
            // TODO: value.type()
            return;
        case ento::CE_CXXMemberOperator: {
            const auto* op = llvm::cast<ento::CXXMemberOperatorCall>(&call);
            const clang::CXXOperatorCallExpr* expr = op->getOriginExpr();
            if (isAssignment(expr))
                modelJSValueAssignment(cx, *op);
            else if (expr->getOperator() == clang::OO_EqualEqual)
                modelEqualityOperator(cx, *op, /* inverse = */ false);
            else if (expr->getOperator() == clang::OO_ExclaimEqual)
                modelEqualityOperator(cx, *op, /* inverse = */ true);
            return;
        }
        default:;
    }
}

bool JSValueModeling::evalCall(const ento::CallEvent& call,
                               ento::CheckerContext& cx) const {
    const auto* meth =
        llvm::dyn_cast_or_null<clang::CXXMethodDecl>(call.getDecl());
    if (!meth) {
        // JS::SameType is inlined and contains a call to almost every type
        // checking method, which messes with the state, so it needs to be
        // replaced in evalCall.
        if (call.isCalled({{"JS", "SameType"}, 2}))
            return evalSameType(cx, call);
        return false;
    }

    const clang::CXXRecordDecl* klass = meth->getParent();

    // Ignore JS::GCPolicy<JS::Value>::isValid() as it is called in the
    // constructor of JS::Rooted and inlines isGCThing(), which clutters the
    // path diagnostic notes.
    if (isTemplatedJSValue(klass, "GCPolicy") &&
        call.isCalled({{"JS", "GCPolicy", "isValid"}, 1}))
        return true;

    if (!isJSValue(klass) || call.getKind() != ento::CE_CXXMember)
        return false;

    // setPrivate and setPrivateUint32 need to be replaced in evalCall,
    // because they assert isDouble() and isInt32() respectively.
    if (call.isCalled({{"JS", "Value", "setPrivate"}, 1})) {
        modelMutator(cx, call, JSValueTag::PrivatePointer);
        return true;
    }
    if (call.isCalled({{"JS", "Value", "setPrivateUint32"}, 1})) {
        modelMutator(cx, call, JSValueTag::PrivateInt);
        return true;
    }
    // FIXME: These should be in checkPreCall, but that somehow messes up the
    // checkArgumentNonNull so that it only warns when the argument is a literal
    // null, not a pointer that may be null
    if (call.isCalled({{"JS", "Value", "setString"}, 1})) {
        checkArgumentNonNull(cx, call);
        modelMutator(cx, call, JSValueTag::String);
        return true;
    }
    if (call.isCalled({{"JS", "Value", "setSymbol"}, 1})) {
        checkArgumentNonNull(cx, call);
        modelMutator(cx, call, JSValueTag::Symbol);
        return true;
    }
    if (call.isCalled({{"JS", "Value", "setBigInt"}, 1})) {
        checkArgumentNonNull(cx, call);
        modelMutator(cx, call, JSValueTag::BigInt);
        return true;
    }

    // Some combined type checker methods need to be replaced in evalCall,
    // because e.g. isObjectOrNull() consists of isObject() || isNull(), and
    // that messes with the state.
    if (call.isCalled({{"JS", "Value", "isNullOrUndefined"}, 0})) {
        modelCheck(cx, call, JSValueTag::NullOrUndefined);
        return true;
    }
    if (call.isCalled({{"JS", "Value", "isObjectOrNull"}, 0})) {
        modelCheck(cx, call, JSValueTag::ObjectOrNull);
        return true;
    }
    if (call.isCalled({{"JS", "Value", "isNumeric"}, 0})) {
        modelCheck(cx, call, JSValueTag::Numeric);
        return true;
    }

    // Some typed payload extractor methods need to be replaced in evalCall
    // because in the SpiderMonkey code they use a different type tag. For
    // example, toPrivate() uses the 'double' tag to store the pointer
    // internally, but we want to be able to distinguish these states.
    if (call.isCalled({{"JS", "Value", "whyMagic"}, 0})) {
        // uses magicUint32() inline
        modelConversion(cx, call, JSValueTag::MagicEnum);
        return true;
    }

    // In addition to using the double and int32 tags respectively, these
    // payload extractors should not produce a warning if they are called on
    // unknown JS::Values, only on JS::Values that are known for certain not to
    // be private values. (This is because there are no isPrivate() methods to
    // check whether the JS::Value you have is opaque.)
    if (call.isCalled({{"JS", "Value", "toPrivate"}, 0}))
        return evalConversionPrivate(cx, call, JSValueTag::PrivatePointer);
    if (call.isCalled({{"JS", "Value", "toPrivateUint32"}, 0}))
        return evalConversionPrivate(cx, call, JSValueTag::PrivateInt);

    return false;
}
