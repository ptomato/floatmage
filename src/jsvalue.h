/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stddef.h>

#include <memory>

#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState_Fwd.h"

namespace clang {
namespace ento {
class CXXMemberOperatorCall;
class CallEvent;
class CheckerContext;
class ExplodedNode;
class MemRegion;
}  // namespace ento
}  // namespace clang

namespace check = clang::ento::check;
namespace ento = clang::ento;
namespace eval = clang::ento::eval;

enum JSValueTag {
    Double = 0x1,
    Int32 = 0x2,
    Number = Double | Int32,
    Boolean = 0x4,
    Undefined = 0x8,
    Null = 0x10,
    NullOrUndefined = Null | Undefined,
    MagicEnum = 0x20,
    MagicInt = 0x40,
    Magic = MagicEnum | MagicInt,
    String = 0x80,
    Symbol = 0x100,
    PrivateGCThing = 0x200,
    BigInt = 0x400,
    Numeric = Number | BigInt,
    Primitive = Double | Int32 | Boolean | Undefined | Null | Magic | String |
                Symbol | PrivateGCThing | BigInt,
    Object = 0x800,
    ObjectOrNull = Object | Null,
    Ordinary = Double | Int32 | Boolean | Undefined | Null | String | Symbol |
               BigInt | Object,
    GCThing = String | Symbol | PrivateGCThing | BigInt | Object,
    PrivatePointer = 0x1000,
    PrivateInt = 0x2000,
    Private = PrivatePointer | PrivateInt,
    Unknown = Double | Int32 | Boolean | Undefined | Null | Magic | String |
              Symbol | PrivateGCThing | BigInt | Object | PrivatePointer |
              PrivateInt
};
static_assert(JSValueTag::Unknown <= 0xffff,
              "Need to update the bitsets in JSValueModeling");

class JSValueModeling
    : public ento::Checker<check::PreCall, check::PostCall, eval::Call> {
    std::unique_ptr<ento::BugType> m_wrongValueTag;
    std::unique_ptr<ento::BugType> m_unnecessaryCompare;
    std::unique_ptr<ento::BugType> m_impossibleCompare;
    std::unique_ptr<ento::BugType> m_nullArgument;

    void reportWrongConversion(ento::CheckerContext& cx,
                               const ento::CallEvent& call,
                               ento::ExplodedNode* node,
                               const ento::MemRegion* key,
                               const JSValueTag tag) const;
    void reportImpossibleCompare(ento::CheckerContext& cx,
                                 const ento::CallEvent& call,
                                 ento::ExplodedNode* node,
                                 const ento::MemRegion* key1,
                                 const JSValueTag tag1,
                                 const ento::MemRegion* key2,
                                 const JSValueTag tag2) const;
    void reportUnnecessaryCompare(ento::CheckerContext& cx,
                                  const ento::CallEvent& call,
                                  ento::ExplodedNode* node,
                                  const ento::MemRegion* key1,
                                  const ento::MemRegion* key2,
                                  const JSValueTag tag) const;
    void reportNullArgument(ento::CheckerContext& cx,
                            const ento::CallEvent& call,
                            ento::ExplodedNode* node, size_t arg_ix) const;

    JSValueTag tagForObjectOrNullParameter(ento::CheckerContext& cx,
                                           const ento::CallEvent& call,
                                           size_t arg_ix) const;
    void checkArgumentNonNull(ento::CheckerContext& cx,
                              const ento::CallEvent& call) const;

    void modelConversion(ento::CheckerContext& cx, const ento::CallEvent& call,
                         const JSValueTag checkTag) const;
    void modelEqualityFunction(ento::CheckerContext& cx,
                               const ento::CallEvent& call) const;
    void modelCheck(ento::CheckerContext& cx, const ento::CallEvent& call,
                    const JSValueTag checkTag) const;
    void modelCheckSpecificValue(ento::CheckerContext& cx,
                                 const ento::CallEvent& call,
                                 const JSValueTag checkTag) const;
    void modelEqualityOperator(ento::CheckerContext& cx,
                               const ento::CXXMemberOperatorCall& call,
                               bool inverse) const;
    void modelTypeOfValue(ento::CheckerContext& cx,
                          const ento::CallEvent& call) const;
    bool evalSameType(ento::CheckerContext& cx,
                      const ento::CallEvent& call) const;
    bool evalConversionPrivate(ento::CheckerContext& cx,
                               const ento::CallEvent& call,
                               const JSValueTag checkTag) const;

 public:
    explicit JSValueModeling();

    void checkPreCall(const ento::CallEvent& call,
                      ento::CheckerContext& cx) const;
    void checkPostCall(const ento::CallEvent& call,
                       ento::CheckerContext& cx) const;
    bool evalCall(const ento::CallEvent& call, ento::CheckerContext& cx) const;
};
