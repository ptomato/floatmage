/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include <algorithm>  // for swap
#include <memory>     // for make_unique, unique_ptr
#include <string>
#include <utility>  // for pair, move
#include <vector>  // for vector

#include "clang/AST/ASTConsumer.h"                           // for ASTConsumer
#include "clang/Frontend/CompilerInstance.h"                 // for Compiler...
#include "clang/Frontend/FrontendAction.h"                   // for ASTFront...
#include "clang/Frontend/FrontendOptions.h"                  // for Frontend...
#include "clang/Frontend/MultiplexConsumer.h"                // for Multiple...
#include "clang/StaticAnalyzer/Core/AnalyzerOptions.h"       // for Analyzer...
#include "clang/StaticAnalyzer/Frontend/AnalysisConsumer.h"  // for Analysis...
#include "clang/StaticAnalyzer/Frontend/CheckerRegistry.h"   // for CheckerR...
#include "clang/Tooling/ArgumentsAdjusters.h"                // for CommandL...
#include "clang/Tooling/CommonOptionsParser.h"               // for CommonOp...
#include "clang/Tooling/Tooling.h"                           // for newFront...
#include "llvm/ADT/ArrayRef.h"                               // for ArrayRef
#include "llvm/ADT/IntrusiveRefCntPtr.h"                     // for Intrusiv...
#include "llvm/ADT/SmallVector.h"                            // for SmallVector
#include "llvm/ADT/StringRef.h"                              // for StringRef
#include "llvm/Support/CommandLine.h"                        // for cat, desc

#include "src/analyzer.h"
#include "src/annotator.h"
#include "src/correctness.h"
#include "src/debug.h"
#include "src/jsvalue.h"
#include "src/recommender.h"

namespace cl = llvm::cl;
namespace ento = clang::ento;
namespace tooling = clang::tooling;

// Apply a custom category to all command-line options so that they are the
// only ones displayed
static cl::OptionCategory floatmageCategory("Floatmage options");

// Options
static cl::opt<bool> debugAnnotationCandidates(
    "fsuggest-annotations",
    cl::desc("Suggest candidate functions from the JSAPI headers to annotate"),
    cl::cat(floatmageCategory));
static cl::opt<unsigned> esrVersion(
    "esr", cl::init(68),
    cl::desc("Customize for given SpiderMonkey ESR version (default 68)"),
    cl::cat(floatmageCategory));
static cl::opt<clang::AnalysisDiagClients> outputFormat(
    "output-format",
    cl::values(
        clEnumValN(clang::PD_HTML, "html", "HTML in a directory"),
        clEnumValN(clang::PD_HTML_SINGLE_FILE, "html-single-file",
                   "HTML single file (not allowing for multi-file bugs)"),
        clEnumValN(clang::PD_PLIST, "plist", "PList"),
        clEnumValN(clang::PD_PLIST_MULTI_FILE, "plist-multi-file",
                   "PLists (allowing for multi-file bugs)"),
        clEnumValN(clang::PD_PLIST_HTML, "plist-html",
                   "HTML wrapped with PLists"),
        clEnumValN(clang::PD_SARIF, "sarif", "SARIF file"),
        clEnumValN(clang::PD_TEXT, "text",
                   "Command line output with source notes")),
    cl::init(clang::PD_TEXT),
    cl::desc("Output format for analyzer error messages"),
    cl::cat(floatmageCategory));
static cl::opt<std::string> outputFile("o",
                                       cl::desc("Where to put output file"),
                                       cl::cat(floatmageCategory));
static cl::opt<bool> verbose1("v", cl::desc("Debug logging"),
                              cl::cat(floatmageCategory));
static cl::opt<bool> verbose2("vv", cl::desc("Verbose debug logging"),
                              cl::cat(floatmageCategory));
static cl::opt<bool> verbose3("vvv",
                              cl::desc("Extremely verbose debug logging"),
                              cl::cat(floatmageCategory));

static cl::extrahelp commonHelp(tooling::CommonOptionsParser::HelpMessage);
static cl::extrahelp moreHelp(R"help(
Floatmage is a tool for analyzing code using the SpiderMonkey API and ensuring
that the JSAPI return convention is followed.

If a function annotated with "jsapi_return_convention" returns true or a
non-null pointer, then an exception must not be pending on the JSContext.
Conversely, if it returns false or nullptr, an exception must be pending.

See https://gitlab.gnome.org/GNOME/gjs/wikis/Understanding-SpiderMonkey-code
for more information.
)help");

extern const char* assertionsShim;

static const char* bugReportURI = "https://gitlab.gnome.org/ptomato/floatmage";

class FloatmageAction : public clang::ASTFrontendAction {
    static void registerCheckers(ento::CheckerRegistry& registry) {
        registry.addChecker<RecommenderChecker>(
            "floatmage.Recommender",
            "Heuristic for recommending functions that should be annotated",
            bugReportURI);
        registry.addChecker<AnnotationCorrectnessChecker>(
            "floatmage.AnnotationCorrectness",
            "Checking minimum requirements of annotated functions",
            bugReportURI);
        registry.addChecker<JSValueModeling>("floatmage.JSValueModeling",
                                             "Modeling JS::Value states",
                                             bugReportURI);
        registry.addChecker<FloatmageAnalyzer>(
            "floatmage.Analyzer", "Ensuring JSAPI return convention",
            bugReportURI);
    }

 public:
    std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
        clang::CompilerInstance& compiler, llvm::StringRef file) override {
        if (verbose3)
            Debug::setVerbosity(3);
        else if (verbose2)
            Debug::setVerbosity(2);
        else if (verbose1)
            Debug::setVerbosity(1);

        // Regular AST-based checks
        std::unique_ptr<clang::ASTConsumer> annotator =
            std::make_unique<Annotator>(compiler, debugAnnotationCandidates,
                                        esrVersion);

        clang::AnalyzerOptionsRef options = compiler.getAnalyzerOpts();
        options->CheckersControlList = {
            {"core.CallAndMessage", true},
            {"core.DivideZero", true},
            {"core.DynamicTypePropagation", true},
            {"core.NonNullParamChecker", true},
            {"core.NonnilStringConstants", true},
            {"core.NullDereference", true},
            {"core.StackAddressEscape", true},
            {"core.UndefinedBinaryOperatorResult", true},
            {"core.VLASize", true},
            {"core.builtin.BuiltinFunctions", true},
            {"core.builtin.NoReturnFunctions", true},
            {"core.uninitialized.ArraySubscript", true},
            {"core.uninitialized.Assign", true},
            {"core.uninitialized.Branch", true},
            {"core.uninitialized.CapturedBlockVariable", true},
            {"core.uninitialized.UndefReturn", true},
            {"floatmage.Recommender", true},
            {"floatmage.AnnotationCorrectness", true},
            {"floatmage.JSValueModeling", true},
            {"floatmage.Analyzer", true},
        };
        options->AnalysisDiagOpt = outputFormat;
        // Supply a default dir name for HTML output
        if (outputFormat == clang::PD_HTML && outputFile.empty())
            outputFile = "analysis-html";
        compiler.getFrontendOpts().OutputFile = outputFile;

        // Static analyzer checks (also including AST-based checks written in
        // the form of static analyzer checkers)
        std::unique_ptr<ento::AnalysisASTConsumer> analyzer =
            ento::CreateAnalysisConsumer(compiler);
        analyzer->AddCheckerRegistrationFn(registerCheckers);

        std::vector<std::unique_ptr<clang::ASTConsumer>> consumers;
        consumers.emplace_back(std::move(annotator));
        consumers.emplace_back(std::move(analyzer));
        return std::make_unique<clang::MultiplexConsumer>(std::move(consumers));
    }
};

static tooling::CommandLineArguments addShims(
    const tooling::CommandLineArguments& args, llvm::StringRef filename) {
    tooling::CommandLineArguments newArgs{
        args[0],
        "-include",
        "mozilla/Assertions.h",
    };
    newArgs.insert(newArgs.end(), ++args.begin(), args.end());
    return newArgs;
}

int main(int argc, const char** argv) {
    // CommonOptionsParser constructor will parse arguments and create a
    // CompilationDatabase. In case of error it will terminate the program.
    tooling::CommonOptionsParser parser(argc, argv, floatmageCategory);

    tooling::ClangTool tool(parser.getCompilations(),
                            parser.getSourcePathList());
    tool.mapVirtualFile("mozilla/Assertions.h", assertionsShim);
    tool.appendArgumentsAdjuster(addShims);
    return tool.run(tooling::newFrontendActionFactory<FloatmageAction>().get());
}
