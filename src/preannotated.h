/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <unordered_set>
#include <vector>

const std::unordered_set<std::string> getPreannotated(unsigned esrVersion);
const std::unordered_set<std::string> getAssumedInfallible(unsigned esrVersion);
const std::vector<std::string> getAssumedInfallibleTypes(unsigned esrVersion);
