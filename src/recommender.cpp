/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>

#include "clang/AST/ASTContext.h"
#include "clang/AST/Decl.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchersInternal.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/SourceManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/AnalysisManager.h"

#include "src/recommender.h"
#include "src/util.h"

// Implements checks on functions that are not annotated with
// jsapi_return_convention: heuristics to recommend if they should be annotated.
// Currently the only heuristic is that functions with the signature of JSNative
// (bool function(JSContext* cx, unsigned argc, JS::Value* vp)) ought to be
// annotated.

namespace ast_matchers = clang::ast_matchers;

class RecommenderCallback : public ast_matchers::MatchFinder::MatchCallback {
    clang::DiagnosticsEngine& m_diagnostics;

 public:
    RecommenderCallback(clang::DiagnosticsEngine& diagnostics)
        : m_diagnostics(diagnostics) {}

    void run(const ast_matchers::MatchFinder::MatchResult& result) {
        const clang::FunctionDecl* func =
            result.Nodes.getNodeAs<clang::FunctionDecl>("unannotated_jsnative");
        assert(func);

        unsigned id = m_diagnostics.getCustomDiagID(
            clang::DiagnosticsEngine::Warning,
            "%q0 seems to have the signature of a JSNative function. Consider "
            "annotating it with jsapi_return_convention.");
        m_diagnostics.Report(func->getLocation(), id) << func;
        if (func->getCanonicalDecl() != func) {
            id = m_diagnostics.getCustomDiagID(clang::DiagnosticsEngine::Note,
                                               "%q0 declared here");
            m_diagnostics.Report(func->getCanonicalDecl()->getLocation(), id)
                << func;
        }
    }
};

void RecommenderChecker::checkASTDecl(const clang::FunctionDecl* func,
                                      ento::AnalysisManager& analysisMgr,
                                      ento::BugReporter&) const {
    if (!analysisMgr.getSourceManager().isWrittenInMainFile(
            func->getLocation()))
        return;

    ast_matchers::MatchFinder finder;
    RecommenderCallback callback(analysisMgr.getASTContext().getDiagnostics());

    using namespace ast_matchers;  // otherwise much too tedious

    // clang-format off
    finder.addMatcher(
        functionDecl(
            unless(isAnnotatedJSAPIReturnConvention()),
            returns(booleanType()),
            parameterCountIs(3),
            hasParameter(0, hasJSContextType()),
            hasParameter(1, hasType(isUnsignedInteger())),
            hasParameter(2, hasType(pointsTo(elaboratedType(
                hasQualifier(specifiesNamespace(hasName("JS"))),
                namesType(recordType(hasDeclaration(namedDecl(
                    hasName("Value"))))))))))
            .bind("unannotated_jsnative"),
        &callback);
    // clang-format on

    finder.match(*func, analysisMgr.getASTContext());
}
