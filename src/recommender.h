/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "clang/StaticAnalyzer/Core/AnalyzerOptions.h"  // for ento
#include "clang/StaticAnalyzer/Core/Checker.h"

namespace clang {
class FunctionDecl;
namespace ento {
class AnalysisManager;
class BugReporter;
}  // namespace ento
}  // namespace clang

namespace check = clang::ento::check;
namespace ento = clang::ento;

class RecommenderChecker
    : public ento::Checker<check::ASTDecl<clang::FunctionDecl>> {
 public:
    explicit RecommenderChecker() {}

    void checkASTDecl(const clang::FunctionDecl* func,
                      ento::AnalysisManager& analysisMgr,
                      ento::BugReporter& bugReporter) const;
};
