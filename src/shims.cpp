/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

// Analyzer-friendly reimplementations of runtime and static assertion macros.
// Assertions are always turned on, etc. The mozilla/Assertions.h in
// SpiderMonkey unfortunately uses *((volatile int*)NULL) = __LINE__ to mark an
// assertion failure, which causes the null dereference checker to go bananas,
// so that's the main reason why we have this shim.

const char* assertionsShim = R"code(
#ifndef mozilla_Assertions_h
#define mozilla_Assertions_h

#include <assert.h>

#include <type_traits>

// Pull in any other things that includers of the header expect to be defined
#include "mozilla/Attributes.h"
#include "mozilla/Compiler.h"
#include "mozilla/Likely.h"
#include "mozilla/MacroArgs.h"
#include "mozilla/StaticAnalysisFunctions.h"
#include "mozilla/Types.h"

#define MOZ_CRASH_ANNOTATE(...) do { /* nothing */ } while (false)

#define MOZ_STATIC_ASSERT_IF(cond, expr, reason) \
    static_assert(!(cond) || (expr), reason)

extern "C" {

[[noreturn]] static inline void MOZ_ReportAssertionFailure(const char* str,
                                                           const char* filename,
                                                           int line) {
    assert(false);
}

[[noreturn]] static inline void MOZ_ReportCrash(const char* str,
                                                const char* filename,
                                                int line) {
    assert(false);
}

[[noreturn]] static inline void MOZ_REALLY_CRASH(int line) {
    assert(false);
}

[[noreturn]] static inline void MOZ_CRASH(...) {
    assert(false);
}

[[noreturn]] static inline void MOZ_CrashOOL(...) {
    assert(false);
}

[[noreturn]] static inline void MOZ_CRASH_UNSAFE_OOL(const char* reason) {
    assert(false);
}

static const size_t sPrintfMaxArgs = 4;
static const size_t sPrintfCrashReasonSize = 1024;

[[noreturn]] static inline void MOZ_CrashPrintf(...) {
    assert(false);
}

[[noreturn]] static inline void MOZ_CALL_CRASH_PRINTF(...) {
    assert(false);
}

[[noreturn]] static inline void MOZ_CRASH_UNSAFE_PRINTF(...) {
    assert(false);
}

}  // extern "C"

namespace mozilla {
namespace detail {

template<typename T>
struct AssertionConditionType {
    typedef typename std::remove_reference<T>::type ValueT;
    static_assert(!std::is_array<ValueT>::value,
                  "Expected boolean assertion condition, got an array or a "
                  "string!");
    static_assert(!std::is_function<ValueT>::value,
                  "Expected boolean assertion condition, got a function! Did "
                  "you intend to call that function?");
    static_assert(!std::is_floating_point<ValueT>::value,
                  "It's often a bad idea to assert that a floating-point number "
                  "is nonzero, because such assertions tend to intermittently "
                  "fail. Shouldn't your code gracefully handle this case instead "
                  "of asserting? Anyway, if you really want to do that, write an "
                  "explicit boolean condition, like !!x or x!=0.");

    static const bool isValid = true;
};

}  // namespace detail
}  // namespace mozilla
#define MOZ_VALIDATE_ASSERT_CONDITION_TYPE(x) \
     static_assert(mozilla::detail::AssertionConditionType<decltype(x)>::isValid, \
                   "invalid assertion condition")

#ifdef DEBUG
[[noreturn]]
static inline void MOZ_REPORT_ASSERTION_FAILURE(...) {
    assert(false);
}
#else
#define MOZ_REPORT_ASSERTION_FAILURE(...) do {} while (false)
#endif

template <typename T>
static inline void MOZ_RELEASE_ASSERT(T expr) {
    assert(expr);
}

template <typename T>
static inline void MOZ_RELEASE_ASSERT(T expr, const char* explain) {
    assert(expr && explain);
}

#ifdef DEBUG
template <typename T>
static inline void MOZ_ASSERT(T expr) {
    assert(expr);
}

template <typename T>
static inline void MOZ_ASSERT(T expr, const char* explain) {
    assert(expr && explain);
}
#else
#define MOZ_ASSERT(...) do {} while (false)
#endif

#if defined(NIGHTLY_BUILD) || defined(MOZ_DEV_EDITION)
#define MOZ_DIAGNOSTIC_ASSERT MOZ_RELEASE_ASSERT
#define MOZ_DIAGNOSTIC_ASSERT_ENABLED 1
#else
#define MOZ_DIAGNOSTIC_ASSERT MOZ_ASSERT
#ifdef DEBUG
#define MOZ_DIAGNOSTIC_ASSERT_ENABLED 1
#endif
#endif

#ifdef DEBUG
template <typename T>
static inline void MOZ_ASSERT_IF(bool cond, T expr) {
    if (cond)
        assert(expr);
}
#else
#define MOZ_ASSERT_IF(cond, expr) do {} while (false)
#endif

#define MOZ_ASSUME_UNREACHABLE_MARKER() __builtin_unreachable()

#ifdef DEBUG
[[noreturn]] static inline void MOZ_ASSERT_UNREACHABLE(const char* reason) {
    assert(false);
}
#else
#define MOZ_ASSERT_UNREACHABLE(reason) do {} while (false)
#endif

#define MOZ_MAKE_COMPILER_ASSUME_IS_UNREACHABLE(reason) \
    do { \
        MOZ_ASSERT_UNREACHABLE(reason); \
        MOZ_ASSUME_UNREACHABLE_MARKER(); \
    } while (false)

#ifdef DEBUG
[[noreturn]] static inline void MOZ_FALLTHROUGH_ASSERT(const char* reason) {
    assert(false);
}
#else
#define MOZ_FALLTHROUGH_ASSERT(...) MOZ_FALLTHROUGH
#endif

#ifdef DEBUG
static inline void MOZ_ALWAYS_TRUE(bool expr) {
    assert(expr);
}

static inline void MOZ_ALWAYS_FALSE(bool expr) {
    assert(!expr);
}

#define MOZ_ALWAYS_OK(expr)   MOZ_ASSERT((expr).isOk())
#define MOZ_ALWAYS_ERR(expr)  MOZ_ASSERT((expr).isErr())
#else
#define MOZ_ALWAYS_TRUE(expr)     \
  do {                            \
    if ((expr)) {}                \
  } while (false)
#define MOZ_ALWAYS_FALSE(expr)    \
  do {                            \
    if ((expr)) {}                \
  } while (false)
#define MOZ_ALWAYS_OK(expr)       \
  do {                            \
    if ((expr).isOk()) {}         \
  } while (false)
#define MOZ_ALWAYS_ERR(expr)      \
  do {                            \
    if ((expr).isErr()) {}        \
  } while (false)
#endif

#endif  // mozilla_Assertions_h
)code";
