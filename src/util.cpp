/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#include "clang/AST/Attr.h"                         // for AnnotateAttr
#include "clang/AST/Decl.h"                         // for FunctionDecl, Rec...
#include "clang/AST/Type.h"                         // for Type, QualType
#include "clang/ASTMatchers/ASTMatchers.h"          // for hasName, hasType
#include "clang/ASTMatchers/ASTMatchersInternal.h"  // for PolymorphicMatche...
#include "clang/Basic/IdentifierTable.h"            // for IdentifierInfo
#include "llvm/ADT/StringRef.h"                     // for operator==, Strin...
#include "llvm/ADT/iterator_range.h"                // for iterator_range

bool TypeIsJSContextPointer(const clang::QualType qtype) {
    const clang::Type* type = qtype.getTypePtr();
    if (!type->isPointerType())
        return false;

    const clang::Type* pointee = type->getPointeeType().getTypePtr();
    if (pointee->isRecordType()) {
        const clang::IdentifierInfo* ident =
            pointee->getAsRecordDecl()->getIdentifier();
        if (ident && ident->isStr("JSContext"))
            return true;
    }

    return false;
}

bool FunctionHasJSAPIReturnConvention(const clang::FunctionDecl* func) {
    for (const auto* attr : func->specific_attrs<clang::AnnotateAttr>()) {
        if (attr->getAnnotation() == "jsapi_return_convention")
            return true;
    }
    return false;
}

namespace clang {
namespace ast_matchers {

const internal::Matcher<ValueDecl> hasJSContextType() {
    return hasType(pointsTo(recordDecl(hasName("JSContext"))));
}

}  // namespace ast_matchers
}  // namespace clang
