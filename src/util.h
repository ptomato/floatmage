/*
 * This file is part of Floatmage.
 *
 * Floatmage is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Floatmage is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Floatmage. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "clang/AST/Decl.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"

namespace clang {
class FunctionDecl;
}

// Returns true if the given declaration has the jsapi_return_convention
// annotation according to the AST.
bool FunctionHasJSAPIReturnConvention(const clang::FunctionDecl* func);

// Returns true if the given type is JSContext* (with any qualifications).
bool TypeIsJSContextPointer(const clang::QualType qtype);

namespace clang {
namespace ast_matchers {

AST_MATCHER(FunctionDecl, isAnnotatedJSAPIReturnConvention) {
    return FunctionHasJSAPIReturnConvention(&Node);
}

const internal::Matcher<ValueDecl> hasJSContextType();

}  // namespace ast_matchers
}  // namespace clang
