//== error no JSContext argument
//= error: 'func' should have JSContext* as the type of its first argument so that it can report exceptions
//= JSAPI_RETURN_CONVENTION bool func(JSObject* object) { return true; }
//=                              ^    ~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION bool func(JSObject* object) { return true; }

//== JSContext class member

class ContextMember {
    JSContext* m_cx;
    JSAPI_RETURN_CONVENTION bool method(JSObject* object) { return true; }
};

//== error no JSContext class member or argument
//= error: 'NoContextMember::method' should have JSContext* as the type of its first argument or as a member of its class so that it can report exceptions
//=     JSAPI_RETURN_CONVENTION bool method(JSObject* object) { return true; }
//=                                  ^      ~~~~~~~~~~~~~~~~

class NoContextMember {
    JSAPI_RETURN_CONVENTION bool method(JSObject* object) { return true; }
};

//== annotated JSNative signature

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, unsigned argc, JS::Value* vp) { return true; }

//== warning non-annotated JSNative signature
//= warning: 'func' seems to have the signature of a JSNative function. Consider annotating it with jsapi_return_convention.
//= bool func(JSContext* cx, unsigned argc, JS::Value* vp) { return true; }
//=      ^

bool func(JSContext* cx, unsigned argc, JS::Value* vp) { return true; }

//== annotated method with JSNative signature

template <typename T>
class UninstantiatedTemplate {
    T* dummy;

    JSAPI_RETURN_CONVENTION
    bool method(JSContext*, unsigned, JS::Value*) { return true; }
};

//== warning non-annotated method with JSNative signature
//= warning: 'UninstantiatedTemplate::method' seems to have the signature of a JSNative function. Consider annotating it with jsapi_return_convention.
//=     bool method(JSContext*, unsigned, JS::Value*) { return true; }
//=          ^

template <typename T>
class UninstantiatedTemplate {
    T* dummy;

    bool method(JSContext*, unsigned, JS::Value*) { return true; }
};
