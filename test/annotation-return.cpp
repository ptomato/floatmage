//== error void return
//= error: 'func' must have a return type; it needs to indicate whether an exception was thrown
//= JSAPI_RETURN_CONVENTION void func(JSContext* cx) {}
//=                         ~~~~ ^

JSAPI_RETURN_CONVENTION void func(JSContext* cx) {}

//== error non-bool-convertible return
//= error: The return type 'struct foo' of 'func' should be a type with a value to clearly indicate whether an exception was thrown, such as 'bool'
//= JSAPI_RETURN_CONVENTION struct foo func(JSContext* cx) { return {0}; }
//=                         ~~~~~~~~~~ ^

struct foo {
    int dummy;
};

JSAPI_RETURN_CONVENTION struct foo func(JSContext* cx) { return {0}; }

//== bool return

JSAPI_RETURN_CONVENTION bool func(JSContext* cx) { return true; }

//== pointer return

JSAPI_RETURN_CONVENTION JSObject* objectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::HandleObject obj) { return objectFunc(cx); }

//== jsid return

JSAPI_RETURN_CONVENTION jsid idFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
jsid func(JSContext* cx, JS::HandleId id) { return idFunc(cx); }

//== bool-convertible return

struct BoolConvertibleStruct : public std::unique_ptr<char> {
    BoolConvertibleStruct(char* p) : BoolConvertibleStruct::unique_ptr(p) {}
    operator bool() const { return !!get(); }
};

JSAPI_RETURN_CONVENTION char* stringFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
BoolConvertibleStruct func(JSContext* cx) { return stringFunc(cx); }

//== pointer-convertible return

JSAPI_RETURN_CONVENTION char* stringFunc(JSContext* cx);

struct PtrConvertibleStruct : public std::unique_ptr<char> {
    PtrConvertibleStruct(char* p) : PtrConvertibleStruct::unique_ptr(p) {}
    operator char*() const { return get(); }
};

JSAPI_RETURN_CONVENTION
PtrConvertibleStruct func(JSContext* cx) { return stringFunc(cx); }

//== typedef of bool-convertible return type

JSAPI_RETURN_CONVENTION JS::UniqueChars uniqueCharsFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JS::UniqueChars func(JSContext* cx) { return uniqueCharsFunc(cx); }
