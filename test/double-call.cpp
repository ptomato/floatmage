//== error second annotated call in same CFG block
//= warning: Calling 'AnnotatedFunction()' with exception already pending
//=     return AnnotatedFunction(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~
//= note: Exception is set pending when 'AnnotatedFunction()' fails
//=     AnnotatedFunction(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~
//= note: Check the error status of 'AnnotatedFunction()' before calling 'AnnotatedFunction()'
//=     return AnnotatedFunction(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION bool func(JSContext* cx) {
    AnnotatedFunction(cx);
    return AnnotatedFunction(cx);
}

//== error second preannotated call in same CFG block
//= warning: Calling 'JS_NewPlainObject()' with exception already pending
//=     return JS_NewPlainObject(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~
//= note: Exception is set pending when 'JS_NewPlainObject()' fails
//=     JS_NewPlainObject(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~
//= note: Check the error status of 'JS_NewPlainObject()' before calling 'JS_NewPlainObject()'
//=     return JS_NewPlainObject(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION JSObject* func(JSContext* cx) {
    JS_NewPlainObject(cx);
    return JS_NewPlainObject(cx);
}

//== error annotated call in different CFG block
//= warning: Calling 'AnnotatedFunction()' with exception already pending
//=     return AnnotatedFunction(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~
//= note: Exception is set pending when 'AnnotatedFunction()' fails
//=     AnnotatedFunction(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~
//= note: Assuming the condition is false
//=     if (UnrelatedFunction())
//=         ^~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (UnrelatedFunction())
//=     ^
//= note: Check the error status of 'AnnotatedFunction()' before calling 'AnnotatedFunction()'
//=     return AnnotatedFunction(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~

bool UnrelatedFunction();

JSAPI_RETURN_CONVENTION bool func(JSContext* cx) {
    AnnotatedFunction(cx);
    if (UnrelatedFunction())
        UnrelatedFunction();
    return AnnotatedFunction(cx);
}

//== error annotated call in inlined procedure
//= warning: Calling 'AnnotatedFunction()' with exception already pending
//=     return AnnotatedFunction(cx);
//=            ^
//= note: Calling 'ErrorAnnotatedCallInInlinedProcedure'
//=     if (!ErrorAnnotatedCallInInlinedProcedure(cx))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'FunctionThatGetsInlined'
//=     FunctionThatGetsInlined(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Exception is set pending when 'FunctionThatGetsInlined()' fails
//=     FunctionThatGetsInlined(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'FunctionThatGetsInlined'
//=     FunctionThatGetsInlined(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Check the error status of 'FunctionThatGetsInlined()' before calling 'AnnotatedFunction()'
//=     return AnnotatedFunction(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool FunctionThatGetsInlined(JSContext* cx) { return AnnotatedFunction(cx); }

JSAPI_RETURN_CONVENTION
bool ErrorAnnotatedCallInInlinedProcedure(JSContext* cx) {
    FunctionThatGetsInlined(cx);
    return AnnotatedFunction(cx);
}

// Note, the error should not occur in this function, since it is already
// flagged in the above function
JSAPI_RETURN_CONVENTION
bool OkayAnnotatedCallInInlinedErrorProcedure(JSContext* cx) {
    if (!ErrorAnnotatedCallInInlinedProcedure(cx))
        return false;
    return AnnotatedFunction(cx);
}

//== error annotated call in unannotated inlined procedure
//= warning: Calling 'AnnotatedFunction()' with exception already pending
//=     return FunctionThatGetsInlinedWithForgottenAnnotation(cx);
//=            ^
//= note: Exception is set pending when 'AnnotatedFunction()' fails
//=     AnnotatedFunction(cx);
//=     ^~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'FunctionThatGetsInlinedWithForgottenAnnotation'
//=     return FunctionThatGetsInlinedWithForgottenAnnotation(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Check the error status of 'AnnotatedFunction()' before calling 'FunctionThatGetsInlinedWithForgottenAnnotation()'
//=     return FunctionThatGetsInlinedWithForgottenAnnotation(cx);
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool FunctionThatGetsInlinedWithForgottenAnnotation(JSContext* cx) {
    return AnnotatedFunction(cx);
}

JSAPI_RETURN_CONVENTION bool func(JSContext* cx) {
    AnnotatedFunction(cx);
    return FunctionThatGetsInlinedWithForgottenAnnotation(cx);
}
