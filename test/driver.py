#!/usr/bin/env python3

# Take an input file which contains one or more sections of the form:
# //== [section title]
# //= [zero or more lines of error message]
# //- [lines of error message that are not required to appear]
# [code]
#
# Sections are separated by a line starting with ‘//==’.
#
# If any sections start with '//!==' (with an extra !) then only those sections
# are run. This is for debugging a particular test.
#
# The wrapper script takes each section and adds some standard includes and
# definitions. It then compiles the code using Clang with Floatmage, and checks
# the compiler output against the expected error message. If there is no
# expected error message it asserts there’s no error.
#
# Output is in TAP format.

import argparse
import os
import re
import shlex
import subprocess
import tempfile

DEFAULT_TEMPLATE_HEAD = '''\
#include <sys/types.h>

#include <memory>

#include <jsapi.h>

#define JSAPI_RETURN_CONVENTION \\
    __attribute__((annotate("jsapi_return_convention")))

JSAPI_RETURN_CONVENTION bool AnnotatedFunction(JSContext* cx);
'''

parser = argparse.ArgumentParser(description='Floatmage test driver.')
parser.add_argument('infile', type=argparse.FileType('r'), metavar='FILE',
                    help='path to input test file')
parser.add_argument('--verbose', '-v', action='store_true',
                    help='print extra log messages')
parser.add_argument('--esr', type=int, choices=[60, 68], default=68,
                    help='configure for given SpiderMonkey ESR version')
parser.add_argument('--debug-spidermonkey', action='store_true',
                    help='configure for debug-enabled SpiderMonkey')
parser.add_argument('--debug', '-d', nargs='?', default=False,
                    const='gdb --args',
                    help='Run under debugger (default="gdb --args")')
args = parser.parse_args()

# Before starting, work out the compiler’s system include paths.
# Thanks to: http://stackoverflow.com/a/17940271/2931197
result = subprocess.run(['cpp', '-xc++', '-Wp,-v'], input='',
                        stdout=subprocess.DEVNULL, stderr=subprocess.PIPE,
                        universal_newlines=True)
lines = result.stderr.splitlines()
system_paths = filter(lambda l: l.startswith(' '), lines)
system_paths = map(lambda l: l.replace('(framework directory)', '').strip(),
                   system_paths)
system_includes = []
for path in system_paths:
    system_includes += ['-isystem', path]

mozjs_package = 'mozjs-{}'.format(args.esr)
output = subprocess.check_output(['pkg-config', '--cflags', mozjs_package],
                                 universal_newlines=True)
mozjs_includes = shlex.split(output)


class Test:
    def __init__(self, name, priority=False):
        self.min_version = None
        self.max_version = None
        self.priority = priority

        match = re.match(r'\[(\d+)?-(\d+)?\]', name)
        if match:
            self.min_version = match.group(1)
            if self.min_version is not None:
                self.min_version = int(self.min_version)
            self.max_version = match.group(2)
            if self.max_version is not None:
                self.max_version = int(self.max_version)
            name = name[len(match.group(0)):].strip()

        self.name = name
        self.source = DEFAULT_TEMPLATE_HEAD
        self.expected_error = []

    def add_source(self, line):
        self.source += line + os.linesep

    def add_error(self, line, required=True):
        self.expected_error.append((line, required))

    def write(self, fp):
        fp.write(self.source)

    def should_execute_for(self, version):
        if self.min_version is not None and version < self.min_version:
            return False
        if self.max_version is not None and version > self.max_version:
            return False
        return True

    def nonmatching_lines(self, output_lines):
        return [line for line, required in self.expected_error
                if all(map(lambda l: required and line not in l,
                           output_lines))]

    def unexpected_lines(self, output_lines):
        retval = []
        for line in output_lines:
            if not re.match(r'.*:\d+:\d+: ', line):
                continue
            line = re.sub(r'^.*:\d+:\d+: ', '', line)
            if all(map(lambda rec: line not in rec[0], self.expected_error)):
                retval.append(line)
        return retval


def log_tap_multiline_text(lines):
    print('#     ' + (os.linesep + '#    ').join(lines))


current_test = None
priority_tests_only = False
tests = []

for line in args.infile:
    line = line.rstrip(os.linesep)
    if line.startswith('//=='):
        if current_test is not None:
            tests.append(current_test)
        current_test = Test(name=line[5:].strip())
    elif line.startswith('//!=='):
        if current_test is not None:
            tests.append(current_test)
        current_test = Test(name=line[6:].strip(), priority=True)
        priority_tests_only = True
    elif current_test is None:
        continue
    elif line.startswith('//='):
        current_test.add_error(line[4:])
    elif line.startswith('//-'):
        current_test.add_error(line[4:], required=False)
    else:
        current_test.add_source(line)
if current_test is not None:
    tests.append(current_test)

if priority_tests_only:
    print('# NOTE: running only priority tests')
    tests = [test for test in tests if test.priority]

print('1..{}'.format(len(tests)))
if args.verbose:
    print('# reading input from {}'.format(args.infile.name))

for ix, test in enumerate(tests):
    if not test.should_execute_for(args.esr):
        print('ok {} {} # SKIP {} ≤ version ≤ {}'.format(
            ix + 1, test.name, test.min_version, test.max_version))
        continue

    fd, path = tempfile.mkstemp(suffix='.cpp', text=True)
    with os.fdopen(fd, 'wt') as fp:
        test.write(fp)

    command = (['./floatmage', '-fsuggest-annotations', '--esr', str(args.esr),
                path, '--', '-xc++', '-std=c++14'] +
               system_includes + mozjs_includes)
    if args.debug_spidermonkey:
        command += ['-DDEBUG']
    if args.verbose:
        print('# compiling {}'.format(' '.join(command)))
    if args.debug:
        result = subprocess.run(shlex.split(args.debug) + command)
    else:
        result = subprocess.run(command, stdout=subprocess.DEVNULL,
                                stderr=subprocess.PIPE,
                                universal_newlines=True)

    if args.verbose:
        print('# compiler exit code: {}'.format(result.returncode))
    output_lines = result.stderr.splitlines()
    failed = False

    for line in output_lines:
        if line.startswith('debug:'):
            print('# ', line)

    if test.expected_error:
        nonmatching_lines = test.nonmatching_lines(output_lines)
        unexpected_lines = test.unexpected_lines(output_lines)
        if nonmatching_lines:
            failed = True
            for line in nonmatching_lines:
                print('# Non-matching line: {}'.format(line))
            print('# Error: expected compiler error was not seen.')
        if unexpected_lines:
            failed = True
            for line in unexpected_lines:
                print('# Unexpected line: {}'.format(line))
            print('# Error: unexpected compiler error seen.')
        if failed:
            print('# Expected:')
            log_tap_multiline_text(list(zip(*test.expected_error))[0])
            print('# Actual:')
            log_tap_multiline_text(output_lines)
    else:
        if result.returncode != 0:
            failed = True
            print('# Error: exit code was nonzero but should have been 0.')
        if result.stderr:
            failed = True
            print('# Error: compiler error when none was expected.')
            log_tap_multiline_text(output_lines)

    if failed:
        if args.verbose:
            print('# not deleting {}'.format(path))
    else:
        os.remove(path)

    print('{}ok {} {}'.format('not ' if failed else '', ix + 1, test.name))
