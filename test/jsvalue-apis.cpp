//== JS_GetPropertyById resets the value to unknown

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleObject obj, JS::HandleId id1,
          JS::HandleId id2) {
    JS::RootedValue v(cx);
    if (!JS_GetPropertyById(cx, obj, id1, &v))
        return false;
    if (v.isBoolean())
        return true;
    if (!JS_GetPropertyById(cx, obj, id2, &v))
        return false;
    if (v.isBoolean())
        return true;
    return true;
}

//== JS::CallArgs::calleev() is an object

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, const JS::CallArgs& args) {
    JS::RootedObject callee(cx, &args.callee());
    return true;
}

//== [68-] JS::SameValue warns if values are known to be different types
//= warning: Call to 'JS::SameValue()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS::SameValue(cx, val1, val2, &same))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Assuming value is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is null because argument 2 is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Assuming value is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2.ptr' is a 32-bit integer because argument 2 is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::SameValue()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS::SameValue(cx, val1, val2, &same))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    JS::RootedValue val1(cx, JS::NullValue());
    JS::RootedValue val2(cx, JS::Int32Value(0));
    bool same;
    if (!JS::SameValue(cx, val1, val2, &same))
        return false;
    assert(!same);
    return true;
}

//== [68-] JS::SameValue makes inferences about the type when it succeeds

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION JSObject* ObjectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::Value v1, JS::Value v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isObjectOrNull() && val2.isObject()) {
        bool same;
        if (!JS::SameValue(cx, val1, val2, &same))
            return nullptr;
        if (same)
            return &val1.toObject();
    }
    return ObjectFunc(cx);
}

//== [68-] JS::SameValue makes inferences about the type when it fails
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is unknown because 'v1' is unknown
//=     JS::RootedValue val1(cx, v1);
//=                              ^~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is undefined or null
//=   bool isNullOrUndefined() const { return value().isNullOrUndefined(); }
//=                                           ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1.ptr' is undefined
//=         if (!JS::SameValue(cx, val1, val2, &same))
//=              ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming the condition is false
//=         if (!JS::SameValue(cx, val1, val2, &same))
//=             ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=         if (!JS::SameValue(cx, val1, val2, &same))
//=         ^
//= note: 'same' is false
//=         if (!same)
//=             ^~~~~
//= note: Taking false branch
//=         if (!same)
//=         ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=             assert(val1.toBoolean());
//=                    ^~~~~~~~~~~~~~~~
//= note: expanded from macro 'assert'
//=      (static_cast <bool> (expr)
//=                           ^~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue v1, JS::HandleValue v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isNullOrUndefined() && val2.isNull()) {
        bool same;
        if (!JS::SameValue(cx, val1, val2, &same))
            return false;
        if (!same)
            assert(val1.toBoolean());
    }
    return true;
}

//== [68-] JS::StrictlyEqual warns if values are known to be different types
//= warning: Call to 'JS::StrictlyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS::StrictlyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Assuming value is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is null because argument 2 is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Assuming value is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2.ptr' is a 32-bit integer because argument 2 is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::StrictlyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS::StrictlyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    JS::RootedValue val1(cx, JS::NullValue());
    JS::RootedValue val2(cx, JS::Int32Value(0));
    bool equal;
    if (!JS::StrictlyEqual(cx, val1, val2, &equal))
        return false;
    assert(!equal);
    return true;
}

//== [68-] JS::StrictlyEqual makes inferences about the type when it succeeds

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION JSObject* ObjectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::Value v1, JS::Value v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isObjectOrNull() && val2.isObject()) {
        bool equal;
        if (!JS::StrictlyEqual(cx, val1, val2, &equal))
            return nullptr;
        if (equal)
            return &val1.toObject();
    }
    return ObjectFunc(cx);
}

//== [68-] JS::StrictlyEqual makes inferences about the type when it fails
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is unknown because 'v1' is unknown
//=     JS::RootedValue val1(cx, v1);
//=                              ^~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is undefined or null
//=   bool isNullOrUndefined() const { return value().isNullOrUndefined(); }
//=                                           ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1.ptr' is undefined
//=         if (!JS::StrictlyEqual(cx, val1, val2, &equal))
//=              ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming the condition is false
//=         if (!JS::StrictlyEqual(cx, val1, val2, &equal))
//=             ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=         if (!JS::StrictlyEqual(cx, val1, val2, &equal))
//=         ^
//= note: 'equal' is false
//=         if (!equal)
//=             ^~~~~~
//= note: Taking false branch
//=         if (!equal)
//=         ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=             assert(val1.toBoolean());
//=                    ^~~~~~~~~~~~~~~~
//= note: expanded from macro 'assert'
//=      (static_cast <bool> (expr)
//=                           ^~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue v1, JS::HandleValue v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isNullOrUndefined() && val2.isNull()) {
        bool equal;
        if (!JS::StrictlyEqual(cx, val1, val2, &equal))
            return false;
        if (!equal)
            assert(val1.toBoolean());
    }
    return true;
}

//== [68-] JS::LooselyEqual warns if values are known to be different types
//= warning: Call to 'JS::LooselyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS::LooselyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Assuming value is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is null because argument 2 is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Assuming value is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2.ptr' is a 32-bit integer because argument 2 is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::LooselyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS::LooselyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    JS::RootedValue val1(cx, JS::NullValue());
    JS::RootedValue val2(cx, JS::Int32Value(0));
    bool equal;
    if (!JS::LooselyEqual(cx, val1, val2, &equal))
        return false;
    assert(!equal);
    return true;
}

//== [68-] JS::LooselyEqual makes inferences about the type when it succeeds                           ^

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION JSObject* ObjectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::Value v1, JS::Value v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isObjectOrNull() && val2.isObject()) {
        bool equal;
        if (!JS::LooselyEqual(cx, val1, val2, &equal))
            return nullptr;
        if (equal)
            return &val1.toObject();
    }
    return ObjectFunc(cx);
}

//== [68-] JS::LooselyEqual makes inferences about the type when it fails
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is unknown because 'v1' is unknown
//=     JS::RootedValue val1(cx, v1);
//=                              ^~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is undefined or null
//=   bool isNullOrUndefined() const { return value().isNullOrUndefined(); }
//=                                           ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1.ptr' is undefined
//=         if (!JS::LooselyEqual(cx, val1, val2, &equal))
//=              ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming the condition is false
//=         if (!JS::LooselyEqual(cx, val1, val2, &equal))
//=             ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=         if (!JS::LooselyEqual(cx, val1, val2, &equal))
//=         ^
//= note: 'equal' is false
//=         if (!equal)
//=             ^~~~~~
//= note: Taking false branch
//=         if (!equal)
//=         ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=             assert(val1.toBoolean());
//=                    ^~~~~~~~~~~~~~~~
//= note: expanded from macro 'assert'
//=      (static_cast <bool> (expr)
//=                           ^~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

#include <js/Equality.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue v1, JS::HandleValue v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isNullOrUndefined() && val2.isNull()) {
        bool equal;
        if (!JS::LooselyEqual(cx, val1, val2, &equal))
            return false;
        if (!equal)
            assert(val1.toBoolean());
    }
    return true;
}

//== [68-] JS::SameType warns if values are known to be the same type
//= warning: Call to 'JS::SameType()' is unnecessary because both 'val1' and 'val2' are a 32-bit integer
//=     return JS::SameType(val1, val2);
//=            ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1' is a 32-bit integer
//=     val1.setInt32(5);
//=     ^~~~~~~~~~~~~~~~
//= note: Assuming 'val2' is a 32-bit integer
//=     val2.setInt32(6);
//=     ^~~~~~~~~~~~~~~~
//= note: Call to 'JS::SameType()' is unnecessary because both 'val1' and 'val2' are a 32-bit integer
//=     return JS::SameType(val1, val2);
//=            ^~~~~~~~~~~~~~~~~~~~~~~~

bool func(JS::Value val1, JS::Value val2) {
    val1.setInt32(5);
    val2.setInt32(6);
    return JS::SameType(val1, val2);
}

//== [68-] JS::SameType warns if values are known not to be the same type
//= warning: Call to 'JS::SameType()' can never succeed because 'val1' can only be null and 'val2' can only be a boolean
//=     return JS::SameType(val1, val2);
//=            ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1' is null
//=     val1.setNull();
//=     ^~~~~~~~~~~~~~
//= note: Assuming 'val2' is a boolean
//=     val2.setBoolean(false);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::SameType()' can never succeed because 'val1' can only be null and 'val2' can only be a boolean
//=     return JS::SameType(val1, val2);
//=            ^~~~~~~~~~~~~~~~~~~~~~~~

bool func(JS::Value val1, JS::Value val2) {
    val1.setNull();
    val2.setBoolean(false);
    return JS::SameType(val1, val2);
}

//== [68-] JS::SameType makes inferences about the type when it succeeds
//= warning: Call to 'JS::Value::toInt32()' is incorrect because 'val2' is a string, a symbol, a private GC value, or a bigint
//=                 return val2.toInt32();
//=                        ^~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isPrimitive() && val2.isGCThing()) {
//=         ^
//= note: Assuming 'val2' is any GC thing
//=     if (val1.isPrimitive() && val2.isGCThing()) {
//=                               ^~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val1.isPrimitive() && val2.isGCThing()) {
//=     ^
//= note: Assuming 'val2' is a string, a symbol, a private GC value, or a bigint
//=         if (JS::SameType(val1, val2)) {
//=             ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=         if (JS::SameType(val1, val2)) {
//=         ^
//= note: Assuming the condition is true
//=             if (SplitPath())
//=                 ^~~~~~~~~~~
//= note: Taking true branch
//=             if (SplitPath())
//=             ^
//= note: Call to 'JS::Value::toInt32()' is incorrect because 'val2' is a string, a symbol, a private GC value, or a bigint
//=                 return val2.toInt32();
//=                        ^~~~~~~~~~~~
//= warning: Call to 'JS::Value::toInt32()' is incorrect because 'val1' is a string, a symbol, a private GC value, or a bigint
//=             return val1.toInt32();
//=                    ^~~~~~~~~~~~~~
//= note: Assuming 'val1' is any primitive value
//=     if (val1.isPrimitive() && val2.isGCThing()) {
//=         ^~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isPrimitive() && val2.isGCThing()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isPrimitive() && val2.isGCThing()) {
//=     ^
//= note: Assuming 'val1' is a string, a symbol, a private GC value, or a bigint
//=         if (JS::SameType(val1, val2)) {
//=             ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=         if (JS::SameType(val1, val2)) {
//=         ^
//= note: Assuming the condition is false
//=             if (SplitPath())
//=                 ^~~~~~~~~~~
//= note: Taking false branch
//=             if (SplitPath())
//=             ^
//= note: Call to 'JS::Value::toInt32()' is incorrect because 'val1' is a string, a symbol, a private GC value, or a bigint
//=             return val1.toInt32();
//=                    ^~~~~~~~~~~~~~

bool SplitPath();

uint32_t func(JS::Value val1, JS::Value val2) {
    if (val1.isPrimitive() && val2.isGCThing()) {
        if (JS::SameType(val1, val2)) {
            if (SplitPath())
                return val2.toInt32();
            return val1.toInt32();
        }
    }
    return 0;
}

//== [68-] JS::SameType makes inferences about the type when it fails and one type was known

JSObject* func(JS::Value val1, JS::Value val2, JS::Value val3, JS::Value val4) {
    if (val1.isNull() && val2.isObjectOrNull()) {
        if (!JS::SameType(val1, val2))
            return &val2.toObject();
    }
    if (val3.isObjectOrNull() && val4.isNull()) {
        if (!JS::SameType(val3, val4))
            return &val3.toObject();
    }
    return nullptr;
}

//== [68-] JS::SameType makes no inference if it fails and neither type was known
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val2' is unknown
//=             return val2.toBoolean();
//=                    ^~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val1.isObjectOrNull()) {
//=     ^
//= note: Taking true branch
//=         if (!JS::SameType(val1, val2))
//=         ^
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val2' is unknown
//=             return val2.toBoolean();
//=                    ^~~~~~~~~~~~~~~~

bool func(JS::Value val1, JS::Value val2) {
    if (val1.isObjectOrNull()) {
        if (!JS::SameType(val1, val2))
            return val2.toBoolean();
    }
    return false;
}

//== [-60] JS_SameValue warns if values are known to be different types
//= warning: Call to 'JS_SameValue()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS_SameValue(cx, val1, val2, &same))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Assuming value is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is null because argument 2 is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Assuming value is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2.ptr' is a 32-bit integer because argument 2 is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS_SameValue()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS_SameValue(cx, val1, val2, &same))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    JS::RootedValue val1(cx, JS::NullValue());
    JS::RootedValue val2(cx, JS::Int32Value(0));
    bool same;
    if (!JS_SameValue(cx, val1, val2, &same))
        return false;
    assert(!same);
    return true;
}

//== [-60] JS_SameValue makes inferences about the type when it succeeds

JSAPI_RETURN_CONVENTION JSObject* ObjectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::Value v1, JS::Value v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isObjectOrNull() && val2.isObject()) {
        bool same;
        if (!JS_SameValue(cx, val1, val2, &same))
            return nullptr;
        if (same)
            return &val1.toObject();
    }
    return ObjectFunc(cx);
}

//== [-60] JS_SameValue makes inferences about the type when it fails
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is unknown because 'v1' is unknown
//=     JS::RootedValue val1(cx, v1);
//=                              ^~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is undefined or null
//=   bool isNullOrUndefined() const { return value().isNullOrUndefined(); }
//=                                           ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1.ptr' is undefined
//=         if (!JS_SameValue(cx, val1, val2, &same))
//=              ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=         if (!JS_SameValue(cx, val1, val2, &same))
//=         ^
//= note: 'same' is false
//=         if (!same)
//=             ^~~~~
//= note: Taking false branch
//=         if (!same)
//=         ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=             assert(val1.toBoolean());
//=                    ^~~~~~~~~~~~~~~~
//= note: expanded from macro 'assert'
//=      (static_cast <bool> (expr)
//=                           ^~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue v1, JS::HandleValue v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isNullOrUndefined() && val2.isNull()) {
        bool same;
        if (!JS_SameValue(cx, val1, val2, &same))
            return false;
        if (!same)
            assert(val1.toBoolean());
    }
    return true;
}

//== [-60] JS_StrictlyEqual warns if values are known to be different types
//= warning: Call to 'JS_StrictlyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS_StrictlyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Assuming value is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is null because argument 2 is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Assuming value is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2.ptr' is a 32-bit integer because argument 2 is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS_StrictlyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS_StrictlyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    JS::RootedValue val1(cx, JS::NullValue());
    JS::RootedValue val2(cx, JS::Int32Value(0));
    bool equal;
    if (!JS_StrictlyEqual(cx, val1, val2, &equal))
        return false;
    assert(!equal);
    return true;
}

//== [-60] JS_StrictlyEqual makes inferences about the type when it succeeds

JSAPI_RETURN_CONVENTION JSObject* ObjectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::Value v1, JS::Value v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isObjectOrNull() && val2.isObject()) {
        bool equal;
        if (!JS_StrictlyEqual(cx, val1, val2, &equal))
            return nullptr;
        if (equal)
            return &val1.toObject();
    }
    return ObjectFunc(cx);
}

//== [-60] JS_StrictlyEqual makes inferences about the type when it fails
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is unknown because 'v1' is unknown
//=     JS::RootedValue val1(cx, v1);
//=                              ^~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is undefined or null
//=   bool isNullOrUndefined() const { return value().isNullOrUndefined(); }
//=                                           ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1.ptr' is undefined
//=         if (!JS_StrictlyEqual(cx, val1, val2, &equal))
//=              ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=         if (!JS_StrictlyEqual(cx, val1, val2, &equal))
//=         ^
//= note: 'equal' is false
//=         if (!equal)
//=             ^~~~~~
//= note: Taking false branch
//=         if (!equal)
//=         ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=             assert(val1.toBoolean());
//=                    ^~~~~~~~~~~~~~~~
//= note: expanded from macro 'assert'
//=      (static_cast <bool> (expr)
//=                           ^~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue v1, JS::HandleValue v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isNullOrUndefined() && val2.isNull()) {
        bool equal;
        if (!JS_StrictlyEqual(cx, val1, val2, &equal))
            return false;
        if (!equal)
            assert(val1.toBoolean());
    }
    return true;
}

//== [-60] JS_LooselyEqual warns if values are known to be different types
//= warning: Call to 'JS_LooselyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS_LooselyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Assuming value is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is null because argument 2 is null
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                              ^~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, JS::NullValue());
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Assuming value is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2.ptr' is a 32-bit integer because argument 2 is a 32-bit integer
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                              ^~~~~~~~~~~~~~~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val2(cx, JS::Int32Value(0));
//=                     ^~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS_LooselyEqual()' can never succeed because first value can only be null and second value can only be a 32-bit integer
//=     if (!JS_LooselyEqual(cx, val1, val2, &equal))
//=          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    JS::RootedValue val1(cx, JS::NullValue());
    JS::RootedValue val2(cx, JS::Int32Value(0));
    bool equal;
    if (!JS_LooselyEqual(cx, val1, val2, &equal))
        return false;
    assert(!equal);
    return true;
}

//== [-60] JS_LooselyEqual makes inferences about the type when it succeeds

JSAPI_RETURN_CONVENTION JSObject* ObjectFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSObject* func(JSContext* cx, JS::Value v1, JS::Value v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isObjectOrNull() && val2.isObject()) {
        bool equal;
        if (!JS_LooselyEqual(cx, val1, val2, &equal))
            return nullptr;
        if (equal)
            return &val1.toObject();
    }
    return ObjectFunc(cx);
}

//== [-60] JS_LooselyEqual makes inferences about the type when it fails
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is unknown because 'v1' is unknown
//=     JS::RootedValue val1(cx, v1);
//=                              ^~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val1(cx, v1);
//=                     ^~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val1.ptr' is undefined or null
//=   bool isNullOrUndefined() const { return value().isNullOrUndefined(); }
//=                                           ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNullOrUndefined'
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1.ptr' is undefined
//=         if (!JS_LooselyEqual(cx, val1, val2, &equal))
//=              ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=         if (!JS_LooselyEqual(cx, val1, val2, &equal))
//=         ^
//= note: 'equal' is false
//=         if (!equal)
//=             ^~~~~~
//= note: Taking false branch
//=         if (!equal)
//=         ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=             assert(val1.toBoolean());
//=                    ^~~~~~~~~~~~~~~~
//= note: expanded from macro 'assert'
//=      (static_cast <bool> (expr)
//=                           ^~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1.ptr' is undefined
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue v1, JS::HandleValue v2) {
    JS::RootedValue val1(cx, v1);
    JS::RootedValue val2(cx, v2);
    if (val1.isNullOrUndefined() && val2.isNull()) {
        bool equal;
        if (!JS_LooselyEqual(cx, val1, val2, &equal))
            return false;
        if (!equal)
            assert(val1.toBoolean());
    }
    return true;
}

//== [78-] JS::IsArrayObject puts false in the out param if the value isn't an object

#include <js/Array.h>

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    bool isArray;
    JS::RootedValue val(cx, JS::NullValue());
    if (!JS::IsArrayObject(cx, val, &isArray))
        return false;
    assert(!isArray);
    return true;
}

//== [78-] JS::IsArrayObject assumes value is an object in the true state
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because value is unknown
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Taking false branch
//=     if (!JS::IsArrayObject(cx, val, &isArray))
//=     ^
//= note: 'isArray' is false
//=     if (isArray) {
//=         ^~~~~~~
//= note: Taking false branch
//=     if (isArray) {
//=     ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=     bool b = val.toBoolean();
//=              ^~~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~
//= note: Assuming value is unknown because 'val' is unknown
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because value is unknown
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

#include <js/Array.h>

void DoSomething(JSObject* obj);

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue val) {
    bool isArray;
    if (!JS::IsArrayObject(cx, val, &isArray))
        return false;
    if (isArray) {
        DoSomething(&val.toObject());
        return true;
    }
    bool b = val.toBoolean();
    return true;
}

//== [-68] JS_IsArrayObject puts false in the out param if the value isn't an object

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx) {
    bool isArray;
    JS::RootedValue val(cx, JS::NullValue());
    if (!JS_IsArrayObject(cx, val, &isArray))
        return false;
    assert(!isArray);
    return true;
}

//== [-68] JS_IsArrayObject assumes value is an object in the true state
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because value is unknown
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Taking false branch
//=     if (!JS_IsArrayObject(cx, val, &isArray))
//=     ^
//= note: 'isArray' is false
//=     if (isArray) {
//=         ^~~~~~~
//= note: Taking false branch
//=     if (isArray) {
//=     ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=     bool b = val.toBoolean();
//=              ^~~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~
//= note: Assuming value is unknown because 'val' is unknown
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because value is unknown
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

void DoSomething(JSObject* obj);

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx, JS::HandleValue val) {
    bool isArray;
    if (!JS_IsArrayObject(cx, val, &isArray))
        return false;
    if (isArray) {
        DoSomething(&val.toObject());
        return true;
    }
    bool b = val.toBoolean();
    return true;
}

//== inline array length and data accessors call js::GetReservedSlot().toInt32

#include <jsfriendapi.h>

void func(JSObject* obj) {
    uint32_t len;
    bool isShared;
    uint8_t* data;
    js::GetUint8ArrayLengthAndData(obj, &len, &isShared, &data);
}

//== [68-] JS_TypeOfValue also assumes the type of the value
//= warning: Call to 'JS::Value::toObject()' is incorrect because value is null
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^
//= note: Assuming 'val' is null
//=     JSType type = JS_TypeOfValue(cx, val);
//=                   ^~~~~~~~~~~~~~~~~~~~~~~
//= note: 'type' is not equal to JSTYPE_UNDEFINED
//=     if (type == JSTYPE_UNDEFINED)
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (type == JSTYPE_UNDEFINED)
//=     ^
//= note: 'type' is equal to JSTYPE_NULL
//=     if (type == JSTYPE_NULL)
//=         ^~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (type == JSTYPE_NULL)
//=     ^
//= note: Calling 'WrappedPtrOperations::toObject'
//=         val.toObject();
//=         ^~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Assuming value is null because 'val' is null
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Call to 'JS::Value::toObject()' is incorrect because value is null
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toObject()' is incorrect because value is undefined
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^
//= note: Assuming 'val' is undefined
//=     JSType type = JS_TypeOfValue(cx, val);
//=                   ^~~~~~~~~~~~~~~~~~~~~~~
//= note: 'type' is equal to JSTYPE_UNDEFINED
//=     if (type == JSTYPE_UNDEFINED)
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (type == JSTYPE_UNDEFINED)
//=     ^
//= note: Calling 'WrappedPtrOperations::toObject'
//=         val.toObject();
//=         ^~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Assuming value is undefined because 'val' is undefined
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Call to 'JS::Value::toObject()' is incorrect because value is undefined
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~~~~~~~~~~~~

void func(JSContext* cx, JS::HandleValue val) {
    JSType type = JS_TypeOfValue(cx, val);
    if (type == JSTYPE_UNDEFINED)
        val.toObject();
    if (type == JSTYPE_NULL)
        val.toObject();
    if (type == JSTYPE_STRING)
        val.toString();
    if (type == JSTYPE_NUMBER)
        val.toNumber();
    if (type == JSTYPE_BOOLEAN)
        val.toBoolean();
    if (type == JSTYPE_SYMBOL)
        val.toSymbol();
    if (type == JSTYPE_BIGINT)
        val.toBigInt();
}

//== [-60] JS_TypeOfValue also assumes the type of the value (without bigint)
//= warning: Call to 'JS::Value::toObject()' is incorrect because value is null
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^
//= note: Assuming 'val' is null
//=     JSType type = JS_TypeOfValue(cx, val);
//=                   ^~~~~~~~~~~~~~~~~~~~~~~
//= note: 'type' is not equal to JSTYPE_UNDEFINED
//=     if (type == JSTYPE_UNDEFINED)
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (type == JSTYPE_UNDEFINED)
//=     ^
//= note: 'type' is equal to JSTYPE_NULL
//=     if (type == JSTYPE_NULL)
//=         ^~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (type == JSTYPE_NULL)
//=     ^
//= note: Calling 'WrappedPtrOperations::toObject'
//=         val.toObject();
//=         ^~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Assuming value is null because 'val' is null
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Call to 'JS::Value::toObject()' is incorrect because value is null
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toObject()' is incorrect because value is undefined
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^
//= note: Assuming 'val' is undefined
//=     JSType type = JS_TypeOfValue(cx, val);
//=                   ^~~~~~~~~~~~~~~~~~~~~~~
//= note: 'type' is equal to JSTYPE_UNDEFINED
//=     if (type == JSTYPE_UNDEFINED)
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (type == JSTYPE_UNDEFINED)
//=     ^
//= note: Calling 'WrappedPtrOperations::toObject'
//=         val.toObject();
//=         ^~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Assuming value is undefined because 'val' is undefined
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~
//= note: Call to 'JS::Value::toObject()' is incorrect because value is undefined
//=   JSObject& toObject() const { return value().toObject(); }
//=                                       ^~~~~~~~~~~~~~~~~~

void func(JSContext* cx, JS::HandleValue val) {
    JSType type = JS_TypeOfValue(cx, val);
    if (type == JSTYPE_UNDEFINED)
        val.toObject();
    if (type == JSTYPE_NULL)
        val.toObject();
    if (type == JSTYPE_STRING)
        val.toString();
    if (type == JSTYPE_NUMBER)
        val.toNumber();
    if (type == JSTYPE_BOOLEAN)
        val.toBoolean();
    if (type == JSTYPE_SYMBOL)
        val.toSymbol();
}

//== JS_TypeOfValue has some odd handling around function/object

JSObject* func(JSContext* cx, JS::HandleValue val) {
    if (JS_TypeOfValue(cx, val) != JSTYPE_FUNCTION)
        return nullptr;
    return &val.toObject();
}

//== JS_TypeOfValue deals only with ordinary JS values
//= warning: Call to 'JS_TypeOfValue()' is incorrect because value is a magic value
//=     JS_TypeOfValue(cx, val);
//=     ^~~~~~~~~~~~~~~~~~~~~~~
//= note: Calling 'MutableWrappedPtrOperations::setMagic'
//=     val.setMagic(JS_GENERIC_MAGIC);
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val.ptr' is a magic value
//=   void setMagic(JSWhyMagic why) { value().setMagic(why); }
//=                                   ^~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'MutableWrappedPtrOperations::setMagic'
//=     val.setMagic(JS_GENERIC_MAGIC);
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS_TypeOfValue()' is incorrect because value is a magic value
//=     JS_TypeOfValue(cx, val);
//=     ^~~~~~~~~~~~~~~~~~~~~~~

void func(JSContext* cx) {
    JS::RootedValue val(cx);
    val.setMagic(JS_GENERIC_MAGIC);
    JS_TypeOfValue(cx, val);
}
