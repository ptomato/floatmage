//== isBoolean/setBoolean/toBoolean used correctly

bool func(JS::Value val) {
    if (!val.isBoolean())
        val.setBoolean(true);
    return val.toBoolean();
}

//== JS::BooleanValue sets the type tag correctly

bool func() {
    JS::Value val = JS::BooleanValue(true);
    return val.toBoolean();
}

//== JS::TrueValue sets the type tag correctly

bool func() {
    JS::Value val = JS::TrueValue();
    return val.toBoolean();
}

//== JS::FalseValue sets the type tag correctly

bool func() {
    JS::Value val = JS::FalseValue();
    return val.toBoolean();
}

//== toBoolean on unknown JS::Value
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val' is unknown
//= bool func(JS::Value val) { return val.toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val' is unknown
//= bool func(JS::Value val) { return val.toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~

bool func(JS::Value val) { return val.toBoolean(); }

//== toBoolean on JS::Value known not to be a boolean
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val' is null
//=     return val.toBoolean();
//=            ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val' is null
//=     return val.toBoolean();
//=            ^~~~~~~~~~~~~~~

bool func(JS::Value val) {
    val.setNull();
    return val.toBoolean();
}

//== isTrue/isFalse on boolean value

bool func(JS::Value val) {
    val.setBoolean(true);
    return val.isTrue() || val.isFalse();
}

//== isTrue/isFalse on possibly boolean value narrows the tag

bool func(JS::Value val) {
    if (val.isTrue())
        return val.toBoolean();
    if (val.isFalse())
        return val.toBoolean();
    return false;
}
