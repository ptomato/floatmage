//== isString/setString/toString used correctly

JSString* StringFunc();

JSString* func(JS::Value val) {
    if (!val.isString()) {
        JSString* str = StringFunc();
        if (!str)
            return nullptr;
        val.setString(str);
    }
    return val.toString();
}

//== JS::StringValue sets the type tag correctly

JSString* StringFunc();

JSString* func() {
    JSString* str = StringFunc();
    if (!str)
        return nullptr;
    JS::Value val = JS::StringValue(str);
    return val.toString();
}

//== toString on unknown JS::Value
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is unknown
//= JSString* func(JS::Value val) { return val.toString(); }
//=                                        ^~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is unknown
//= JSString* func(JS::Value val) { return val.toString(); }
//=                                        ^~~~~~~~~~~~~~

JSString* func(JS::Value val) { return val.toString(); }

//== toString on JS::Value known not to be a string
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is null
//=     return val.toString();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is null
//=     return val.toString();
//=            ^~~~~~~~~~~~~~

JSString* func(JS::Value val) {
    val.setNull();
    return val.toString();
}

//== setString must be nonnull
//= warning: Argument 0 to 'JS::Value::setString()' must not be null
//=     val.setString(nullptr);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setString()' must not be null
//=     val.setString(nullptr);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= warning: Argument 0 to 'JS::Value::setString()' must not be null
//=     val.setString(StringFunc());
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setString()' must not be null
//=     val.setString(StringFunc());
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~

JSString* StringFunc();

void func(JS::Value val) {
    val.setString(nullptr);
    val.setString(StringFunc());
}

//== JS::StringValue must be nonnull
//= warning: Argument 0 to 'JS::Value::setString()' must not be null
//=   v.setString(str);
//=   ^
//= note: Calling 'StringValue'
//= JS::Value func2() { return JS::StringValue(StringFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setString()' must not be null
//=   v.setString(str);
//=   ^
//= warning: Argument 0 to 'JS::StringValue()' must not be null
//= JS::Value func1() { return JS::StringValue(nullptr); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::StringValue()' must not be null
//= JS::Value func1() { return JS::StringValue(nullptr); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~
//= warning: Argument 0 to 'JS::StringValue()' must not be null
//= JS::Value func2() { return JS::StringValue(StringFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::StringValue()' must not be null
//= JS::Value func2() { return JS::StringValue(StringFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JSString* StringFunc();

JS::Value func1() { return JS::StringValue(nullptr); }

JS::Value func2() { return JS::StringValue(StringFunc()); }

//== toString returns a nonnull pointer

JSAPI_RETURN_CONVENTION JSString* StringFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JSString* func(JSContext* cx, JS::Value val) {
    if (val.isString())
        return val.toString();
    return StringFunc(cx);
}

//== isSymbol/setSymbol/toSymbol used correctly

JS::Symbol* SymbolFunc();

JS::Symbol* func(JS::Value val) {
    if (!val.isSymbol()) {
        JS::Symbol* sym = SymbolFunc();
        if (!sym)
            return nullptr;
        val.setSymbol(sym);
    }
    return val.toSymbol();
}

//== JS::SymbolValue sets the type tag correctly

JS::Symbol* SymbolFunc();

JS::Symbol* func() {
    JS::Symbol* sym = SymbolFunc();
    if (!sym)
        return nullptr;
    JS::Value val = JS::SymbolValue(sym);
    return val.toSymbol();
}

//== toSymbol on unknown JS::Value
//= warning: Call to 'JS::Value::toSymbol()' is incorrect because 'val' is unknown
//= JS::Symbol* func(JS::Value val) { return val.toSymbol(); }
//=                                          ^~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toSymbol()' is incorrect because 'val' is unknown
//= JS::Symbol* func(JS::Value val) { return val.toSymbol(); }
//=                                          ^~~~~~~~~~~~~~

JS::Symbol* func(JS::Value val) { return val.toSymbol(); }

//== toSymbol on JS::Value known not to be a symbol
//= warning: Call to 'JS::Value::toSymbol()' is incorrect because 'val' is null
//=     return val.toSymbol();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toSymbol()' is incorrect because 'val' is null
//=     return val.toSymbol();
//=            ^~~~~~~~~~~~~~

JS::Symbol* func(JS::Value val) {
    val.setNull();
    return val.toSymbol();
}

//== setSymbol must be nonnull
//= warning: Argument 0 to 'JS::Value::setSymbol()' must not be null
//=     val.setSymbol(nullptr);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setSymbol()' must not be null
//=     val.setSymbol(nullptr);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= warning: Argument 0 to 'JS::Value::setSymbol()' must not be null
//=     val.setSymbol(SymbolFunc());
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setSymbol()' must not be null
//=     val.setSymbol(SymbolFunc());
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~

JS::Symbol* SymbolFunc();

void func(JS::Value val) {
    val.setSymbol(nullptr);
    val.setSymbol(SymbolFunc());
}

//== JS::SymbolValue must be nonnull
//= warning: Argument 0 to 'JS::Value::setSymbol()' must not be null
//=   v.setSymbol(sym);
//=   ^
//= note: Calling 'SymbolValue'
//= JS::Value func2() { return JS::SymbolValue(SymbolFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setSymbol()' must not be null
//=   v.setSymbol(sym);
//=   ^
//= warning: Argument 0 to 'JS::SymbolValue()' must not be null
//= JS::Value func1() { return JS::SymbolValue(nullptr); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::SymbolValue()' must not be null
//= JS::Value func1() { return JS::SymbolValue(nullptr); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~
//= warning: Argument 0 to 'JS::SymbolValue()' must not be null
//= JS::Value func2() { return JS::SymbolValue(SymbolFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::SymbolValue()' must not be null
//= JS::Value func2() { return JS::SymbolValue(SymbolFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JS::Symbol* SymbolFunc();

JS::Value func1() { return JS::SymbolValue(nullptr); }

JS::Value func2() { return JS::SymbolValue(SymbolFunc()); }

//== toSymbol returns a nonnull pointer

JSAPI_RETURN_CONVENTION JS::Symbol* SymbolFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JS::Symbol* func(JSContext* cx, JS::Value val) {
    if (val.isSymbol())
        return val.toSymbol();
    return SymbolFunc(cx);
}

//== [68-] isBigInt/setBigInt/toBigInt used correctly

JS::BigInt* BigIntFunc();

JS::BigInt* func(JS::Value val) {
    if (!val.isBigInt()) {
        JS::BigInt* bi = BigIntFunc();
        if (!bi)
            return nullptr;
        val.setBigInt(bi);
    }
    return val.toBigInt();
}

//== [68-] JS::BigIntValue sets the type tag correctly

JS::BigInt* BigIntFunc();

JS::BigInt* func() {
    JS::BigInt* bi = BigIntFunc();
    if (!bi)
        return nullptr;
    JS::Value val = JS::BigIntValue(bi);
    return val.toBigInt();
}

//== [68-] toBigInt on unknown JS::Value
//= warning: Call to 'JS::Value::toBigInt()' is incorrect because 'val' is unknown
//= JS::BigInt* func(JS::Value val) { return val.toBigInt(); }
//=                                          ^~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBigInt()' is incorrect because 'val' is unknown
//= JS::BigInt* func(JS::Value val) { return val.toBigInt(); }
//=                                          ^~~~~~~~~~~~~~

JS::BigInt* func(JS::Value val) { return val.toBigInt(); }

//== [68-] toBigInt on JS::Value known not to be a BigInt
//= warning: Call to 'JS::Value::toBigInt()' is incorrect because 'val' is null
//=     return val.toBigInt();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBigInt()' is incorrect because 'val' is null
//=     return val.toBigInt();
//=            ^~~~~~~~~~~~~~

JS::BigInt* func(JS::Value val) {
    val.setNull();
    return val.toBigInt();
}

//== [68-] setBigInt must be nonnull
//= warning: Argument 0 to 'JS::Value::setBigInt()' must not be null
//=     val.setBigInt(nullptr);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setBigInt()' must not be null
//=     val.setBigInt(nullptr);
//=     ^~~~~~~~~~~~~~~~~~~~~~
//= warning: Argument 0 to 'JS::Value::setBigInt()' must not be null
//=     val.setBigInt(BigIntFunc());
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setBigInt()' must not be null
//=     val.setBigInt(BigIntFunc());
//=     ^~~~~~~~~~~~~~~~~~~~~~~~~~~

JS::BigInt* BigIntFunc();

void func(JS::Value val) {
    val.setBigInt(nullptr);
    val.setBigInt(BigIntFunc());
}

//== [68-] JS::BigIntValue must be nonnull
//= warning: Argument 0 to 'JS::Value::setBigInt()' must not be null
//=   v.setBigInt(bi);
//=   ^
//= note: Calling 'BigIntValue'
//= JS::Value func2() { return JS::BigIntValue(BigIntFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::Value::setBigInt()' must not be null
//=   v.setBigInt(bi);
//=   ^
//= warning: Argument 0 to 'JS::BigIntValue()' must not be null
//= JS::Value func1() { return JS::BigIntValue(nullptr); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::BigIntValue()' must not be null
//= JS::Value func1() { return JS::BigIntValue(nullptr); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~
//= warning: Argument 0 to 'JS::BigIntValue()' must not be null
//= JS::Value func2() { return JS::BigIntValue(BigIntFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Argument 0 to 'JS::BigIntValue()' must not be null
//= JS::Value func2() { return JS::BigIntValue(BigIntFunc()); }
//=                            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~

JS::BigInt* BigIntFunc();

JS::Value func1() { return JS::BigIntValue(nullptr); }

JS::Value func2() { return JS::BigIntValue(BigIntFunc()); }

//== [68-] toBigInt returns a nonnull pointer

JSAPI_RETURN_CONVENTION JS::BigInt* BigIntFunc(JSContext* cx);

JSAPI_RETURN_CONVENTION
JS::BigInt* func(JSContext* cx, JS::Value val) {
    if (val.isBigInt())
        return val.toBigInt();
    return BigIntFunc(cx);
}

//== isGCThing/toGCThing/toGCCellPtr used correctly

JSObject* ObjectFunc();

js::gc::Cell* func1(JS::Value val) {
    if (!val.isGCThing()) {
        JSObject* obj = ObjectFunc();
        if (!obj)
            return nullptr;
        val.setObject(*obj);
    }
    return val.toGCThing();
}

JS::GCCellPtr func2(JS::Value val) {
    if (!val.isGCThing()) {
        JSObject* obj = ObjectFunc();
        if (!obj)
            return nullptr;
        val.setObject(*obj);
    }
    return val.toGCCellPtr();
}

//== toGCThing/toGCCellPtr on unknown JS::Value
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is unknown
//=   GCCellPtr toGCCellPtr() const { return GCCellPtr(toGCThing(), traceKind()); }
//=                                                    ^
//= note: Calling 'Value::toGCCellPtr'
//= JS::GCCellPtr func2(JS::Value val) { return val.toGCCellPtr(); }
//=                                             ^~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is unknown
//=   GCCellPtr toGCCellPtr() const { return GCCellPtr(toGCThing(), traceKind()); }
//=                                                    ^~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is unknown
//= js::gc::Cell* func1(JS::Value val) { return val.toGCThing(); }
//=                                             ^~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is unknown
//= js::gc::Cell* func1(JS::Value val) { return val.toGCThing(); }
//=                                             ^~~~~~~~~~~~~~~

js::gc::Cell* func1(JS::Value val) { return val.toGCThing(); }

JS::GCCellPtr func2(JS::Value val) { return val.toGCCellPtr(); }

//== toGCThing/toGCCellPtr on JS::Value known not to be a GC thing
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a boolean
//=   GCCellPtr toGCCellPtr() const { return GCCellPtr(toGCThing(), traceKind()); }
//=                                                    ^
//= note: Assuming 'val' is a boolean
//=     val.setBoolean(true);
//=     ^~~~~~~~~~~~~~~~~~~~
//= note: Calling 'Value::toGCCellPtr'
//=     return val.toGCCellPtr();
//=            ^~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a boolean
//=   GCCellPtr toGCCellPtr() const { return GCCellPtr(toGCThing(), traceKind()); }
//=                                                    ^~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a boolean
//=     return val.toGCThing();
//=            ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is a boolean
//=     val.setBoolean(true);
//=     ^~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a boolean
//=     return val.toGCThing();
//=            ^~~~~~~~~~~~~~~

js::gc::Cell* func1(JS::Value val) {
    val.setBoolean(true);
    return val.toGCThing();
}

JS::GCCellPtr func2(JS::Value val) {
    val.setBoolean(true);
    return val.toGCCellPtr();
}

//== string, symbol, and object are GC things

JSString* StringFunc();
JS::Symbol* SymbolFunc();
JSObject* ObjectFunc();
void gcFunc(js::gc::Cell* gc1, js::gc::Cell* gc2, js::gc::Cell* gc3);

bool func(JS::Value val1, JS::Value val2, JS::Value val3) {
    JSString* str = StringFunc();
    if (!str)
        return false;
    val1.setString(str);
    JS::Symbol* sym = SymbolFunc();
    if (!sym)
        return false;
    val2.setSymbol(sym);
    JSObject* obj = ObjectFunc();
    if (!obj)
        return false;
    val3.setObject(*obj);
    gcFunc(val1.toGCThing(), val2.toGCThing(), val3.toGCThing());
    return true;
}

//== [68-] bigint is a GC thing

JS::BigInt* BigIntFunc();

js::gc::Cell* func(JS::Value val) {
    JS::BigInt* bi = BigIntFunc();
    if (!bi)
        return nullptr;
    val.setBigInt(bi);
    return val.toGCThing();
}

//== anything else is not a GC thing
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a double
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is a double
//=     if (val.isDouble())
//=         ^~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isDouble())
//=     ^
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a double
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a 32-bit integer
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but a double
//=     if (val.isDouble())
//=         ^~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isDouble())
//=     ^
//= note: Assuming 'val' is a 32-bit integer
//=     if (val.isInt32())
//=         ^~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isInt32())
//=     ^
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a 32-bit integer
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a boolean
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but a double
//=     if (val.isDouble())
//=         ^~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isDouble())
//=     ^
//= note: Assuming 'val' is anything but a double or a 32-bit integer
//=     if (val.isInt32())
//=         ^~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isInt32())
//=     ^
//= note: Assuming 'val' is a boolean
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isBoolean())
//=     ^
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is a boolean
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is undefined
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but a double
//=     if (val.isDouble())
//=         ^~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isDouble())
//=     ^
//= note: Assuming 'val' is anything but a double or a 32-bit integer
//=     if (val.isInt32())
//=         ^~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isInt32())
//=     ^
//= note: Assuming 'val' is anything but a double, a 32-bit integer, or a boolean
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isBoolean())
//=     ^
//= note: Assuming 'val' is undefined
//=     if (val.isUndefined())
//=         ^~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isUndefined())
//=     ^
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is undefined
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is null
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but a double
//=     if (val.isDouble())
//=         ^~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isDouble())
//=     ^
//= note: Assuming 'val' is anything but a double or a 32-bit integer
//=     if (val.isInt32())
//=         ^~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isInt32())
//=     ^
//= note: Assuming 'val' is anything but a double, a 32-bit integer, or a boolean
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isBoolean())
//=     ^
//= note: Assuming 'val' is anything but a double, a 32-bit integer, a boolean, or undefined
//=     if (val.isUndefined())
//=         ^~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isUndefined())
//=     ^
//= note: Assuming 'val' is null
//=     if (val.isNull())
//=         ^~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isNull())
//=     ^
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is null
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is magic
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but a double
//=     if (val.isDouble())
//=         ^~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isDouble())
//=     ^
//= note: Assuming 'val' is anything but a double or a 32-bit integer
//=     if (val.isInt32())
//=         ^~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isInt32())
//=     ^
//= note: Assuming 'val' is anything but a double, a 32-bit integer, or a boolean
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isBoolean())
//=     ^
//= note: Assuming 'val' is anything but a double, a 32-bit integer, a boolean, or undefined
//=     if (val.isUndefined())
//=         ^~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isUndefined())
//=     ^
//= note: Assuming 'val' is anything but a double, a 32-bit integer, a boolean, undefined, or null
//=     if (val.isNull())
//=         ^~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isNull())
//=     ^
//= note: Assuming 'val' is magic
//=     if (val.isMagic())
//=         ^~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isMagic())
//=     ^
//= note: Call to 'JS::Value::toGCThing()' is incorrect because 'val' is magic
//=         gcFunc(val.toGCThing());
//=                ^~~~~~~~~~~~~~~

void gcFunc(js::gc::Cell* gc);

void func(JS::Value val) {
    if (val.isDouble())
        gcFunc(val.toGCThing());
    if (val.isInt32())
        gcFunc(val.toGCThing());
    if (val.isBoolean())
        gcFunc(val.toGCThing());
    if (val.isUndefined())
        gcFunc(val.toGCThing());
    if (val.isNull())
        gcFunc(val.toGCThing());
    if (val.isMagic())
        gcFunc(val.toGCThing());
}
