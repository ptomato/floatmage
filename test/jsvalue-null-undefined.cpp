//== isNull/setNull used correctly

void func(JS::Value val) {
    if (!val.isNull())
        val.setNull();
}

//== setNull tracks the type tag correctly
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is null
//=     return val.toString();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is null
//=     return val.toString();
//=            ^~~~~~~~~~~~~~

JSString* func(JS::Value val) {
    val.setNull();
    return val.toString();
}

//== JS::NullValue sets the type tag correctly
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is null
//=     return val.toString();
//=            ^~~~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::Value val = JS::NullValue();
//=                     ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     JS::Value val = JS::NullValue();
//=                     ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::Value val = JS::NullValue();
//=                     ^~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is null
//=     return val.toString();
//=            ^~~~~~~~~~~~~~

JSString* func() {
    JS::Value val = JS::NullValue();
    return val.toString();
}

//== isUndefined/setUndefined used correctly

void func(JS::Value val) {
    if (!val.isUndefined())
        val.setUndefined();
}

//== setUndefined tracks the type tag correctly
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined
//=     return val.toString();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is undefined
//=     val.setUndefined();
//=     ^~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined
//=     return val.toString();
//=            ^~~~~~~~~~~~~~

JSString* func(JS::Value val) {
    val.setUndefined();
    return val.toString();
}

//== JS::UndefinedValue sets the type tag correctly
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined
//=     return val.toString();
//=            ^~~~~~~~~~~~~~
//- note: Calling 'UndefinedValue'
//-     JS::Value val = JS::UndefinedValue();
//-                     ^~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val' is undefined
//=     JS::Value val = JS::UndefinedValue();
//=                     ^~~~~~~~~~~~~~~~~~~~
//- note: Returning from 'UndefinedValue'
//-     JS::Value val = JS::UndefinedValue();
//-                     ^~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined
//=     return val.toString();
//=            ^~~~~~~~~~~~~~

JSString* func() {
    JS::Value val = JS::UndefinedValue();
    return val.toString();
}

//== isNullOrUndefined
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined or null
//=         DoSomething(val.toString());
//=                     ^~~~~~~~~~~~~~
//= note: Assuming 'val' is undefined or null
//=     if (val.isNullOrUndefined())
//=         ^~~~~~~~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isNullOrUndefined())
//=     ^
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined or null
//=         DoSomething(val.toString());
//=                     ^~~~~~~~~~~~~~

void DoSomething(JSString*);

void func(JS::Value val) {
    if (val.isNullOrUndefined())
        DoSomething(val.toString());
}

//== null and undefined are NullOrUndefined

bool func(JS::Value val1, JS::Value val2) {
    val1.setNull();
    val2.setUndefined();
    return val1.isNullOrUndefined() && val2.isNullOrUndefined();
}

//== representative sample of nothing else is NullOrUndefined

bool func(JS::Value val1, JS::Value val2, JS::Value val3, JS::Value val4,
          JS::Value val5, JS::Value val6, JS::Value val7) {
    val1.setDouble(0);
    val2.setInt32(0);
    val3.setBoolean(true);
    return val1.isNullOrUndefined() || val2.isNullOrUndefined() ||
           val3.isNullOrUndefined();
}
