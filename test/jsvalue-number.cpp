//== isInt32/setInt32/toInt32 used correctly

uint32_t func(JS::Value val) {
    if (!val.isInt32())
        val.setInt32(0);
    return val.toInt32();
}

//== JS::Int32Value sets the type tag correctly

uint32_t func() {
    JS::Value val = JS::Int32Value(0);
    return val.toInt32();
}

//== int32 static constructor

uint32_t func() { return JS::Value::fromInt32(0).toInt32(); }

//== toInt32 on unknown JS::Value
//= warning: Call to 'JS::Value::toInt32()' is incorrect because 'val' is unknown
//= uint32_t func(JS::Value val) { return val.toInt32(); }
//=                                       ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toInt32()' is incorrect because 'val' is unknown
//= uint32_t func(JS::Value val) { return val.toInt32(); }
//=                                       ^~~~~~~~~~~~~

uint32_t func(JS::Value val) { return val.toInt32(); }

//== toInt32 on JS::Value known not to be an int32
//= warning: Call to 'JS::Value::toInt32()' is incorrect because 'val' is null
//=     return val.toInt32();
//=            ^~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toInt32()' is incorrect because 'val' is null
//=     return val.toInt32();
//=            ^~~~~~~~~~~~~

uint32_t func(JS::Value val) {
    val.setNull();
    return val.toInt32();
}

//== isInt32(arg) on int32 value

bool func(JS::Value val) {
    val.setInt32(0);
    return val.isInt32(0);
}

//== isInt32(arg) on possibly int32 value narrows the tag

uint32_t func(JS::Value val) {
    if (val.isInt32(0))
        return val.toInt32();
    return 0;
}

//== isDouble/setDouble/toDouble used correctly

double func(JS::Value val) {
    if (!val.isDouble())
        val.setDouble(0);
    return val.toDouble();
}

//== [-68] setNaN sets the type tag correctly

double func(JS::Value val) {
    val.setNaN();
    return val.toDouble();
}

//== JS::DoubleValue sets the type tag correctly

double func() {
    JS::Value val = JS::DoubleValue(0);
    return val.toDouble();
}

//== JS::CanonicalizedDoubleValue sets the type tag correctly

double func() {
    JS::Value val = JS::CanonicalizedDoubleValue(0);
    return val.toDouble();
}

//== [-68] JS::DoubleNaNValue sets the type tag correctly

double func() {
    JS::Value val = JS::DoubleNaNValue();
    return val.toDouble();
}

//== [-68] JS_GetNaNValue sets the type tag correctly

double func(JSContext* cx) {
    JS::Value val = JS_GetNaNValue(cx);
    return val.toDouble();
}

//== [78-] JS::NaNValue sets the type tag correctly

double func() {
    JS::Value val = JS::NaNValue();
    return val.toDouble();
}

//== [-68] JS_GetPositiveInfinityValue sets the type tag correctly

double func(JSContext* cx) {
    JS::Value val = JS_GetPositiveInfinityValue(cx);
    return val.toDouble();
}

//== [-68] JS_GetNegativeInfinityValue sets the type tag correctly

double func(JSContext* cx) {
    JS::Value val = JS_GetNegativeInfinityValue(cx);
    return val.toDouble();
}

//== [78-] JS::InfinityValue sets the type tag correctly

double func() {
    JS::Value val = JS::InfinityValue();
    return val.toDouble();
}

//== JS::Float32Value sets the type tag correctly

double func() {
    JS::Value val = JS::Float32Value(0);
    return val.toDouble();
}

//== double static constructor

double func() { return JS::Value::fromDouble(0).toDouble(); }

//== toDouble on unknown JS::Value
//= warning: Call to 'JS::Value::toDouble()' is incorrect because 'val' is unknown
//= double func(JS::Value val) { return val.toDouble(); }
//=                                     ^~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toDouble()' is incorrect because 'val' is unknown
//= double func(JS::Value val) { return val.toDouble(); }
//=                                     ^~~~~~~~~~~~~~

double func(JS::Value val) { return val.toDouble(); }

//== toDouble on JS::Value known not to be a double
//= warning: Call to 'JS::Value::toDouble()' is incorrect because 'val' is null
//=     return val.toDouble();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toDouble()' is incorrect because 'val' is null
//=     return val.toDouble();
//=            ^~~~~~~~~~~~~~

double func(JS::Value val) {
    val.setNull();
    return val.toDouble();
}

//== isNumber/setNumber/toNumber used correctly

double func(JS::Value val1, JS::Value val2, uint32_t fallback) {
    if (!val1.isNumber())
        val1.setNumber(3.14);
    if (!val2.isNumber())
        val2.setNumber(fallback);
    return val1.toNumber() + val2.toNumber();
}

//== JS::NumberValue sets the type tag correctly

double func(uint32_t a_int) {
    JS::Value val1 = JS::NumberValue(3.14);
    JS::Value val2 = JS::NumberValue(a_int);
    return val1.toNumber() + val2.toNumber();
}

//== toNumber on unknown JS::Value
//= warning: Call to 'JS::Value::toNumber()' is incorrect because 'val' is unknown
//= double func(JS::Value val) { return val.toNumber(); }
//=                                     ^~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toNumber()' is incorrect because 'val' is unknown
//= double func(JS::Value val) { return val.toNumber(); }
//=                                     ^~~~~~~~~~~~~~

double func(JS::Value val) { return val.toNumber(); }

//== toNumber on JS::Value known not to be a number
//= warning: Call to 'JS::Value::toNumber()' is incorrect because 'val' is null
//=     return val.toNumber();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toNumber()' is incorrect because 'val' is null
//=     return val.toNumber();
//=            ^~~~~~~~~~~~~~

double func(JS::Value val) {
    val.setNull();
    return val.toNumber();
}

//== setNumber(uint32_t) sets int or double appropriately if argument is known

double func(JS::Value val1, JS::Value val2) {
    val1.setNumber(uint32_t(0));
    val2.setNumber(uint32_t(0xffffffff));
    return val1.toInt32() + val2.toDouble();
}

//== JS::NumberValue(int8_t) sets int

uint32_t func(int8_t i) {
    JS::Value val = JS::NumberValue(i);
    return val.toInt32();
}

//== JS::NumberValue(uint8_t) sets int

uint32_t func(uint8_t i) {
    JS::Value val = JS::NumberValue(i);
    return val.toInt32();
}

//== JS::NumberValue(int16_t) sets int

uint32_t func(int16_t i) {
    JS::Value val = JS::NumberValue(i);
    return val.toInt32();
}

//== JS::NumberValue(uint16_t) sets int

uint32_t func(uint16_t i) {
    JS::Value val = JS::NumberValue(i);
    return val.toInt32();
}

//== JS::NumberValue(int32_t) sets int

uint32_t func(int32_t i) {
    JS::Value val = JS::NumberValue(i);
    return val.toInt32();
}

//== JS::NumberValue(uint32_t) sets int or double appropriately if argument is known

double func() {
    JS::Value val1 = JS::NumberValue(uint32_t(0));
    JS::Value val2 = JS::NumberValue(uint32_t(0xffffffff));
    return val1.toInt32() + val2.toDouble();
}

//== JS::NumberValue(T) sets int or double appropriately if argument is known

double func() {
    JS::Value val1 = JS::NumberValue(int64_t(0));
    JS::Value val2 = JS::NumberValue(int64_t(0xffffffff));
    JS::Value val3 = JS::NumberValue(uint64_t(0));
    JS::Value val4 = JS::NumberValue(uint64_t(0xffffffff));
    return val1.toInt32() + val2.toDouble() + val3.toInt32() + val4.toDouble();
}

//== int32 and double are numbers

void DoSomething(double);

void func(JS::Value val1, JS::Value val2) {
    val1.setInt32(0);
    DoSomething(val1.toNumber());
    val2.setDouble(0);
    DoSomething(val2.toNumber());
}

//== representative sample of anything else is not numbers
//= warning: Call to 'JS::Value::toNumber()' is incorrect because 'val' is null
//=         DoSomething(val.toNumber());
//=                     ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     if (val.isNull())
//=         ^~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isNull())
//=     ^
//= note: Call to 'JS::Value::toNumber()' is incorrect because 'val' is null
//=         DoSomething(val.toNumber());
//=                     ^~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toNumber()' is incorrect because 'val' is undefined
//=         DoSomething(val.toNumber());
//=                     ^~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but null
//=     if (val.isNull())
//=         ^~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isNull())
//=     ^
//= note: Assuming 'val' is undefined
//=     if (val.isUndefined())
//=         ^~~~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isUndefined())
//=     ^
//= note: Call to 'JS::Value::toNumber()' is incorrect because 'val' is undefined
//=         DoSomething(val.toNumber());
//=                     ^~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toNumber()' is incorrect because 'val' is a boolean
//=         DoSomething(val.toNumber());
//=                     ^~~~~~~~~~~~~~
//= note: Assuming 'val' is anything but null
//=     if (val.isNull())
//=         ^~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isNull())
//=     ^
//= note: Assuming 'val' is anything but undefined or null
//=     if (val.isUndefined())
//=         ^~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isUndefined())
//=     ^
//= note: Assuming 'val' is a boolean
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isBoolean())
//=     ^
//= note: Call to 'JS::Value::toNumber()' is incorrect because 'val' is a boolean
//=         DoSomething(val.toNumber());
//=                     ^~~~~~~~~~~~~~

void DoSomething(double number);

void func(JS::Value val) {
    if (val.isNull())
        DoSomething(val.toNumber());
    if (val.isUndefined())
        DoSomething(val.toNumber());
    if (val.isBoolean())
        DoSomething(val.toNumber());
}

//== [78-] int32, double, and bigint are numeric

JS::BigInt* BigIntFunc();

bool func(JS::Value val1, JS::Value val2, JS::Value val3) {
    val1.setInt32(0);
    val2.setDouble(0);
    val3.setBigInt(BigIntFunc());
    return val1.isNumeric() && val2.isNumeric() && val3.isNumeric();
}

//== [78-] representative sample of anything else is not numeric

bool func(JS::Value val1, JS::Value val2, JS::Value val3) {
    val1.setNull();
    val2.setUndefined();
    val3.setBoolean(true);
    return val1.isNumeric() || val2.isNumeric() || val3.isNumeric();
}
