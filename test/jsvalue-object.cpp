//== isObject/setObject/toObject used correctly

JSObject* ObjectFunc();

JSObject* func(JS::Value val) {
    if (!val.isObject()) {
        JSObject* obj = ObjectFunc();
        if (!obj)
            return nullptr;
        val.setObject(*obj);
    }
    return &val.toObject();
}

//== JS::ObjectValue sets the type tag correctly

JSObject* ObjectFunc();

JSObject* func() {
    JSObject* obj = ObjectFunc();
    if (!obj)
        return nullptr;
    JS::Value val = JS::ObjectValue(*obj);
    return &val.toObject();
}

//== toObject on unknown JS::Value
//= warning: Call to 'JS::Value::toObject()' is incorrect because 'val' is unknown
//= JSObject* func(JS::Value val) { return &val.toObject(); }
//=                                         ^~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toObject()' is incorrect because 'val' is unknown
//= JSObject* func(JS::Value val) { return &val.toObject(); }
//=                                         ^~~~~~~~~~~~~~

JSObject* func(JS::Value val) { return &val.toObject(); }

//== toObject on JS::Value known not to be an object
//= warning: Call to 'JS::Value::toObject()' is incorrect because 'val' is null
//=     return &val.toObject();
//=             ^~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toObject()' is incorrect because 'val' is null
//=     return &val.toObject();
//=             ^~~~~~~~~~~~~~

JSObject* func(JS::Value val) {
    val.setNull();
    return &val.toObject();
}

//== isObjectOrNull/setObjectOrNull/toObjectOrNull used correctly

JSObject* ObjectFunc();

JSObject* func(JS::Value val) {
    if (!val.isObjectOrNull())
        val.setObjectOrNull(ObjectFunc());
    return val.toObjectOrNull();
}

//== JS::ObjectOrNullValue sets type tag correctly

JSObject* ObjectFunc();

JSObject* func() {
    JS::Value val = JS::ObjectOrNullValue(ObjectFunc());
    return val.toObjectOrNull();
}

//== toObjectOrNull on unknown JS::Value
//= warning: Call to 'JS::Value::toObjectOrNull()' is incorrect because 'val' is unknown
//= JSObject* func(JS::Value val) { return val.toObjectOrNull(); }
//=                                        ^~~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toObjectOrNull()' is incorrect because 'val' is unknown
//= JSObject* func(JS::Value val) { return val.toObjectOrNull(); }
//=                                        ^~~~~~~~~~~~~~~~~~~~

JSObject* func(JS::Value val) { return val.toObjectOrNull(); }

//== toObjectOrNull on JS::Value known not to be an object or null
//= warning: Call to 'JS::Value::toObjectOrNull()' is incorrect because 'val' is a 32-bit integer
//=     return val.toObjectOrNull();
//=            ^~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val' is a 32-bit integer
//=     val.setInt32(0);
//=     ^~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toObjectOrNull()' is incorrect because 'val' is a 32-bit integer
//=     return val.toObjectOrNull();
//=            ^~~~~~~~~~~~~~~~~~~~

JSObject* func(JS::Value val) {
    val.setInt32(0);
    return val.toObjectOrNull();
}

//== setObjectOrNull sets type tag more specifically if known

JSObject* ObjectFunc();

bool func(JS::Value val) {
    JSObject* obj = ObjectFunc();
    val.setObjectOrNull(obj);
    if (obj) {
        return val.isObject();
    } else {
        return val.isNull();
    }
}

//== JS::ObjectOrNullValue sets type tag more specifically if known

JSObject* ObjectFunc();

bool func() {
    JSObject* obj = ObjectFunc();
    JS::Value val = JS::ObjectOrNullValue(obj);
    if (obj) {
        return val.isObject();
    } else {
        return val.isNull();
    }
}

//== null is an object-or-null

JSObject* func(JS::Value val) {
    val.setNull();
    return val.toObjectOrNull();
}

//== object is an object-or-null

JSObject* ObjectFunc();

JSObject* func(JS::Value val) {
    JSObject* obj = ObjectFunc();
    if (!obj)
        return nullptr;
    val.setObject(*obj);
    return val.toObjectOrNull();
}

//== object is not primitive

bool func(JS::Value val, JSObject* obj) {
    if (!obj)
        return false;
    val.setObject(*obj);
    assert(!val.isPrimitive());
    return true;
}

//== anything else is primitive

bool func(JS::Value val) {
    val.setUndefined();
    return val.isPrimitive();
}
