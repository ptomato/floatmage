//== setMagic/whyMagic used correctly

JSWhyMagic func(JS::Value val) {
    val.setMagic(JS_GENERIC_MAGIC);
    return val.whyMagic();
}

//== setMagicUint32/magicUint32 used correctly

uint32_t func(JS::Value val) {
    val.setMagicUint32(JS_WHY_MAGIC_COUNT + 1);
    return val.magicUint32();
}

//== isMagic used correctly

void func(JS::Value val, bool magic) {
    if (magic)
        val.setMagic(JS_GENERIC_MAGIC);
    else
        val.setMagicUint32(JS_WHY_MAGIC_COUNT + 1);
    assert(val.isMagic());
}

//== magicUint32 covers both magic enum and magic int32

uint32_t func(JS::Value val, bool magic) {
    if (magic)
        val.setMagic(JS_GENERIC_MAGIC);
    else
        val.setMagicUint32(JS_WHY_MAGIC_COUNT + 1);
    return val.magicUint32();
}

//== isMagic(arg) on magic value

bool func(JS::Value val) {
    val.setMagic(JS_GENERIC_MAGIC);
    return val.isMagic(JS_GENERIC_MAGIC);
}

//== isMagic(arg) on possibly magic value narrows the tag

JSWhyMagic func(JS::Value val) {
    if (val.isMagic(JS_GENERIC_MAGIC))
        return val.whyMagic();
    return JS_GENERIC_MAGIC;
}

//== JS::MagicValue sets the type tag correctly

JSWhyMagic func() {
    JS::Value val = JS::MagicValue(JS_GENERIC_MAGIC);
    return val.whyMagic();
}

//== JS::MagicValueUint32 sets the type tag correctly

uint32_t func() {
    JS::Value val = JS::MagicValueUint32(JS_GENERIC_MAGIC);
    return val.magicUint32();
}

//== isMagic(arg) on non-magic value

void func(JS::Value val) {
    val.setNull();
    assert(!val.isMagic(JS_GENERIC_MAGIC));
}

//== setPrivate/toPrivate used correctly

void* PtrFunc();

void* func(JS::Value val) {
    val.setPrivate(PtrFunc());
    return val.toPrivate();
}

//== JS::PrivateValue sets the type tag correctly

void* PtrFunc();

void* func() {
    JS::Value val = JS::PrivateValue(PtrFunc());
    return val.toPrivate();
}

//== toPrivate on unknown JS::Value is allowed

void* func(JS::Value val) { return val.toPrivate(); }

//== toPrivate on JS::Value known not to be a private pointer
//= warning: Call to 'JS::Value::toPrivate()' is incorrect because 'val' is null
//=     return val.toPrivate();
//=            ^~~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toPrivate()' is incorrect because 'val' is null
//=     return val.toPrivate();
//=            ^~~~~~~~~~~~~~~

void* func(JS::Value val) {
    val.setNull();
    return val.toPrivate();
}

//== setPrivateUint32/toPrivateUint32 used correctly

uint32_t func(JS::Value val) {
    val.setPrivateUint32(0);
    return val.toPrivateUint32();
}

//== JS::PrivateUint32Value sets the type tag correctly

uint32_t func() {
    JS::Value val = JS::PrivateUint32Value(0);
    return val.toPrivateUint32();
}

//== toPrivateUint32 on unknown JS::Value is allowed

uint32_t func(JS::Value val) { return val.toPrivateUint32(); }

//== toPrivateUint32 on JS::Value known not to be a private int
//= warning: Call to 'JS::Value::toPrivateUint32()' is incorrect because 'val' is null
//=     return val.toPrivateUint32();
//=            ^~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val' is null
//=     val.setNull();
//=     ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toPrivateUint32()' is incorrect because 'val' is null
//=     return val.toPrivateUint32();
//=            ^~~~~~~~~~~~~~~~~~~~~

uint32_t func(JS::Value val) {
    val.setNull();
    return val.toPrivateUint32();
}

//== setPrivateGCThing/isPrivateGCThing used correctly

js::gc::Cell* CellFunc();

js::gc::Cell* func(JS::Value val) {
    if (!val.isPrivateGCThing())
        val.setPrivateGCThing(CellFunc());
    return val.toGCThing();
}

//== JS::PrivateGCThingValue sets the type tag correctly

js::gc::Cell* CellFunc();

js::gc::Cell* func() {
    JS::Value val = JS::PrivateGCThingValue(CellFunc());
    return val.toGCThing();
}
