//== default after construction is undefined
//= warning: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined
//=     return val.toString();
//=            ^~~~~~~~~~~~~~
//= note: Assuming 'val' is undefined
//=     JS::Value val;
//=               ^~~
//= note: Call to 'JS::Value::toString()' is incorrect because 'val' is undefined
//=     return val.toString();
//=            ^~~~~~~~~~~~~~

JSString* func() {
    JS::Value val;
    return val.toString();
}

//== copy constructor

bool func(JS::Value val1) {
    val1.setBoolean(true);
    JS::Value val2 = val1;
    return val2.toBoolean();
}

//== JS::RootedValue WrappedPtrOperations
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val3.ptr' is unknown
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val3(cx, val1);
//=                     ^~~~~~~~~~~~~~
//= note: Assuming 'val3.ptr' is unknown because 'val1' is unknown
//=     JS::RootedValue val3(cx, val1);
//=                              ^~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue val3(cx, val1);
//=                     ^~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     return val2.toBoolean() && val3.toBoolean();
//=            ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=     return val2.toBoolean() && val3.toBoolean();
//=                                ^~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val3.ptr' is unknown
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^

bool func(JSContext* cx, JS::Value val1) {
    JS::RootedValue val2(cx, JS::TrueValue());
    JS::RootedValue val3(cx, val1);
    return val2.toBoolean() && val3.toBoolean();
}

//== JS::RootedValue traces provenance of values in its constructors
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'rooted1.ptr' is a string
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Assuming 'val1' is a string
//=     if (val1.isString()) {
//=         ^~~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val1.isString()) {
//=     ^
//= note: Calling 'Rooted::operator='
//=         rooted1 = val1;
//=         ^~~~~~~~~~~~~~
//= note: Calling 'Rooted::set'
//=   DECLARE_POINTER_ASSIGN_OPS(Rooted, T);
//=   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: expanded from macro 'DECLARE_POINTER_ASSIGN_OPS'
//=     set(p);                                        \
//=     ^~~~~~
//= note: Assuming 'rooted1.ptr' is a string because 'val1' is a string
//=     ptr = value;
//=     ^~~~~~~~~~~
//= note: Returning from 'Rooted::set'
//=   DECLARE_POINTER_ASSIGN_OPS(Rooted, T);
//=   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: expanded from macro 'DECLARE_POINTER_ASSIGN_OPS'
//=     set(p);                                        \
//=     ^~~~~~
//= note: Returning from 'Rooted::operator='
//=         rooted1 = val1;
//=         ^~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=         return rooted1.toBoolean();
//=                ^~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'rooted1.ptr' is a string
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'rooted2.ptr' is a double
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^
//= note: Calling 'DoubleValue'
//=     JS::Value val2 = JS::DoubleValue(3.14);
//=                      ^~~~~~~~~~~~~~~~~~~~~
//= note: Assuming 'val2' is a double
//=     JS::Value val2 = JS::DoubleValue(3.14);
//=                      ^~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'DoubleValue'
//=     JS::Value val2 = JS::DoubleValue(3.14);
//=                      ^~~~~~~~~~~~~~~~~~~~~
//= note: Calling constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue rooted2(cx, val2);
//=                     ^~~~~~~~~~~~~~~~~
//= note: Assuming 'rooted2.ptr' is a double because 'val2' is a double
//=     JS::RootedValue rooted2(cx, val2);
//=                                 ^~~~
//- note: Loop condition is false.  Exiting loop
//-     MOZ_ASSERT(GCPolicy<T>::isValid(ptr));
//-     ^
//- note: expanded from macro 'MOZ_ASSERT'
//- #define MOZ_ASSERT(...) do {} while (false)
//-                         ^
//= note: Returning from constructor for 'Rooted<JS::Value>'
//=     JS::RootedValue rooted2(cx, val2);
//=                     ^~~~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val1.isString()) {
//=     ^
//= note: Calling 'WrappedPtrOperations::toBoolean'
//=     return rooted2.toBoolean();
//=            ^~~~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'rooted2.ptr' is a double
//=   bool toBoolean() const { return value().toBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~

bool func(JSContext* cx, JS::Value val1) {
    JS::Value val2 = JS::DoubleValue(3.14);
    JS::RootedValue rooted1(cx);
    JS::RootedValue rooted2(cx, val2);
    if (val1.isString()) {
        rooted1 = val1;
        return rooted1.toBoolean();
    }
    return rooted2.toBoolean();
}

//== JS::HandleValue WrappedPtrOperations
//= warning: Call to 'JS::Value::toInt32()' is incorrect because value is a double or a 32-bit integer
//=   int32_t toInt32() const { return value().toInt32(); }
//=                                    ^
//= note: Calling 'WrappedPtrOperations::isBoolean'
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Calling 'WrappedPtrOperations::value'
//=   bool isBoolean() const { return value().isBoolean(); }
//=                                   ^~~~~~~
//= note: Assuming value is unknown because 'val' is unknown
//=     return static_cast<const Wrapper*>(this)->get();
//=            ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::value'
//=   bool isBoolean() const { return value().isBoolean(); }
//=                                   ^~~~~~~
//= note: Assuming value is anything but a boolean
//=   bool isBoolean() const { return value().isBoolean(); }
//=                                   ^~~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isBoolean'
//=     if (val.isBoolean())
//=         ^~~~~~~~~~~~~~~
//= note: Taking false branch
//=     if (val.isBoolean())
//=     ^
//= note: Calling 'WrappedPtrOperations::isNumber'
//=     if (val.isNumber())
//=         ^~~~~~~~~~~~~~
//= note: Assuming value is a double or a 32-bit integer
//=   bool isNumber() const { return value().isNumber(); }
//=                                  ^~~~~~~~~~~~~~~~~~
//= note: Returning from 'WrappedPtrOperations::isNumber'
//=     if (val.isNumber())
//=         ^~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (val.isNumber())
//=     ^
//= note: Calling 'WrappedPtrOperations::toInt32'
//=         return !!val.toInt32();
//=                  ^~~~~~~~~~~~~
//= note: Call to 'JS::Value::toInt32()' is incorrect because value is a double or a 32-bit integer
//=   int32_t toInt32() const { return value().toInt32(); }
//=                                    ^

bool func(JSContext* cx, JS::HandleValue val) {
    if (val.isBoolean())
        return val.toBoolean();
    if (val.isNumber())
        return !!val.toInt32();
    return false;
}

//== propagate value when using WrappedPtrOperations::get()

bool func(JSContext* cx, JS::HandleValue val1, JS::MutableHandleValue val2) {
    JS::Value v = val1.get();
    if (v.isBoolean())
        return val1.toBoolean();
    v = val2.get();
    if (v.isBoolean())
        return val2.toBoolean();
    JS::RootedValue val3(cx);
    val3.get().setBoolean(true);
    return val3.toBoolean();
}

//== swap

void DoSomethingBool(bool);
void DoSomethingNum(double);

void func(JS::Value val1, JS::Value val2) {
    val1.setBoolean(true);
    val2.setNumber(3.14);
    val1.swap(val2);
    DoSomethingBool(val2.toBoolean());
    DoSomethingNum(val1.toNumber());
}

//== operator=

uint32_t func() {
    JS::Value val1 = JS::TrueValue();
    JS::Value val2 = JS::Int32Value(0);
    val1 = val2;
    return val1.toInt32();
}

//== operator= makes sure constraints on an assigned value propagate back

bool func1(JS::Value v1, JS::Value v2) {
    v1 = v2;
    if (v1.isBoolean())
        return v2.toBoolean();
    return true;
}

bool func2(JS::Value v1, JS::Value v2) {
    v1 = v2;
    if (v2.isBoolean())
        return v1.toBoolean();
    return true;
}

//== mutator breaks constraint set by operator=
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'v2' is a 32-bit integer
//=         return v2.toBoolean();
//=                ^~~~~~~~~~~~~~
//= note: Assuming 'v2' is a 32-bit integer
//=     v2.setInt32(0);
//=     ^~~~~~~~~~~~~~
//= note: Taking true branch
//=     if (v1.isBoolean())
//=     ^
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'v2' is a 32-bit integer
//=         return v2.toBoolean();
//=                ^~~~~~~~~~~~~~

bool func(JS::Value v1, JS::Value v2) {
    v1 = v2;
    v2.setInt32(0);
    if (v1.isBoolean())
        return v2.toBoolean();
    return true;
}

//== operator== warns if values are known to be different types
//= warning: Call to 'JS::Value::operator==()' can never succeed because 'val1' can only be null and 'val2' can only be a 32-bit integer
//=     return val1 == val2;
//=            ^~~~~~~~~~~~
//= note: Calling 'NullValue'
//=     JS::Value val1 = JS::NullValue();
//=                      ^~~~~~~~~~~~~~~
//= note: Assuming 'val1' is null
//=     JS::Value val1 = JS::NullValue();
//=                      ^~~~~~~~~~~~~~~
//= note: Returning from 'NullValue'
//=     JS::Value val1 = JS::NullValue();
//=                      ^~~~~~~~~~~~~~~
//= note: Calling 'Int32Value'
//=     JS::Value val2 = JS::Int32Value(0);
//=                      ^~~~~~~~~~~~~~~~~
//= note: Assuming 'val2' is a 32-bit integer
//=     JS::Value val2 = JS::Int32Value(0);
//=                      ^~~~~~~~~~~~~~~~~
//= note: Returning from 'Int32Value'
//=     JS::Value val2 = JS::Int32Value(0);
//=                      ^~~~~~~~~~~~~~~~~
//= note: Call to 'JS::Value::operator==()' can never succeed because 'val1' can only be null and 'val2' can only be a 32-bit integer
//=     return val1 == val2;
//=            ^~~~~~~~~~~~

bool func() {
    JS::Value val1 = JS::NullValue();
    JS::Value val2 = JS::Int32Value(0);
    return val1 == val2;
}

//== operator== makes inferences about the type when it succeeds

JSObject* func(JS::Value val1, JS::Value val2) {
    if (val1.isObjectOrNull() && val2.isObject()) {
        if (val1 == val2)
            return &val1.toObject();
    }
    return nullptr;
}

//== operator!= makes inferences about the type when it succeeds
//= warning: Call to 'JS::Value::toBoolean()' is incorrect because 'val1' is undefined
//=             return val1.toBoolean();
//=                    ^~~~~~~~~~~~~~~~
//= note: Assuming 'val1' is undefined or null
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^~~~~~~~~~~~~~~~~~~~~~~~
//= note: Left side of '&&' is true
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=         ^
//= note: Taking true branch
//=     if (val1.isNullOrUndefined() && val2.isNull()) {
//=     ^
//= note: Assuming 'val1' is undefined
//=         if (val1 != val2)
//=             ^~~~
//= note: Taking true branch
//=         if (val1 != val2)
//=         ^
//= note: Call to 'JS::Value::toBoolean()' is incorrect because 'val1' is undefined
//=             return val1.toBoolean();
//=                    ^~~~~~~~~~~~~~~~

bool func(JS::Value val1, JS::Value val2) {
    if (val1.isNullOrUndefined() && val2.isNull()) {
        if (val1 != val2)
            return val1.toBoolean();
    }
    return false;
}
