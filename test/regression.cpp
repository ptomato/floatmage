// Various constructions that trip up naively-written analyzer code

//== unique_ptr with deleter

#include <memory>

template <void (*f)(char*) = nullptr>
struct S : std::unique_ptr<char, void (*)(char*)> {};

S<> foo();

static void bar() { foo(); }

//== JSNative with JS::CallArgs

JSAPI_RETURN_CONVENTION
bool func(JSContext* cx [[maybe_unused]], unsigned argc, JS::Value* vp) {
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    args.rval().setUndefined();
    return true;
}
